package pro.solid.inv.controlador;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.ModelAndView;

import pro.solid.inv.formularios.FormPrueba;

@Controller
@RequestMapping("/admin")
public class AdminInicioControlador {

	@GetMapping("/plantilla")
	public ModelAndView inicio(Model model){
		
		model.addAttribute("FormPrueba",new FormPrueba());
		
		return new ModelAndView("Administrador/plantilla").addObject(model);
		
	}
	
	
	@PostMapping("/plantilla")
	public ModelAndView iniciopost(Model model,FormPrueba formprueba){
		
		System.out.println(formprueba.toString());
		
		return new ModelAndView("redirect:/admin/plantilla").addObject(model);
		
	}
	
    @RequestMapping(value="/message",produces=MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public ModelAndView processForm(@RequestParam(required = false) String name,@RequestParam(required = false) String adult) {

    	System.out.println("Nombre:"+name+" ,Adulto:"+adult);
 
        return new ModelAndView("redirect:/admin/plantilla");
    }
	
}
