package pro.solid.inv.controlador;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pro.solid.inv.config.PaginaConfig;
import pro.solid.inv.formularios.FormIngreso;
import pro.solid.inv.modelo.Inventario;
import pro.solid.inv.modelo.RegistroIngreso;
import pro.solid.inv.servicio.CompuertaServicio;
import pro.solid.inv.servicio.RegistroIngresoServicio;
import pro.solid.inv.servicio.InventarioServicio;
import pro.solid.inv.servicio.ProductoServicio;
import pro.solid.inv.servicio.PuestoServicio;
import pro.solid.inv.servicio.UbicacionServicio;
import pro.solid.inv.servicio.UsuarioServicio;

@Controller
@EnableAutoConfiguration
@RequestMapping("/ingreso")
public class IngresoControlador {

	@Autowired
    private UsuarioServicio usuarioServicio;
	
	@Autowired
	private CompuertaServicio puertaServicio;

	@Autowired
	private ProductoServicio productoServicio;
	
	@Autowired
	private UbicacionServicio ubicacionServicio;
	
	@Autowired
	private InventarioServicio inventarioServicio;
	
	@Autowired
	private RegistroIngresoServicio registroIngresoServicio;

	@Autowired
	private PuestoServicio puestoServico;
	
	@GetMapping
    public ModelAndView inicio(Model model,HttpServletRequest request){
		
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	model.addAttribute("puesto",this.puestoServico.BuscarPuesto(auth.getName()));
		model.addAttribute("name",auth.getName());
		model.addAttribute("ingreso",true);
    	
    	ModelAndView modelandview = new ModelAndView(PaginaConfig.Ingreso);
    		    		    
	    	modelandview.addObject("compuertas",this.puertaServicio.ConsultaCompuertasDomo((long)1));
	    	modelandview.addObject("formIngreso",new FormIngreso());
	    	
	    	/*
	        InetAddress inetAddress;
			try {
				inetAddress = InetAddress.getLocalHost();
				System.out.println("IP Address:- " + inetAddress.getHostAddress());
		        System.out.println("Host Name:- " + inetAddress.getHostName());
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
	        
	    	
        return modelandview.addObject(model);
    }
	
	@PostMapping
    public ModelAndView RegistroIngreso(FormIngreso formIngreso,Model model,RedirectAttributes redirectAttributes){

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	model.addAttribute("puesto",this.puestoServico.BuscarPuesto(auth.getName()));
		model.addAttribute("name",auth.getName());
		
    	ModelAndView modelandview = new ModelAndView();

    		Long id_ubicacion = this.ubicacionServicio.IdUbicacion(formIngreso.getUbicacion());
    		Long id_producto = this.productoServicio.JPQLConsultaId(formIngreso.getCodigo());
    		
    		if(id_ubicacion==null || id_producto == null){//pregunta si existe los codigo de producto y ubicacion
	
	    		if(id_ubicacion==null){model.addAttribute("error_ubicacion","Ubicacion no Valida");}
	    		if(id_producto==null){model.addAttribute("error_producto","Producto no Valida");}
	    			
	    			modelandview.addObject("compuertas",this.puertaServicio.ConsultaCompuertasDomo((long)1));
	    			modelandview.addObject(model);
	    			
					modelandview.setViewName(PaginaConfig.Ingreso);
					
    			return modelandview;   			

    		}else{

    			Long id_inventario = this.inventarioServicio.JPQLConsultaExistencia(id_ubicacion, id_producto,formIngreso.getLote());
    				Inventario inventario = new Inventario();
    				System.out.println("Id Inventario: "+id_inventario);
    				//Inventario
    				if(id_inventario==null){//insertar 
    					inventario.setCantidad(Long.parseLong(formIngreso.getCantidad()));
    					inventario.setId_producto(id_producto);
    					inventario.setId_ubicacion(id_ubicacion);
    					inventario.setCantidad(Long.parseLong(formIngreso.getCantidad()));
    					inventario.setId_domo(1);
    					inventario.setLote(formIngreso.getLote());
    					this.inventarioServicio.JDBCInsercion(inventario);
    							 	
    				}else{//actualizar
    					this.inventarioServicio.ModificarIncrementar(Integer.parseInt(formIngreso.getCantidad()),id_inventario,formIngreso.getLote());
    				}
    				//Ingresar en Ingreso Registro
    				RegistroIngreso registroingreso = new RegistroIngreso();
    				registroingreso.setId_producto(id_producto);
    				registroingreso.setId_ubicacion(id_ubicacion);
    				registroingreso.setId_compuerta(Long.parseLong(formIngreso.getPuerta()));
    				registroingreso.setId_usuario(this.usuarioServicio.ConsultaId(auth.getName()));
    				registroingreso.setCantidad(Long.parseLong(formIngreso.getCantidad()));
    				registroingreso.setLote(formIngreso.getLote());
    				this.registroIngresoServicio.JDBCInsercion(registroingreso);		
    				
    				redirectAttributes.addFlashAttribute("exito", "Ingreso con Exito");
    				
    	            modelandview.setViewName("redirect:/ingreso");
    				
    	        return modelandview;
    		}
    		
    }
	
}
