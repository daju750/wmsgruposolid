package pro.solid.inv.controlador;

import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import pro.solid.inv.JDBCObject.JDBCReportaTraslados;
import pro.solid.inv.JDBCObject.JDBCReporteIngreso;
import pro.solid.inv.JDBCObject.JDBCReporteTotalInventario;
import pro.solid.inv.JDBCObject.JDBCReporteUbicaciones;
import pro.solid.inv.config.PaginaConfig;
import pro.solid.inv.formularios.FormReporteIngreso;
import pro.solid.inv.servicio.ReporteServicio;
import pro.solid.inv.servicio.UsuarioServicio;
import pro.solid.inv.JDBCObject.JDBCReporteSalida;

@Controller
@RequestMapping("/admin/reporte")
public class AdminReporte {

	@Autowired
	private ReporteServicio reporteServicio;
	
	@Autowired
	private UsuarioServicio usuarioServicio;
	
	@GetMapping
	public ModelAndView inicio(Model model){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		return new ModelAndView(PaginaConfig.AdminReporteInicio).addObject(model);
	}
	
	
	@GetMapping("/total")
	public ModelAndView inventarioTotal(Model model){
		model.addAttribute("inventarioTotal",this.reporteServicio.JDBReporteInvTotal());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		
		return new ModelAndView("/Administrador/reporte/inventario_total").addObject(model);
	}
	
	@GetMapping("/descargar/inventarioTotal")
	public ResponseEntity<StreamingResponseBody> DescagarinventarioTotal(Model model){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		
		@SuppressWarnings("resource")
		Workbook workBook = new XSSFWorkbook();
		  Sheet sheet = workBook.createSheet("Inventario Total");
		  sheet.setColumnWidth(0, 4500);
		  sheet.setColumnWidth(1, 10000);
		  sheet.setColumnWidth(2, 4560);
		  sheet.setColumnWidth(3, 2560);
		  sheet.setColumnWidth(4, 4500);
		  Row row = sheet.createRow(0);
		  row.createCell(0).setCellValue("CodigoSAP");
		  row.createCell(1).setCellValue("Descripcion");
		  row.createCell(2).setCellValue("Inventario Total");
		  row.createCell(3).setCellValue("Reservas");
		  row.createCell(4).setCellValue("Inventario Neto");
		  
		  int x=1;
		  List<JDBCReporteTotalInventario> templateTotal= this.reporteServicio.JDBReporteInvTotal();
		  for(JDBCReporteTotalInventario fila:templateTotal) {
			row = sheet.createRow(x);
			  row.createCell(0).setCellValue(fila.getCodigosap());
			  row.createCell(1).setCellValue(fila.getDescripcion());
			  row.createCell(2).setCellValue(fila.getTotal());
			  row.createCell(3).setCellValue(fila.getReserva());
			  row.createCell(4).setCellValue(fila.getNeto());
			x+=1;
		}

	  return ResponseEntity
			    .ok()
			    .contentType(MediaType.APPLICATION_OCTET_STREAM)
			    .header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=\"Reporte Inventario  Total "+new Date()+".xlsx\"")
			    .body(workBook::write);
	}
	
	@GetMapping("/ingreso")
	public ModelAndView montacargistaIngreso(Model model,FormReporteIngreso formReporteIngreso){
		
		model.addAttribute("formReporteIngreso",new FormReporteIngreso());
		model.addAttribute("usuarios",this.usuarioServicio.JDBConsultaDondePuesto("montacargista"));
		
		return new ModelAndView("/Administrador/reporte/montacargista_ingreso").addObject(model);
	}
	
	@PostMapping("/descargar/ingreso")
	public ResponseEntity<StreamingResponseBody> DescargarmontacargistaIngreso(Model model,FormReporteIngreso formIngresoReporte){

		List<JDBCReporteIngreso> templateIngreso = this.reporteServicio.JDBCReporteIngresos(formIngresoReporte.getFechaInicio(),formIngresoReporte.getFechaFin(),formIngresoReporte.getUsuario());
		
		  @SuppressWarnings("resource")
		  Workbook workBook = new XSSFWorkbook();
		
		  CellStyle style = workBook.createCellStyle();//Create style
		  Font font = workBook.createFont();
		  font.setBold(true);//Make font bold
		  style.setFont(font);//set it to bold
		  
		  Sheet sheet = workBook.createSheet("Motacargista Ingreso");
		  sheet.setColumnWidth(0, 4500);
		  sheet.setColumnWidth(1, 2560);
		  sheet.setColumnWidth(2, 3500);
		  sheet.setColumnWidth(3, 3500);
		  sheet.setColumnWidth(4, 3500);
		  sheet.setColumnWidth(5, 6500);
		  
		  Row row = sheet.createRow(0);
		  row.createCell(0).setCellValue("Usuario");
		  row.createCell(1).setCellValue(formIngresoReporte.getUsuario());
		  row.createCell(2).setCellValue("Fecha Inicio");
		  row.createCell(3).setCellValue(formIngresoReporte.getFechaInicio());
		  row.createCell(4).setCellValue("Fecha Fin");
		  row.createCell(5).setCellValue(formIngresoReporte.getFechaFin());
		  
		  row.getCell(0).setCellStyle(style);
		  row.getCell(2).setCellStyle(style);
		  row.getCell(4).setCellStyle(style);
		  
		  row = sheet.createRow(1);
		  row.createCell(0).setCellValue("codido SAP");
		  row.createCell(1).setCellValue("ubicacion");
		  row.createCell(2).setCellValue("cantidad");
		  row.createCell(3).setCellValue("puerta");
		  row.createCell(4).setCellValue("lote");
		  row.createCell(5).setCellValue("fecha");
		  
		  row.getCell(0).setCellStyle(style);
		  row.getCell(1).setCellStyle(style);
		  row.getCell(2).setCellStyle(style);
		  row.getCell(3).setCellStyle(style);
		  row.getCell(4).setCellStyle(style);
		  row.getCell(5).setCellStyle(style);
		  
		  int x=2;
		for(JDBCReporteIngreso fila:templateIngreso) {
			row = sheet.createRow(x);
			  row.createCell(0).setCellValue(fila.getCodigosap());
			  row.createCell(1).setCellValue(fila.getUbicacion());
			  row.createCell(2).setCellValue(fila.getCantidad());
			  row.createCell(3).setCellValue(fila.getPuerta());
			  row.createCell(4).setCellValue(fila.getLote());
			  row.createCell(5).setCellValue(fila.getFecha());
			x+=1;
		}
		  
		  return ResponseEntity
		    .ok()
		    .contentType(MediaType.APPLICATION_OCTET_STREAM)
		    .header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=\"Reporte Ingreso Montacargista "+new Date()+".xlsx\"")
		    .body(workBook::write);
	}
	
	
	@GetMapping("/traslado")
	public ModelAndView montacargistaTraslado(Model model){
		
		model.addAttribute("formReporteIngreso",new FormReporteIngreso());
		model.addAttribute("usuarios",this.usuarioServicio.JDBConsultaDondePuesto("montacargista"));
		
		return new ModelAndView("/Administrador/reporte/montacargista_traslado").addObject(model);
	}
	
	@PostMapping("/descargar/traslado")
	public ResponseEntity<StreamingResponseBody> DescargarTraslados(Model model,FormReporteIngreso formIngresoReporte){
		
		List<JDBCReportaTraslados> template = this.reporteServicio.JDBCReporteTraslados(formIngresoReporte.getFechaInicio(),formIngresoReporte.getFechaFin(),formIngresoReporte.getUsuario());
		  @SuppressWarnings("resource")
		  Workbook workBook = new XSSFWorkbook();
		
		  CellStyle style = workBook.createCellStyle();//Create style
		  Font font = workBook.createFont();
		  font.setBold(true);//Make font bold
		  style.setFont(font);//set it to bold
		  
		  Sheet sheet = workBook.createSheet("Motacargista Traslado");
		  sheet.setColumnWidth(0, 4500);
		  sheet.setColumnWidth(1, 2560);
		  sheet.setColumnWidth(2, 3500);
		  sheet.setColumnWidth(3, 3500);
		  sheet.setColumnWidth(4, 3500);
		  sheet.setColumnWidth(5, 6500);
		  
		  Row row = sheet.createRow(0);
		  row.createCell(0).setCellValue("Usuario");
		  row.createCell(1).setCellValue(formIngresoReporte.getUsuario());
		  row.createCell(2).setCellValue("Fecha Inicio");
		  row.createCell(3).setCellValue(formIngresoReporte.getFechaInicio());
		  row.createCell(4).setCellValue("Fecha Fin");
		  row.createCell(5).setCellValue(formIngresoReporte.getFechaFin());
		  
		  row.getCell(0).setCellStyle(style);
		  row.getCell(2).setCellStyle(style);
		  row.getCell(4).setCellStyle(style);
		  
		  row = sheet.createRow(1);
		  row.createCell(0).setCellValue("Codigo SAP");
		  row.createCell(1).setCellValue("Lote");
		  row.createCell(2).setCellValue("Cantidad");
		  row.createCell(3).setCellValue("Origen");
		  row.createCell(4).setCellValue("Destino");
		  row.createCell(5).setCellValue("Fecha");
		  
		  row.getCell(0).setCellStyle(style);
		  row.getCell(1).setCellStyle(style);
		  row.getCell(2).setCellStyle(style);
		  row.getCell(3).setCellStyle(style);
		  row.getCell(4).setCellStyle(style);
		  row.getCell(5).setCellStyle(style);
		  
		  int x=2;

		  for(JDBCReportaTraslados fila:template) {
			row = sheet.createRow(x);
			  row.createCell(0).setCellValue(fila.getCodigosap());
			  row.createCell(1).setCellValue(fila.getLote());
			  row.createCell(2).setCellValue(fila.getCantidad());
			  row.createCell(3).setCellValue(fila.getOrigen());
			  row.createCell(4).setCellValue(fila.getDestino());
			  row.createCell(5).setCellValue(fila.getFecha());
			x+=1;
		}
		  
		  return ResponseEntity
		    .ok()
		    .contentType(MediaType.APPLICATION_OCTET_STREAM)
		    .header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=\"Reporte Traslado Montacargista "+new Date()+".xlsx\"")
		    .body(workBook::write);
	}
	
	
	@GetMapping("/salida")
	public ModelAndView pikeadorSalida(Model model){
		
		model.addAttribute("formReporteIngreso",new FormReporteIngreso());
		model.addAttribute("usuarios",this.usuarioServicio.JDBConsultaDondePuesto("pikeador"));

		return new ModelAndView("/Administrador/reporte/pikeador_salida").addObject(model);
	}
	
	@PostMapping("/descargar/salida")
	public ResponseEntity<StreamingResponseBody> DescargarSalidas(Model model,FormReporteIngreso formIngresoReporte){
		
		List<JDBCReporteSalida> template = this.reporteServicio.JDBCReporteSalida(formIngresoReporte.getUsuario(),formIngresoReporte.getFechaInicio(),formIngresoReporte.getFechaFin());
		  @SuppressWarnings("resource")
		  Workbook workBook = new XSSFWorkbook();
		
		  CellStyle style = workBook.createCellStyle();//Create style
		  Font font = workBook.createFont();
		  font.setBold(true);//Make font bold
		  style.setFont(font);//set it to bold
		  
		  Sheet sheet = workBook.createSheet("Motacargista Traslado");
		  sheet.setColumnWidth(0, 4500);
		  sheet.setColumnWidth(1, 2560);
		  sheet.setColumnWidth(2, 3500);
		  sheet.setColumnWidth(3, 3500);
		  sheet.setColumnWidth(4, 3500);
		  sheet.setColumnWidth(5, 6500);
		  
		  Row row = sheet.createRow(0);
		  row.createCell(0).setCellValue("Usuario");
		  row.createCell(1).setCellValue(formIngresoReporte.getUsuario());
		  row.createCell(2).setCellValue("Fecha Inicio");
		  row.createCell(3).setCellValue(formIngresoReporte.getFechaInicio());
		  row.createCell(4).setCellValue("Fecha Fin");
		  row.createCell(5).setCellValue(formIngresoReporte.getFechaFin());
		  
		  row.getCell(0).setCellStyle(style);
		  row.getCell(2).setCellStyle(style);
		  row.getCell(4).setCellStyle(style);
		  
		  row = sheet.createRow(1);
		  row.createCell(0).setCellValue("Id Hoja");
		  row.createCell(1).setCellValue("Codigosap");
		  row.createCell(2).setCellValue("Ubicacion");
		  row.createCell(3).setCellValue("Cantidad");
		  row.createCell(4).setCellValue("Descripcion");
		  row.createCell(5).setCellValue("Ruta");
		  row.createCell(6).setCellValue("Puerta");
		  row.createCell(7).setCellValue("Fecha");
		  
		  row.getCell(0).setCellStyle(style);
		  row.getCell(1).setCellStyle(style);
		  row.getCell(2).setCellStyle(style);
		  row.getCell(3).setCellStyle(style);
		  row.getCell(4).setCellStyle(style);
		  row.getCell(5).setCellStyle(style);
		  row.getCell(6).setCellStyle(style);
		  row.getCell(7).setCellStyle(style);
		  
		  int x=2;

		  for(JDBCReporteSalida fila:template) {
			row = sheet.createRow(x);
			  row.createCell(0).setCellValue(fila.getId());
			  row.createCell(1).setCellValue(fila.getCodigosap());
			  row.createCell(2).setCellValue(fila.getUbicacion());
			  row.createCell(3).setCellValue(fila.getCantidad());
			  row.createCell(4).setCellValue(fila.getDescripcion());
			  row.createCell(5).setCellValue(fila.getRuta());
			  row.createCell(6).setCellValue(fila.getPuerta());
			  row.createCell(7).setCellValue(fila.getLote());
			  row.createCell(7).setCellValue(fila.getFecha());
			  
			x+=1;
		}
		  
		  return ResponseEntity
		    .ok()
		    .contentType(MediaType.APPLICATION_OCTET_STREAM)
		    .header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=\"Reporte Traslado Montacargista "+new Date()+".xlsx\"")
		    .body(workBook::write);
	}
	
	
	@GetMapping("/ubicaciones")
	public ModelAndView ubicaciones(Model model){
		
		model.addAttribute("ubicaciones",this.reporteServicio.JDBReporteUbicaciones());
		
		return new ModelAndView("/Administrador/reporte/ubicaciones").addObject(model);
	}
	
	@GetMapping("/descargar/Ubicaciones")
	public ResponseEntity<StreamingResponseBody> Descargarubicaciones(Model model){
		
		List<JDBCReporteUbicaciones> template = this.reporteServicio.JDBReporteUbicaciones(); 
		
		@SuppressWarnings("resource")
		Workbook workBook = new XSSFWorkbook();
		  Sheet sheet = workBook.createSheet("Ubicaciones");
		  sheet.setColumnWidth(0, 4500);
		  sheet.setColumnWidth(1, 2560);
		  sheet.setColumnWidth(2, 3560);
		  sheet.setColumnWidth(3, 3560);
		  sheet.setColumnWidth(4, 4500);
		  
		  CellStyle style = workBook.createCellStyle();//Create style
		  Font font = workBook.createFont();
		  font.setBold(true);//Make font bold
		  style.setFont(font);//set it to bold
		  
		  Row row = sheet.createRow(0);
		  row.createCell(0).setCellValue("CodigoSAP");
		  row.createCell(1).setCellValue("Ubicacion");
		  row.createCell(2).setCellValue("Cantidad");
		  row.createCell(3).setCellValue("Reservas");
		  row.createCell(4).setCellValue("Lote");
		  row.getCell(0).setCellStyle(style);
		  row.getCell(1).setCellStyle(style);
		  row.getCell(2).setCellStyle(style);
		  row.getCell(3).setCellStyle(style);
		  row.getCell(4).setCellStyle(style);
		  
		  //row = sheet.createRow(1);
		  int x=1;
		  for(JDBCReporteUbicaciones fila: template) {
			  row = sheet.createRow(x);
				  row.createCell(0).setCellValue(fila.getCodigosap());
				  row.createCell(1).setCellValue(fila.getUbicacion());
				  row.createCell(2).setCellValue(fila.getCantidad());
				  row.createCell(3).setCellValue(fila.getReserva());
				  row.createCell(4).setCellValue(fila.getLote());
			x+=1;
		  }
		  
		  
		  return ResponseEntity
				    .ok()
				    .contentType(MediaType.APPLICATION_OCTET_STREAM)
				    .header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=\"Reporte Ubicaciones "+new Date()+".xlsx\"")
				    .body(workBook::write);
	}
	
	@GetMapping("/excel")
	public ResponseEntity<StreamingResponseBody> excel() {
	  
	  @SuppressWarnings("resource")
	  Workbook workBook = new XSSFWorkbook();
	  Sheet sheet = workBook.createSheet("Reporte XXXX");
	  sheet.setColumnWidth(0, 2560);
	  Row row = sheet.createRow(0);
	  row.createCell(0).setCellValue("CodigoSAP");
	  row.createCell(1).setCellValue("Ubicacion");
	  row.createCell(2).setCellValue("Ruta");
	  row.createCell(3).setCellValue("Usuario");
	  
	  row = sheet.createRow(1);
	  row.createCell(0).setCellValue("00-001020391");
	  row.createCell(1).setCellValue("10104");
	  row.createCell(2).setCellValue("Escuintla");
	  row.createCell(3).setCellValue("kverde");
	  
	  return ResponseEntity
	    .ok()
	    .contentType(MediaType.APPLICATION_OCTET_STREAM)
	    .header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=\"Reporte XXXX.xlsx\"")
	    .body(workBook::write);
	}

	
}
