package pro.solid.inv.controlador;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import pro.solid.inv.componente.Paginacion;
import pro.solid.inv.config.PaginaConfig;
import pro.solid.inv.entidad.UsuarioMaestroDetalle;
import pro.solid.inv.formularios.FormUsuario;
import pro.solid.inv.modelo.Usuario;
import pro.solid.inv.modelo.UsuarioDetalle;
import pro.solid.inv.servicio.PuestoServicio;
import pro.solid.inv.servicio.RolServicio;
import pro.solid.inv.servicio.UsuarioDetalleServicio;
import pro.solid.inv.servicio.UsuarioServicio;

@Controller
@RequestMapping("/admin/usuario")
public class AdminUsuarioControlador {

	@Autowired
	private UsuarioServicio usuarioServicio;
	
	@Autowired
	private PuestoServicio puestoServicio;
	
	@Autowired
	private UsuarioDetalleServicio usuarioDetalleServicio;

	@Autowired
	private RolServicio roleServicio;
	
	@GetMapping
	public ModelAndView Iniciosuario(Model model){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		model.addAttribute("usuario","usuario");
		return new ModelAndView(PaginaConfig.AdminUsuario).addObject(model);
	}
	
	@GetMapping("/nuevo")
	public ModelAndView GETNuevousuario(Model model){
		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		model.addAttribute("formUsuario",new FormUsuario());
		
		return new ModelAndView(PaginaConfig.AdminNuevoUsuario).addObject(model);
	}
	
	
	@PostMapping("/nuevo")
	public ModelAndView POSTNuevousuario(Model model,FormUsuario formUsuario){
		
		Usuario usuario = new Usuario(this.usuarioServicio.JDBCMax()+1,formUsuario.getUsuario(),formUsuario.getContrasena());
		this.usuarioServicio.JDBCGuardar(usuario);
		
		UsuarioDetalle usuarioDetalle = new UsuarioDetalle();
		usuarioDetalle.setId(this.usuarioDetalleServicio.JDBCMax()+1);
		usuarioDetalle.setId_usuario(usuario.getId());
		usuarioDetalle.setPrimer_nombre(formUsuario.getPrimer_nombre());
		usuarioDetalle.setSegundo_nombre(formUsuario.getSegundo_nombre());
		usuarioDetalle.setEmail(formUsuario.getEmail());
		usuarioDetalle.setPrimer_apellido(formUsuario.getPrimer_apellido());
		usuarioDetalle.setSegundo_apellido(formUsuario.getSegundo_apellido());
		usuarioDetalle.setDireccion(formUsuario.getDireccion());
		usuarioDetalle.setTelefono(Integer.parseInt(formUsuario.getTelefono()));
		
		this.roleServicio.JDBCGuardar(usuario.getId(),(long)2);
		this.puestoServicio.JDBCInsertar(usuario.getId(),Long.valueOf(formUsuario.getPuesto()));
		this.usuarioDetalleServicio.Guardar(usuarioDetalle);
		
		return new ModelAndView("redirect:/admin/usuario/lista").addObject(model);
	}
	
	@GetMapping("/lista")
	public ModelAndView Listarsuario(Model model,HttpServletRequest request){
		
		int page = 0; //default page number is 0 (yes it is weird)
        int size = 4; //default page size is 10
		
        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        
        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }
		
        Paginacion paginacion = new Paginacion();
        paginacion.setPaginaActual((long)(page));
        paginacion.setTotalItems(this.usuarioServicio.JDBConsultaContarUsuarioRoleUser());
        paginacion.GenerarLimites(page+1,paginacion.PaginasTotales(size,paginacion.getTotalItems()).intValue());
        List<UsuarioMaestroDetalle> listaUsuariosUserRole = this.usuarioServicio.JDBCPaginacionListarUsuariosRoleUser((long)size,(long) (page*size));
        
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		model.addAttribute("UsuariosUser",listaUsuariosUserRole);
		
		model.addAttribute("PaginasTotales",paginacion.getPaginasTotales());
		model.addAttribute("PaginaActual",paginacion.getPaginaActual()+1);
        model.addAttribute("BotonLimtSup", paginacion.isBotonLimtSup());
        model.addAttribute("BotonLimtInf", paginacion.isBotonLimtInf());
        model.addAttribute("LimitPagSup",paginacion.getLimiteSuperior());
        model.addAttribute("LimitPagInf",paginacion.getLimiteInferior());
		
		return new ModelAndView(PaginaConfig.AdminListarUsuarios).addObject(model);
	}
	
	
	@GetMapping("/{id}")
	public ModelAndView GETeditarusuario(@PathVariable("id") long id,Model model){

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String tempuesto=this.puestoServicio.JDBCConsultar(id);
		
		ModelAndView modelandview;boolean validador=true;
		
		if(tempuesto=="administrador" && this.usuarioServicio.ConsultaId(auth.getName())!=id){
			validador=false;
		}
		
		if(validador==true) {
			
			UsuarioMaestroDetalle usuarioDetalle = this.usuarioServicio.usuarioRolerUserId(id);
			model.addAttribute("usuarioDetalle",usuarioDetalle);
			model.addAttribute("puestos",this.puestoServicio.JDBConsultaPuestoUnionTodosNo(id));
			model.addAttribute("id",id);
			model.addAttribute("admin",this.usuarioServicio.ConsultaId(auth.getName()));
			
			
			modelandview = new ModelAndView(PaginaConfig.AdminEditarUsuario);
			modelandview.addObject(model);
			
		}else{
			modelandview = new ModelAndView("error_uthentificion");
		}
		
		return modelandview;
	}
	
	@PostMapping("/{id}")
	public ModelAndView POSTeditarusuario(@PathVariable("id") long id,UsuarioMaestroDetalle MaestrousuarioDetalle,Model model){

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		UsuarioDetalle usuarioDetalle = new UsuarioDetalle();
		usuarioDetalle.setId(MaestrousuarioDetalle.getId());
		usuarioDetalle.setId_usuario(id);
		usuarioDetalle.setPrimer_nombre(MaestrousuarioDetalle.getPrimer_nombre());
		usuarioDetalle.setSegundo_nombre(MaestrousuarioDetalle.getSegundo_nombre());
		usuarioDetalle.setEmail(MaestrousuarioDetalle.getEmail());
		usuarioDetalle.setPrimer_apellido(MaestrousuarioDetalle.getPrimer_apellido());
		usuarioDetalle.setSegundo_apellido(MaestrousuarioDetalle.getSegundo_apellido());
		usuarioDetalle.setDireccion(MaestrousuarioDetalle.getDireccion());
		usuarioDetalle.setTelefono(Integer.parseInt(MaestrousuarioDetalle.getTelefono()));
		
		this.puestoServicio.Actulizar(id,Long.parseLong(MaestrousuarioDetalle.getPuesto()));
		this.usuarioDetalleServicio.Guardar(usuarioDetalle);
		
		ModelAndView modelandview = new ModelAndView("redirect:/admin/usuario/"+id);
		modelandview.addObject(model);
		
		
		return modelandview;
	}
	
	@GetMapping("/contrasena/{id}")
	public ModelAndView editarcontraGET(@PathVariable("id") long id,Model model){

		Usuario usuario  = new Usuario();
		usuario.setUsername(this.usuarioServicio.usuario(id).getUsername());
		model.addAttribute("usuario",usuario);
		model.addAttribute("id",id);
		
		return new ModelAndView("Administrador/usuario_cambiarcontra").addObject(model);
	}
	
	@ResponseBody
	@PostMapping("/contrasena/{id}")
	public String editarcontraPOST(@PathVariable("id") long id,Usuario usuario){
		
		this.usuarioServicio.JDBCActulizarContrasena(id,usuario.getPassword());
        return "<script>window.close();</script>";
	}	
	//verde
	@GetMapping("/historial/{id}")
	public ModelAndView historialusuario(@PathVariable("id") long id,Model model){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		return new ModelAndView(PaginaConfig.AdminHistorialUsuario).addObject(model);
	}
	
}
