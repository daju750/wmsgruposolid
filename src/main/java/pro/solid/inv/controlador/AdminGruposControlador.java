package pro.solid.inv.controlador;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pro.solid.inv.componente.Paginacion;
import pro.solid.inv.modelo.Puesto;
import pro.solid.inv.servicio.PuestoServicio;

@Controller
@RequestMapping("/admin/grupos")
public class AdminGruposControlador {

	@Autowired
	private PuestoServicio puestoServicio;
	
	@GetMapping
	public ModelAndView Grupos(HttpServletRequest request,Model model){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		int page = 0; //default page number is 0 (yes it is weird)
        int size = 10; //default page size is 10
		
        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }
		
        Page<Puesto> puestos = this.puestoServicio.PuestoUsuario(PageRequest.of(page,size));
		
        Paginacion paginacion = new Paginacion(puestos.getNumber()+1,puestos.getTotalPages());
        
        model.addAttribute("puestos", puestos);
        model.addAttribute("paginacion", puestos);
        model.addAttribute("PaginasTotales", puestos.getTotalPages());
        model.addAttribute("BotonLimtSup", paginacion.isBotonLimtSup());
        model.addAttribute("BotonLimtInf", paginacion.isBotonLimtInf());
        model.addAttribute("LimitPagSup",paginacion.getLimiteSuperior());
        model.addAttribute("LimitPagInf",paginacion.getLimiteInferior());
        
		return new ModelAndView("Administrador/usuario_grupos").addObject(model);
	}
	
}
