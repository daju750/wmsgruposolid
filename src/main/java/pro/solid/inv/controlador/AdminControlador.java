package pro.solid.inv.controlador;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pro.solid.inv.componente.Paginacion;
import pro.solid.inv.config.PaginaConfig;
import pro.solid.inv.modelo.Producto;
import pro.solid.inv.servicio.ProductoServicio;

@Controller
@RequestMapping("/admin")
public class AdminControlador {

	@Autowired
	private ProductoServicio productServicio;
	
	@RequestMapping
    public ModelAndView inicio(Model model){
    	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		ModelAndView modelandview = new ModelAndView(PaginaConfig.AdminInicio);
        return modelandview.addObject(model);
    }

	@ResponseBody
	@RequestMapping("/cerrar")
	public String cerrar(){
        return "<script>window.close();</script>";
    }
	
	@RequestMapping("/catalogo")
    public ModelAndView catalogo(Model model){
    	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		ModelAndView modelandview = new ModelAndView("Administrador/catalogo");
        return modelandview.addObject(model);
    }
	
	
	@GetMapping("/productos")
	public ModelAndView producto(HttpServletRequest request,Model model){
		
		int page = 0; //default page number is 0 (yes it is weird)
        int size = 10; //default page size is 10
		
        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }
		
        Page<Producto> productos = this.productServicio.Producto(PageRequest.of(page,size));
        
        Paginacion paginacion = new Paginacion(productos.getNumber()+1,productos.getTotalPages());
        
        model.addAttribute("productos", productos);
        model.addAttribute("PaginasTotales", productos.getTotalPages());
        model.addAttribute("BotonLimtSup", paginacion.isBotonLimtSup());
        model.addAttribute("BotonLimtInf", paginacion.isBotonLimtInf());
        model.addAttribute("LimitPagSup",paginacion.getLimiteSuperior());
        model.addAttribute("LimitPagInf",paginacion.getLimiteInferior());
        
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		return new ModelAndView("Administrador/catalogo_productos").addObject(model);
	}
	
	@GetMapping("/ubicacion")
	public ModelAndView ubicacion(Model model){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		return new ModelAndView(PaginaConfig.AdminReporteInicio).addObject(model);
	}
	
}
