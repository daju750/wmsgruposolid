package pro.solid.inv.controlador;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pro.solid.inv.config.PaginaConfig;
import pro.solid.inv.servicio.InventarioServicio;
import pro.solid.inv.servicio.ProductoServicio;
import pro.solid.inv.servicio.PuestoServicio;
import pro.solid.inv.servicio.RegistroTrasladoServicio;
import pro.solid.inv.servicio.UbicacionServicio;
import pro.solid.inv.servicio.UsuarioServicio;
import pro.solid.inv.formularios.FormTraslado;
import pro.solid.inv.modelo.Inventario;
import pro.solid.inv.entidad.RegistroTraslado;

@Controller
@EnableAutoConfiguration
@RequestMapping("/traslado")
public class TrasladoControlador {

	@Autowired
    private UsuarioServicio usuarioServicio;
	
	@Autowired
	private UbicacionServicio ubicacionServicio;
	
	@Autowired
	private InventarioServicio inventarioServicio;
	
	@Autowired
	private ProductoServicio productoServicio;
	
	@Autowired
	private RegistroTrasladoServicio registroTrasladoServicio;
	
	@Autowired
	private PuestoServicio puestServico;
	
    @GetMapping
    public ModelAndView inicio(Model model,HttpServletRequest request){
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	model.addAttribute("puesto",this.puestServico.BuscarPuesto(auth.getName()));
		model.addAttribute("name",auth.getName());
		
    	model.addAttribute("traslado",true);
    	
    	ModelAndView modelandview= new ModelAndView(PaginaConfig.Traslado);
    		
	    	modelandview.addObject(model);
	    	modelandview.addObject("formTraslado",new FormTraslado());

        return modelandview;
    }
	
    @PostMapping
    public ModelAndView generarTraslado(FormTraslado formTraslado,Model model,RedirectAttributes redirectAttributes){	
    
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	model.addAttribute("puesto",this.puestServico.BuscarPuesto(auth.getName()));
		model.addAttribute("name",auth.getName());
    	
			ModelAndView modelandview = new ModelAndView();
				
    		Long id_destino = this.ubicacionServicio.IdUbicacion(formTraslado.getDestino());
    		Long id_origen = this.ubicacionServicio.IdUbicacion(formTraslado.getOrigen());
    		Long id_producto = this.productoServicio.JPQLConsultaId(formTraslado.getCodigosap());
    		Long id_invOrigen=null;
    		
    		if(id_origen!=null && id_producto != null){try{id_invOrigen=this.inventarioServicio.JPQLConsultaExistencia(id_origen, id_producto,formTraslado.getLote());}catch(Exception ex) {System.out.println(ex);}};
    		
			if(id_origen==null || id_producto == null || id_destino==null || id_invOrigen==null ){
    			
    			if(id_origen==null){model.addAttribute("error_ubicacionOrigen","Origen no Valida");}
	    		if(id_producto==null){model.addAttribute("error_producto","Producto no Valida");}
	    		if(id_destino==null){model.addAttribute("error_productoDestino","Ubicacion destino invalida");}
	    		if(id_invOrigen==null){model.addAttribute("error_invOrigen","No existene en el Inventario");}

	    		modelandview.setViewName(PaginaConfig.Traslado);
    			return modelandview.addObject(model);
    			
    		}else{
    			
    			Inventario invOrigen = this.inventarioServicio.JQPLConsultaCantidad(id_origen, id_producto,formTraslado.getLote());
    			Long cantidad = Long.parseLong(formTraslado.getCantidad());
    			Long cantiNeto = invOrigen.getCantidad()-invOrigen.getReserva();
    			
    			if(cantidad>cantiNeto || cantidad==cantiNeto){

    				model.addAttribute("error_faltaUni","Producto Insuficiente:"+cantiNeto+" unidades disponibles, de las "+formTraslado.getCantidad()+" solicitadas");
    				formTraslado.setCantidad(String.valueOf(cantiNeto));
    				model.addAttribute("formTraslado",formTraslado);
    				modelandview.addObject(model);
    	    		modelandview.setViewName(PaginaConfig.Traslado);
    				
    			}else{
    			
    				Long id_inventarioDestino = this.inventarioServicio.JPQLConsultaExistencia(id_destino, id_producto,formTraslado.getLote());
        			
        			if(id_inventarioDestino!=null){//Se incrementara	
        				this.inventarioServicio.ModificarIncrementar(Integer.parseInt(formTraslado.getCantidad()),id_inventarioDestino,formTraslado.getLote());
        			}else {//Se insertara  
        				
        				Inventario invDestino = new Inventario();
            			invDestino.setId_producto(id_producto);
            			invDestino.setId_ubicacion(id_destino);
            			invDestino.setCantidad(cantidad);
            			invDestino.setLote(formTraslado.getLote());
            			invDestino.setId_domo(1);
        				
            			this.inventarioServicio.JDBCInsercion(invDestino);
        			}
        			

        			if(cantidad.equals(cantiNeto)==true){invOrigen.setHabilitado(false);}
        			invOrigen.setCantidad(invOrigen.getCantidad()-cantidad);
        			this.inventarioServicio.Insercion(invOrigen);
        			
    				//Registro Traslado
    				RegistroTraslado registroTraslado = new RegistroTraslado();
    				registroTraslado.setId_producto(id_producto);
    				registroTraslado.setCantidad(Integer.parseInt(formTraslado.getCantidad()));
    				registroTraslado.setId_origen(id_origen);
    				registroTraslado.setId_destino(id_destino);
    				registroTraslado.setId_usuario(this.usuarioServicio.ConsultaId(auth.getName()));
    				registroTraslado.setLote(formTraslado.getLote());
    				this.registroTrasladoServicio.JDBCInsercion(registroTraslado);

    				redirectAttributes.addFlashAttribute("exito", "Traslado gestionado");
    				modelandview.setViewName("redirect:/traslado");
    				
    			}

				
    		return modelandview;
    		}
    		
    }
}
