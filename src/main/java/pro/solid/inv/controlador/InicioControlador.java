package pro.solid.inv.controlador;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pro.solid.inv.config.PaginaConfig;
import pro.solid.inv.servicio.PuestoServicio;

@Controller
@RequestMapping({"/", "/inicio"})
public class InicioControlador {

	@Autowired
	private PuestoServicio puestServico;
	
    @GetMapping
    public ModelAndView inicio(Model model){
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	String usuario = auth.getName();
    	String puesto = this.puestServico.BuscarPuesto(usuario);
    	
	    	model.addAttribute("puesto",puesto);
			model.addAttribute("name",usuario);
	    
	    	if(puesto.equals("administrador")){
	    		return new ModelAndView(PaginaConfig.AdminInicio).addObject(model);
	    	}
	    	else{
	    		return new ModelAndView(PaginaConfig.Inicio).addObject(model);
	    	}
    }
	
    
    @GetMapping("/prueba")
    public ModelAndView inicioPruebas(Model model){
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	String usuario = auth.getName();
    	String puesto = this.puestServico.BuscarPuesto(usuario);
    	
	    	model.addAttribute("puesto",puesto);
			model.addAttribute("name",usuario);
	    
	    	if(puesto.equals("administrador")){
	    		return new ModelAndView("prueba").addObject(model);
	    	}
	    	else{
	    		return new ModelAndView("prueba").addObject(model);
	    	}
	    	
    }
	
 
    @PostMapping("/prueba")
    public ModelAndView PostinicioPruebas(Model model){
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	String usuario = auth.getName();
    	String puesto = this.puestServico.BuscarPuesto(usuario);
    	
	    	model.addAttribute("puesto",puesto);
			model.addAttribute("name",usuario);
	    
	    	if(puesto.equals("administrador")){
	    		return new ModelAndView("redirect:/prueba").addObject(model);
	    	}
	    	else{
	    		return new ModelAndView("prueba").addObject(model);
	    	}
	    	
    }
    
}
