package pro.solid.inv.controlador;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pro.solid.inv.componente.Paginacion;
import pro.solid.inv.config.PaginaConfig;
import pro.solid.inv.modelo.Inventario;
import pro.solid.inv.modelo.Preparacion;
import pro.solid.inv.modelo.PreparacionDetalleSalida;
import pro.solid.inv.modelo.RegistroSalida;
import pro.solid.inv.servicio.InventarioServicio;
import pro.solid.inv.servicio.PreparacionDetalleSalidaServicio;
import pro.solid.inv.servicio.PuestoServicio;
import pro.solid.inv.servicio.RegistroSalidaServicio;
import pro.solid.inv.servicio.SalidaServicio;
import pro.solid.inv.servicio.UsuarioServicio;

@Controller
@RequestMapping("/salida")
public class SalidaControllador{

	@Autowired
	private SalidaServicio salidaServicio;
	
	@Autowired
	private PuestoServicio puestServico;
	
	@Autowired
	private UsuarioServicio usuarioServicio;
	
	@Autowired
	private PreparacionDetalleSalidaServicio preparacionDetaleSalidaServicio;
	
	@Autowired
	private InventarioServicio inventarioServicio;

    @GetMapping
    public ModelAndView inicio(HttpServletRequest request,Model model){
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	model.addAttribute("puesto",this.puestServico.BuscarPuesto(auth.getName()));
		model.addAttribute("name",auth.getName());
		model.addAttribute("salida",true);
		
		int page = 0; //default page number is 0 (yes it is weird)
        int size = 8; //default page size is 10
        
        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }
        
		Page<Preparacion> listaHojas=this.salidaServicio.JPQLConsultaListaUsuario(this.usuarioServicio.ConsultaId(auth.getName()),PageRequest.of(page,size));
		
		Paginacion paginacion = new Paginacion(listaHojas.getNumber()+1,listaHojas.getTotalPages());
		
		model.addAttribute("paginacion",listaHojas);
		model.addAttribute("listaHojas",listaHojas);
		model.addAttribute("PaginasTotales",listaHojas.getTotalPages());
        model.addAttribute("BotonLimtSup", paginacion.isBotonLimtSup());
        model.addAttribute("BotonLimtInf", paginacion.isBotonLimtInf());
        model.addAttribute("LimitPagSup",paginacion.getLimiteSuperior());
        model.addAttribute("LimitPagInf",paginacion.getLimiteInferior());
	
    	ModelAndView modelandview= new ModelAndView(PaginaConfig.Salida);

        return modelandview.addObject(model);
    }
    
    @GetMapping("/{id}")
    public ModelAndView listaSalida(HttpServletRequest request,Model model,@PathVariable("id") long id){

    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	ModelAndView modelandview;

		if(this.salidaServicio.JDBConsultaIdUsuarioPreparacion(this.usuarioServicio.ConsultaId(auth.getName()),id)==id){

			model.addAttribute("puesto",this.puestServico.BuscarPuesto(auth.getName()));
			model.addAttribute("name",auth.getName());
			model.addAttribute("salida",true);
	    	
			int page = 0;
	        int size = 6;
	        
	        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
	            page = Integer.parseInt(request.getParameter("page")) - 1;
	        }

	        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
	            size = Integer.parseInt(request.getParameter("size"));
	        }
	    	
	        Paginacion paginacion = new Paginacion();
	        paginacion.setTotalItems(this.salidaServicio.JPQLConsultaTotalItemsDetalleSalida(id));
	        paginacion.PaginasTotales(size,paginacion.getTotalItems());
	        paginacion.GenerarLimites(page+1,Integer.valueOf(paginacion.getPaginasTotales().toString()));
	        
	        paginacion.setPaginaActual((long)(page));
	        
	        if(page>paginacion.getPaginasTotales()){
	        	modelandview = new ModelAndView("redirect:/salida/"+id+"?page="+paginacion.getPaginasTotales());
	        }else{
	        	modelandview = new ModelAndView(PaginaConfig.ListadoSalida);
	        }
	        
	        model.addAttribute("preparacion",this.salidaServicio.ConsultaEncabezadoDetalle(id));
	        model.addAttribute("preparaciondetalle",this.salidaServicio.JDBConsultaDetalleLista(id,Long.valueOf(size),Long.valueOf(page*size)));
	        
	        model.addAttribute("paginacion",paginacion);
			model.addAttribute("PaginasTotales",paginacion.getPaginasTotales());
			model.addAttribute("PaginaActual",paginacion.getPaginaActual()+1);
	        model.addAttribute("BotonLimtSup", paginacion.isBotonLimtSup());
	        model.addAttribute("BotonLimtInf", paginacion.isBotonLimtInf());
	        model.addAttribute("LimitPagSup",paginacion.getLimiteSuperior());
	        model.addAttribute("LimitPagInf",paginacion.getLimiteInferior());
	        model.addAttribute("Idpreparacion",id);
			
		}else{
			modelandview = new ModelAndView("error_uthentificion");
		}
		        
        return modelandview.addObject(model);
    }
    
    @Autowired
    private RegistroSalidaServicio registroSalidaServicio; 
    
    @GetMapping("/descargar/{Idpreparacion}/{id}")
    public ModelAndView descargarProducto(@PathVariable("Idpreparacion") long idpreparacion,@PathVariable("id") long id,HttpServletRequest request,Model model){
    	
    	int page = 0;
    	ModelAndView modelandview;
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	Long id_usuario = this.usuarioServicio.ConsultaId(auth.getName());
    	if(this.salidaServicio.JDBConsultaIdUsuarioPreparacion(id_usuario,idpreparacion)==idpreparacion){
    		
            if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
                page = Integer.parseInt(request.getParameter("page")) - 1;
            }
            
            PreparacionDetalleSalida prepaDetaSali = this.preparacionDetaleSalidaServicio.ConsultarUno(id);
            Inventario inventario = this.inventarioServicio.ConsultaUno(prepaDetaSali.getId_inventario());
            Long total=inventario.getCantidad()-inventario.getReserva()+prepaDetaSali.getCantidad();
            
        	inventario.setCantidad(inventario.getCantidad()-prepaDetaSali.getCantidad());
        	inventario.setReserva(inventario.getReserva()-prepaDetaSali.getCantidad());
            
            if(total>prepaDetaSali.getCantidad()||total==prepaDetaSali.getCantidad()){
            	inventario.setHabilitado(true);
            }else{
            	inventario.setHabilitado(false);
            	inventario.setLote("");
            }

            inventarioServicio.Insercion(inventario);
            RegistroSalida registroSalida= new RegistroSalida();
            registroSalida.setId_usuario(id_usuario);
            registroSalida.setId_preparacion(prepaDetaSali.getId_preparacion());
            registroSalida.setId_producto(prepaDetaSali.getId_producto());
            registroSalida.setId_ubicacion(prepaDetaSali.getId_ubicacion());
            registroSalida.setCantidad(prepaDetaSali.getCantidad());
            registroSalida.setLote(prepaDetaSali.getLote());
            
            this.salidaServicio.ActulizarEstadoPreparacionDetalleSalida(prepaDetaSali.getId());
            this.registroSalidaServicio.Insertar(registroSalida);
            
    		modelandview = new ModelAndView("redirect:/salida/"+idpreparacion+"?page="+(page+1));
    	}else{	
    		modelandview = new ModelAndView("error_uthentificion");
		}
        
    	
        return modelandview.addObject(model);
    }
	
}
