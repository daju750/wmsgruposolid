package pro.solid.inv.controlador;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pro.solid.inv.componente.Paginacion;
import pro.solid.inv.servicio.InventarioServicio;

@Controller
@RequestMapping("/admin/inventario")
public class AdminInventario {

	@Autowired
	private InventarioServicio inventarioServicio;
	
	@GetMapping
    public ModelAndView inicio(Model model,HttpServletRequest request){
		
		int page = 0; //default page number is 0 (yes it is weird)
        int size = 10; //default page size is 10	
		
        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }
        
        Paginacion paginacion = new Paginacion();
        paginacion.setPaginaActual((long)(page));
        paginacion.setTotalItems(this.inventarioServicio.JDBContarInventarioGeneral());
        paginacion.GenerarLimites(page+1,paginacion.PaginasTotales(size,paginacion.getTotalItems()).intValue());
        
		model.addAttribute("inventarioGeneral",this.inventarioServicio.JDBConsultarInventarioGeneral(size,(long) (page*size)));
		ModelAndView modelandview = new ModelAndView("Administrador/inventario");
		
		model.addAttribute("PaginasTotales",paginacion.getPaginasTotales());
		model.addAttribute("PaginaActual",paginacion.getPaginaActual()+1);
        model.addAttribute("BotonLimtSup", paginacion.isBotonLimtSup());
        model.addAttribute("BotonLimtInf", paginacion.isBotonLimtInf());
        model.addAttribute("LimitPagSup",paginacion.getLimiteSuperior());
        model.addAttribute("LimitPagInf",paginacion.getLimiteInferior());
		
		return modelandview;
	}
	
}
