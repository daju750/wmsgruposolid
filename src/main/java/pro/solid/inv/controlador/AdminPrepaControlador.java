package pro.solid.inv.controlador;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pro.solid.inv.JDBCObject.JDBCListasPreparacionAdmin;
import pro.solid.inv.JDBCObject.JDBCPreparacionSalidaEliminar;
import pro.solid.inv.componente.ExcelApachePoi;
import pro.solid.inv.componente.Paginacion;
import pro.solid.inv.config.PaginaConfig;
import pro.solid.inv.formularios.FormDetallePreparacion;
import pro.solid.inv.formularios.FormIngreso;
import pro.solid.inv.formularios.FormPreparacion;
import pro.solid.inv.interesp.ExcelPreparacionSalida;
import pro.solid.inv.modelo.Preparacion;
import pro.solid.inv.modelo.PreparacionDetalle;
import pro.solid.inv.modelo.PreparacionDetalleSalida;
import pro.solid.inv.modelo.PreparacionError;
import pro.solid.inv.servicio.CompuertaServicio;
import pro.solid.inv.servicio.InventarioServicio;
import pro.solid.inv.servicio.PreparacionDetalleSalidaServicio;
import pro.solid.inv.servicio.PreparacionDetalleServicio;
import pro.solid.inv.servicio.PreparacionErrorServicio;
import pro.solid.inv.servicio.PreparacionServicio;
import pro.solid.inv.servicio.RutaServicio;
import pro.solid.inv.servicio.UsuarioServicio;

@Controller
@RequestMapping("/admin/preparacion")
public class AdminPrepaControlador {

	@Autowired
	private CompuertaServicio puertaServicio;
	
	@Autowired
	private PreparacionServicio preparacionServicio;
	
	@Autowired
	private RutaServicio rutaServicio;
	
	@Autowired
	private UsuarioServicio usuarioServicio;
	
	@Autowired
	private CompuertaServicio compueraServicio;
	
	@Autowired
	private InventarioServicio inventarioServicio;
	
	@Autowired
	private PreparacionDetalleServicio PreparaciondetalleServicio;
	
	@Autowired
	private PreparacionDetalleSalidaServicio preparacionsalida;
	
	@GetMapping("/borrar/{id}")
    public ModelAndView borrarPreparacion(@PathVariable("id") long id,Model model,HttpServletRequest request){

		List<JDBCPreparacionSalidaEliminar> lista=this.preparacionServicio.JDBConsultaReservas(id);
		if(lista!=null){
			for(JDBCPreparacionSalidaEliminar fila:lista) {
				this.inventarioServicio.JPQLQuitarReservar(fila.getId_inventario(),fila.getCantidad());
			}
			this.preparacionServicio.JPQLBorrar(id);
		} 		
		ModelAndView modelandview = new ModelAndView("redirect:/admin/preparacion"); 
		
		return modelandview;
	}
	
	@GetMapping
    public ModelAndView inicio(Model model,HttpServletRequest request){
    	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		ModelAndView modelandview = new ModelAndView(PaginaConfig.AdminPreparacion);

		int page = 0; //default page number is 0 (yes it is weird)
        int size = 10; //default page size is 10
		
        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }
		
        Page<Object> paginacionPreparacion = this.preparacionServicio.JPQLConsultaPaginacionAdmin(this.usuarioServicio.ConsultaId(auth.getName()),PageRequest.of(page,size));
        Paginacion paginacion = new Paginacion(paginacionPreparacion.getNumber()+1,paginacionPreparacion.getTotalPages());
        model.addAttribute("paginacion",paginacionPreparacion);
		model.addAttribute("PaginasTotales",paginacionPreparacion.getTotalPages());
        model.addAttribute("BotonLimtSup", paginacion.isBotonLimtSup());
        model.addAttribute("BotonLimtInf", paginacion.isBotonLimtInf());
        model.addAttribute("LimitPagSup",paginacion.getLimiteSuperior());
        model.addAttribute("LimitPagInf",paginacion.getLimiteInferior());
        
        String ids="";
        
        for(int i=0;i<paginacionPreparacion.getTotalElements();i++){
        	if(paginacionPreparacion.getContent().get(i)!=""){
        	ids+="CAST('"+paginacionPreparacion.getContent().get(i)+"' AS INTEGER),";
        	}
        }	
        List<JDBCListasPreparacionAdmin> listaPreparacion = null;
        if(ids!=""){
    		listaPreparacion = this.preparacionServicio.JDBConsultaListaAdmin(this.usuarioServicio.ConsultaId(auth.getName()),ids.substring(0,ids.length()-1));
        }
        
        model.addAttribute("listapreparacion",listaPreparacion);
		return modelandview.addObject(model);
    }

	@GetMapping("/nueva")
    public ModelAndView NuevaHoja(Model model,FormPreparacion formPreparacion){		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		model.addAttribute("nombre",auth.getName());
		model.addAttribute("compuertas",this.puertaServicio.ConsultaCompuertasDomo((long)1));
		model.addAttribute("empleados",this.usuarioServicio.JDBConsultaGrupoUsuarios("pikeador"));
		model.addAttribute("rutas",this.rutaServicio.ConsultaTodasRutas());
		
		ModelAndView modelandview = new ModelAndView(PaginaConfig.AdminPreparacionNueva);
		
		return modelandview.addObject(model);
    }

	
	@PostMapping("/nueva")
	public ModelAndView PostPreparacion(@RequestParam("file") MultipartFile reapExcelDataFile,FormPreparacion formPreparacion,Model model,RedirectAttributes redirectAttributes) throws IOException{
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());

		Preparacion preparacion = new Preparacion();
		preparacion.setId_usuario(formPreparacion.getUsuario());
		preparacion.setId_administrador(this.usuarioServicio.ConsultaId(auth.getName()));
		preparacion.setId_compuerta(formPreparacion.getCompuerta());
		preparacion.setId_ruta(formPreparacion.getRuta());
		preparacion.setGenerado(false);
		preparacion.setObservacion(formPreparacion.getObservacion());
		
		String sqlInsercionPreparacion = "insert into preparacion_detalle(id_preparacion,codigosap,cantidad) values";
		String sqlInsercionPreparacionSalida = "insert into preparacion_salida(id_preparacion,id_inventario,id_ubicacion,id_producto,lote,cantidad) values";
		String sqlInsercionErrores = "insert into preparacion_error(id_preparacion,descripcion) values";
		
		@SuppressWarnings("resource")
		XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
	    XSSFSheet worksheet = workbook.getSheetAt(0);
	    
	    ExcelApachePoi excel= new ExcelApachePoi();
	    
	    for(int i=1;i<worksheet.getPhysicalNumberOfRows() ;i++){
	    	XSSFRow row = worksheet.getRow(i);

	    	String codigosap = excel.getCellValue(row.getCell(0)).trim();
	    	String srtcantidad = excel.getCellValue(row.getCell(1)).trim();
	    		
	    	if(!codigosap.equals("") && !srtcantidad.equals("")){
	    	
	    		Integer cantidad = Integer.parseInt(srtcantidad.replace(".0","")); 
	    		String cadena = this.PreparaciondetalleServicio.JDBProcedimientoIdsexcelpreparacion(codigosap, cantidad);
	    		if(!cadena.equals("")){//Producto Encontrado Correctamente
	    			String[] split = cadena.split(":");
	    			int contador_total=0;
	    			System.out.println("Largo Split:"+split.length);
	    			//Hasta aqui se genera la hoja
	    			if(preparacion.isGenerado()==false){
	    				preparacion.setId(this.preparacionServicio.JDBConsultaIdMax()+1);
	    				this.preparacionServicio.Insertar(preparacion);
	    				preparacion.setGenerado(true);}
	    			
	    			sqlInsercionPreparacion+="("+preparacion.getId()+",'"+codigosap+"',"+cantidad+"),";
	    			
	    			for(String fila : split){
	    				String[] columna = fila.split(",");
	    				//insert into preparacion_salida(id_preparacion,id_ubicacion,id_producto,lote,cantidad) values
	    				sqlInsercionPreparacionSalida+="("+preparacion.getId()+","+columna[0]+","+columna[1]+","+columna[2]+","+columna[3];
	    				
		    				if(Integer.parseInt(columna[4])<cantidad || Integer.parseInt(columna[4])==cantidad){
		    					sqlInsercionPreparacionSalida+=","+columna[4]+"),";
		    					this.PreparaciondetalleServicio.JDBCTemplateInsertDetallePreparacion("update inventario set reserva="+columna[4]+",habilitado=false where id="+columna[0]);
		    					//System.out.println("Cantidad Inventario "+columna[4]+" Solicitada:"+cantidad+" Mayor o Igual Habiliado falso");
		    				}else{
		    					sqlInsercionPreparacionSalida+=","+cantidad+"),";
		    					this.PreparaciondetalleServicio.JDBCTemplateInsertDetallePreparacion("update inventario set reserva=reserva+"+cantidad+" where id="+columna[0]);
		    					//System.out.println("Cantidad Inventario "+columna[4]+" Solicitada:"+cantidad+" Y menor");
		    				}
		    				cantidad-=Integer.parseInt(columna[4]);
			    				contador_total+=1;
			    				if(split.length==contador_total && cantidad>0){
			    					sqlInsercionErrores+="("+preparacion.getId()+",'No se encontro suficiente cantidad con el codigoSap:"+codigosap+" con "+cantidad+" unidades faltantes'),";
			    				}
	    			} 
	    			
	    			contador_total=0;
	    		}else{//producto no encontrado en la base de datos
	    			sqlInsercionErrores+="("+preparacion.getId()+",'No se encontro el codigoSap:"+codigosap+" con unidades:"+cantidad+"'),";
	    		}
	    		
	    	}	
	    }
	    
	    this.PreparaciondetalleServicio.JDBCTemplateInsertDetallePreparacion(sqlInsercionPreparacion.substring(0,sqlInsercionPreparacion.length()-1));
		this.PreparaciondetalleServicio.JDBCTemplateInsertDetallePreparacion(sqlInsercionPreparacionSalida.substring(0,sqlInsercionPreparacionSalida.length()-1)); 
		if(sqlInsercionErrores.length()>65){this.PreparaciondetalleServicio.JDBCTemplateInsertDetallePreparacion(sqlInsercionErrores.substring(0,sqlInsercionErrores.length()-1));}
		
		return new ModelAndView("redirect:/admin/preparacion/registro/"+preparacion.getId());
	}
	
	
	@Autowired
	private PreparacionErrorServicio preparacionErrorServicio;
	
	@GetMapping("/registro/{id}")
    public ModelAndView PreparacionRegistro(@PathVariable("id") long id,Model model,HttpServletRequest request){
    	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		Preparacion preparacion = this.preparacionServicio.ConsultaPreparacion(id);
		ModelAndView modelandview;
		
		if(preparacion.getId_administrador()==this.usuarioServicio.ConsultaId(auth.getName())){
			
			int page = 0; //default page number is 0 (yes it is weird)
	        int size = 4; //default page size is 10
			
	        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
	            page = Integer.parseInt(request.getParameter("page")) - 1;
	        }
	        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
	            size = Integer.parseInt(request.getParameter("size"));
	        }
			
	        model.addAttribute("formPreparacion",new FormPreparacion());
	        model.addAttribute("formIngreso",new FormIngreso());
	        boolean posteado = preparacion.isPosteado();
	        model.addAttribute("id",id);
	        model.addAttribute("posteado",posteado);
	        
	        if(posteado== true){

	        	model.addAttribute("compuerta",this.compueraServicio.ConsultaCompuerta(preparacion.getId_compuerta()));
	        	model.addAttribute("usuario",this.usuarioServicio.usuario(preparacion.getId_usuario()));
	        	model.addAttribute("ruta",this.rutaServicio.ConsultaDondeUnicoId(preparacion.getId_ruta()));
	        	model.addAttribute("frompreparaciondetalle",new FormDetallePreparacion());
	        }else{
	        	model.addAttribute("compuertas",this.puertaServicio.JDBConsultaPrimeroUnionTodos(preparacion.getId_compuerta()));
		        model.addAttribute("usuarios",this.usuarioServicio.JDBConsultaPrimeroUnionTodos(preparacion.getId_usuario(),"pikeador"));
		        model.addAttribute("rutas",this.rutaServicio.JDBConsultaPrimeroUnionTodos(preparacion.getId_ruta()));
	        }
	        
	        model.addAttribute("fecha",preparacion.getFecha());
	        
	        Paginacion paginacion = new Paginacion();
	        paginacion.setPaginaActual((long)(page));
	        paginacion.setTotalItems(this.PreparaciondetalleServicio.JDBConsultaCountIDetalle(preparacion.getId()));
	        paginacion.GenerarLimites(page+1,paginacion.PaginasTotales(size,paginacion.getTotalItems()).intValue());
	        
	        if(page>paginacion.getPaginasTotales()){
	        	modelandview = new ModelAndView("redirect:/admin/preparacion/registro/"+id+"?page="+paginacion.getPaginasTotales());
	        }else{
	        	modelandview = new ModelAndView(PaginaConfig.AdminRegisroDetallePreparacion);
	        }
	        
	        List<PreparacionError> preparacionErrores = this.preparacionErrorServicio.ConsultaAdminListaError(id);
	    	model.addAttribute("preparacionErrores",preparacionErrores);
	        
	        List<PreparacionDetalle> preparacionDetalle = this.PreparaciondetalleServicio.JPQLPaginacionAdmin(preparacion.getId(),size,page*size);
	        model.addAttribute("preparacionDetalle",preparacionDetalle);
	        
	        model.addAttribute("formprepaDeta",new PreparacionDetalle());
	        
			model.addAttribute("PaginasTotales",paginacion.getPaginasTotales());
			model.addAttribute("idPreparacion",preparacion.getId());
			model.addAttribute("PaginaActual",paginacion.getPaginaActual()+1);
	        model.addAttribute("BotonLimtSup", paginacion.isBotonLimtSup());
	        model.addAttribute("BotonLimtInf", paginacion.isBotonLimtInf());
	        model.addAttribute("LimitPagSup",paginacion.getLimiteSuperior());
	        model.addAttribute("LimitPagInf",paginacion.getLimiteInferior());
	             
		}else{			
			modelandview = new ModelAndView("error_uthentificion");
		}

        return modelandview.addObject(model);
    }
	
	@PostMapping("/registro/{id}")
    public ModelAndView GuardarpreparacionRegistroDetalle(@PathVariable("id") long id,Model model,FormPreparacion formpreparacion){
    	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("nombre",auth.getName());
		
		Preparacion preparacion = new Preparacion();
		preparacion.setId_ruta(formpreparacion.getRuta());
		preparacion.setId_usuario(formpreparacion.getUsuario());
		preparacion.setId_compuerta(formpreparacion.getCompuerta());
		preparacion.setId(id);
		
		this.preparacionServicio.JDBCActulizar(preparacion);

		return new ModelAndView("redirect:/admin/preparacion/registro/"+id);
    }

	
	@PostMapping("/detalle/eliminar/{id}")
    public ModelAndView ElimnarDetallePreparacion(@PathVariable("id") long id,Model model){
		
		PreparacionDetalle preparaciondetalle = this.PreparaciondetalleServicio.ConsultaUno(id);

		String ids="";
		List<PreparacionDetalleSalida> preparacionsalida = this.preparacionsalida.JDBConsultaIdIDIventarioCantidad(preparaciondetalle.getId_preparacion(),preparaciondetalle.getCodigosap());
		
		for(PreparacionDetalleSalida fila:preparacionsalida){
			ids+=fila.getId()+",";
			this.inventarioServicio.JDBCQuitarReservas(fila.getId_inventario(),fila.getCantidad());
		}
		
		this.preparacionsalida.JDBCEliminarIdin(ids.subSequence(0,ids.length()-1).toString());
		this.PreparaciondetalleServicio.Eliminar(id);
		return new ModelAndView("redirect:/admin/preparacion/registro/"+preparaciondetalle.getId_preparacion());
	}
	
	
	
	@PostMapping("/registro/agregar/{id}")
    public ModelAndView AgregarPreparacionDetalle(@PathVariable("id") long id,Model model,FormIngreso formingreso,RedirectAttributes redirectAttributes){
		
		try {
		
		if(this.PreparaciondetalleServicio.JDBCExisteRegistro(id,formingreso.getCodigo())==false){

			String cadena = this.PreparaciondetalleServicio.JDBProcedimientoIdsexcelpreparacion(formingreso.getCodigo(),Integer.parseInt(formingreso.getCantidad()));
			String[] split = cadena.split(":");
			Integer cantidad =Integer.parseInt(formingreso.getCantidad());
			int contador_total=0;
			
			String sqlInsercionPreparacion = "insert into preparacion_detalle(id_preparacion,codigosap,cantidad) values("+id+",'"+formingreso.getCodigo()+"',"+cantidad+")";
			String sqlInsercionPreparacionSalida = "insert into preparacion_salida(id_preparacion,id_inventario,id_ubicacion,id_producto,lote,cantidad) values";
			String sqlInsercionErrores = "insert into preparacion_error(id_preparacion,descripcion) values";
			
			for(String fila : split){
				String[] columna = fila.split(",");
				//insert into preparacion_salida(id_preparacion,id_ubicacion,id_producto,lote,cantidad) values
				sqlInsercionPreparacionSalida+="("+id+","+columna[0]+","+columna[1]+","+columna[2]+","+columna[3];
				
    				if(Integer.parseInt(columna[4])<cantidad || Integer.parseInt(columna[4])==cantidad){
    					sqlInsercionPreparacionSalida+=","+columna[4]+"),";
    					this.PreparaciondetalleServicio.JDBCTemplateInsertDetallePreparacion("update inventario set reserva="+columna[4]+",habilitado=false where id="+columna[0]);
    					//System.out.println("Cantidad Inventario "+columna[4]+" Solicitada:"+cantidad+" Mayor o Igual Habiliado falso");
    				}else{
    					sqlInsercionPreparacionSalida+=","+cantidad+"),";
    					this.PreparaciondetalleServicio.JDBCTemplateInsertDetallePreparacion("update inventario set reserva=reserva+"+cantidad+" where id="+columna[0]);
    					//System.out.println("Cantidad Inventario "+columna[4]+" Solicitada:"+cantidad+" Y menor");
    				}
    				cantidad-=Integer.parseInt(columna[4]);
	    				contador_total+=1;
	    				if(split.length==contador_total && cantidad>0){
	    					sqlInsercionErrores+="("+id+",'No se encontro suficiente cantidad con el codigoSap:"+formingreso.getCodigo()+" con "+cantidad+" unidades faltantes'),";
	    				}
			} 
			
			this.PreparaciondetalleServicio.JDBCTemplateInsertDetallePreparacion(sqlInsercionPreparacion);
			this.PreparaciondetalleServicio.JDBCTemplateInsertDetallePreparacion(sqlInsercionPreparacionSalida.substring(0,sqlInsercionPreparacionSalida.length()-1)); 
			if(sqlInsercionErrores.length()>65){this.PreparaciondetalleServicio.JDBCTemplateInsertDetallePreparacion(sqlInsercionErrores.substring(0,sqlInsercionErrores.length()-1));}
			
		}else {
			redirectAttributes.addFlashAttribute("existeproducto", "Ya existe");
		}
		
		}catch(Exception ex) {redirectAttributes.addFlashAttribute("erroragregar", "Producto no encontrado");}
		
		return new ModelAndView("redirect:/admin/preparacion/registro/"+id);
	}
	
	
	@GetMapping("/descargar/excel/{id}")
	public ResponseEntity<StreamingResponseBody> excel(@PathVariable("id") long id,Model model) {
	
	List<PreparacionDetalle> preparaciondetalle = this.PreparaciondetalleServicio.JDBConsultaPreparacionCodigoCantidad(id);
	List<PreparacionError> preparacionErrores = this.preparacionErrorServicio.ConsultaAdminListaError(id);
	List<ExcelPreparacionSalida> preparaciondetasalida = this.preparacionsalida.JDBCExcelPrepaSalida(id);
	
	  @SuppressWarnings("resource")
	  Workbook workBook = new XSSFWorkbook();
	  Sheet sheetAdmin = workBook.createSheet("Hoja Administrador");
	  sheetAdmin.setColumnWidth(0, 1000);
	  sheetAdmin.setColumnWidth(1, 4500);
	  sheetAdmin.setColumnWidth(2, 3560);
	  Row rowadmin = sheetAdmin.createRow(0);
	  rowadmin.createCell(0).setCellValue("#");
	  rowadmin.createCell(1).setCellValue("CodigoSAP");
	  rowadmin.createCell(2).setCellValue("Cantidad");
	  
	  int a=1;
	  for(PreparacionDetalle prepa:preparaciondetalle){
		  rowadmin = sheetAdmin.createRow(a);
		  	rowadmin.createCell(0).setCellValue(a);
		  	rowadmin.createCell(1).setCellValue(prepa.getCodigosap());
		  	rowadmin.createCell(2).setCellValue(prepa.getCantidad());
		  	a+=1;
	  }
	  
	  
	  Sheet sheetSalida = workBook.createSheet("Hoja Pikedo");
	  sheetSalida.setColumnWidth(0, 1000);
	  sheetSalida.setColumnWidth(1, 4560);
	  sheetSalida.setColumnWidth(2, 2560);
	  sheetSalida.setColumnWidth(3, 4800);
	  sheetSalida.setColumnWidth(4, 3560);
	  
	  Row rowsalida = sheetSalida.createRow(0);
	  rowsalida.createCell(0).setCellValue("#");
	  rowsalida.createCell(1).setCellValue("CodigoSAP");
	  rowsalida.createCell(2).setCellValue("Ubicacion");
	  rowsalida.createCell(3).setCellValue("Lote");
	  rowsalida.createCell(4).setCellValue("Cantidad");
	  
	  a=1;
	  for(ExcelPreparacionSalida prepa:preparaciondetasalida){
		  rowsalida = sheetSalida.createRow(a);
		  	rowsalida.createCell(0).setCellValue(a);	
		  	rowsalida.createCell(1).setCellValue(prepa.getCodigosap());
		  	rowsalida.createCell(2).setCellValue(prepa.getUbicacion());
		  	rowsalida.createCell(3).setCellValue(prepa.getLote());
		  	rowsalida.createCell(4).setCellValue(prepa.getCantidad());
		  	a+=1;
	  }
	  
	  
	  Sheet sheeterror = workBook.createSheet("Errores");
	  sheeterror.setColumnWidth(0,1000);
	  sheeterror.setColumnWidth(1,15000);
	  Row rowerror = sheeterror.createRow(0);
	  rowerror.createCell(0).setCellValue("#");
	  rowerror.createCell(1).setCellValue("Descripcion");
	  
	  a=1;
	  for(PreparacionError prepa:preparacionErrores){
		  rowerror = sheeterror.createRow(a);
		  rowerror.createCell(0).setCellValue(a);
		  rowerror.createCell(1).setCellValue(prepa.getDescripcion());
		  	a+=1;
	  }
	  
	  
	  return ResponseEntity
	    .ok()
	    .contentType(MediaType.APPLICATION_OCTET_STREAM)
	    .header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=\"Preparacion Completa.xlsx\"")
	    .body(workBook::write);
	}
	
	
}
