package pro.solid.inv.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import pro.solid.inv.modelo.Usuario;
import pro.solid.inv.servicio.SeguridadServicio;
import pro.solid.inv.servicio.UsuarioServicio;
import pro.solid.inv.validador.UsuarioValidador;

import pro.solid.inv.config.PaginaConfig;

@Controller
public class LoginControlador{
	
    @Autowired
    private UsuarioServicio usuarioServicio;

    @Autowired
    private SeguridadServicio seguridadServicio;

    @Autowired
    private UsuarioValidador usuarioValidador;

    @GetMapping("/resgitrar_usuario")
    public ModelAndView registration(Model model) {
        return new ModelAndView("Login/registrar","usuario", new Usuario());
    }

    @PostMapping("/resgitrar_usuario")
    public String registration(Usuario userForm, BindingResult bindingResult) {
        usuarioValidador.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "redirect:/resgitrar_usuario";
        }

        usuarioServicio.save(userForm);
        seguridadServicio.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/";
    }

    @GetMapping("/login")
    public ModelAndView login(Model model, String error, String logout) {
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    	if(auth.getName().equals("anonymousUser")){
    		
        	Usuario usuario = new Usuario();
        	model.addAttribute(usuario);
        	
        	if (error != null)
                model.addAttribute("error", "Usuario y Contrasena Invalidos");

            if (logout != null)
                model.addAttribute("message", "Se ha deslogeado con exito.");

            return new ModelAndView(PaginaConfig.Login).addObject(model);
    	}else{
    		return new ModelAndView("redirect:/");
    	}
    	
    }

}
