package pro.solid.inv.JDBCObject;

public class JDBCPreparacionSalidaEliminar {

	private Long id_inventario;
	private Long cantidad;
	
	public JDBCPreparacionSalidaEliminar() {}
	
	public JDBCPreparacionSalidaEliminar(Long id_inventario, Long cantidad) {
		super();
		this.id_inventario = id_inventario;
		this.cantidad = cantidad;
	}
	
	public Long getId_inventario() {
		return id_inventario;
	}

	public void setId_inventario(Long id_inventario) {
		this.id_inventario = id_inventario;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cantidad == null) ? 0 : cantidad.hashCode());
		result = prime * result + ((id_inventario == null) ? 0 : id_inventario.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JDBCPreparacionSalidaEliminar other = (JDBCPreparacionSalidaEliminar) obj;
		if (cantidad == null) {
			if (other.cantidad != null)
				return false;
		} else if (!cantidad.equals(other.cantidad))
			return false;
		if (id_inventario == null) {
			if (other.id_inventario != null)
				return false;
		} else if (!id_inventario.equals(other.id_inventario))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "JDBCPreparacionSalidaEliminar [id_inventario=" + id_inventario + ", cantidad=" + cantidad + "]";
	}

}
