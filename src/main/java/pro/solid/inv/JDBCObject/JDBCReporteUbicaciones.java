package pro.solid.inv.JDBCObject;

public class JDBCReporteUbicaciones {

	private String codigosap;
	private Long ubicacion;
	private Long cantidad;
	private Long reserva;
	private String lote;

	public JDBCReporteUbicaciones(){}

	public JDBCReporteUbicaciones(String codigosap, Long ubicacion, Long cantidad, Long reserva, String lote) {
		super();
		this.codigosap = codigosap;
		this.ubicacion = ubicacion;
		this.cantidad = cantidad;
		this.reserva = reserva;
		this.lote = lote;
	}

	public String getCodigosap() {
		return codigosap;
	}


	public void setCodigosap(String codigosap) {
		this.codigosap = codigosap;
	}


	public Long getUbicacion() {
		return ubicacion;
	}


	public void setUbicacion(Long ubicacion) {
		this.ubicacion = ubicacion;
	}


	public Long getCantidad() {
		return cantidad;
	}


	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}


	public Long getReserva() {
		return reserva;
	}


	public void setReserva(Long reserva) {
		this.reserva = reserva;
	}


	public String getLote() {
		return lote;
	}


	public void setLote(String lote) {
		this.lote = lote;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cantidad == null) ? 0 : cantidad.hashCode());
		result = prime * result + ((codigosap == null) ? 0 : codigosap.hashCode());
		result = prime * result + ((lote == null) ? 0 : lote.hashCode());
		result = prime * result + ((reserva == null) ? 0 : reserva.hashCode());
		result = prime * result + ((ubicacion == null) ? 0 : ubicacion.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JDBCReporteUbicaciones other = (JDBCReporteUbicaciones) obj;
		if (cantidad == null) {
			if (other.cantidad != null)
				return false;
		} else if (!cantidad.equals(other.cantidad))
			return false;
		if (codigosap == null) {
			if (other.codigosap != null)
				return false;
		} else if (!codigosap.equals(other.codigosap))
			return false;
		if (lote == null) {
			if (other.lote != null)
				return false;
		} else if (!lote.equals(other.lote))
			return false;
		if (reserva == null) {
			if (other.reserva != null)
				return false;
		} else if (!reserva.equals(other.reserva))
			return false;
		if (ubicacion == null) {
			if (other.ubicacion != null)
				return false;
		} else if (!ubicacion.equals(other.ubicacion))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JDBCReporteUbicaciones [codigosap=" + codigosap + ", ubicacion=" + ubicacion + ", cantidad=" + cantidad
				+ ", reserva=" + reserva + ", lote=" + lote + "]";
	}
	
}
