package pro.solid.inv.JDBCObject;

public class JDBCDetalleListaSalida {

	private Long id;
	private String codigosap;
	private String ubicacion;
	private String lote;
	private Long cantidad;
	private boolean estado;
	
	public JDBCDetalleListaSalida() {}
	
	public JDBCDetalleListaSalida(Long id, String codigosap, String ubicacion, String lote, Long cantidad,
			boolean estado) {
		super();
		this.id = id;
		this.codigosap = codigosap;
		this.ubicacion = ubicacion;
		this.lote = lote;
		this.cantidad = cantidad;
		this.estado = estado;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigosap() {
		return codigosap;
	}

	public void setCodigosap(String codigosap) {
		this.codigosap = codigosap;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	
	
}
