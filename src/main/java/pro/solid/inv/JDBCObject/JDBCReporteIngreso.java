package pro.solid.inv.JDBCObject;

public class JDBCReporteIngreso {

	private String codigosap;
	private String ubicacion;
	private Integer cantidad;
	private String puerta;
	private String lote;
	private String fecha;
	
	public JDBCReporteIngreso() {}

	public JDBCReporteIngreso(String codigosap, String ubicacion, Integer cantidad, String puerta, String lote,
			String fecha) {
		super();
		this.codigosap = codigosap;
		this.ubicacion = ubicacion;
		this.cantidad = cantidad;
		this.puerta = puerta;
		this.lote = lote;
		this.fecha = fecha;
	}

	public String getCodigosap() {
		return codigosap;
	}

	public void setCodigosap(String codigosap) {
		this.codigosap = codigosap;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getPuerta() {
		return puerta;
	}

	public void setPuerta(String puerta) {
		this.puerta = puerta;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cantidad == null) ? 0 : cantidad.hashCode());
		result = prime * result + ((codigosap == null) ? 0 : codigosap.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((lote == null) ? 0 : lote.hashCode());
		result = prime * result + ((puerta == null) ? 0 : puerta.hashCode());
		result = prime * result + ((ubicacion == null) ? 0 : ubicacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JDBCReporteIngreso other = (JDBCReporteIngreso) obj;
		if (cantidad == null) {
			if (other.cantidad != null)
				return false;
		} else if (!cantidad.equals(other.cantidad))
			return false;
		if (codigosap == null) {
			if (other.codigosap != null)
				return false;
		} else if (!codigosap.equals(other.codigosap))
			return false;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (lote == null) {
			if (other.lote != null)
				return false;
		} else if (!lote.equals(other.lote))
			return false;
		if (puerta == null) {
			if (other.puerta != null)
				return false;
		} else if (!puerta.equals(other.puerta))
			return false;
		if (ubicacion == null) {
			if (other.ubicacion != null)
				return false;
		} else if (!ubicacion.equals(other.ubicacion))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JDBCReporteIngreso [codigosap=" + codigosap + ", ubicacion=" + ubicacion + ", cantidad=" + cantidad
				+ ", puerta=" + puerta + ", lote=" + lote + ", fecha=" + fecha + "]";
	}
	
	
	
}
