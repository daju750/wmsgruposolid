package pro.solid.inv.JDBCObject;

public class JDBCReporteTotalInventario {

	private String codigosap;
	private String descripcion;
	private Long total;
	private Long reserva;
	private Long neto;
	
	public JDBCReporteTotalInventario() {}
	
	public JDBCReporteTotalInventario(String codigosap, String descrpcion, Long total, Long reserva, Long neto) {
		super();
		this.codigosap = codigosap;
		this.descripcion = descrpcion;
		this.total = total;
		this.reserva = reserva;
		this.neto = neto;
	}

	public String getCodigosap() {
		return codigosap;
	}

	public void setCodigosap(String codigosap) {
		this.codigosap = codigosap;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getReserva() {
		return reserva;
	}

	public void setReserva(Long reserva) {
		this.reserva = reserva;
	}

	public Long getNeto() {
		return neto;
	}

	public void setNeto(Long neto) {
		this.neto = neto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigosap == null) ? 0 : codigosap.hashCode());
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((neto == null) ? 0 : neto.hashCode());
		result = prime * result + ((reserva == null) ? 0 : reserva.hashCode());
		result = prime * result + ((total == null) ? 0 : total.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JDBCReporteTotalInventario other = (JDBCReporteTotalInventario) obj;
		if (codigosap == null) {
			if (other.codigosap != null)
				return false;
		} else if (!codigosap.equals(other.codigosap))
			return false;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (neto == null) {
			if (other.neto != null)
				return false;
		} else if (!neto.equals(other.neto))
			return false;
		if (reserva == null) {
			if (other.reserva != null)
				return false;
		} else if (!reserva.equals(other.reserva))
			return false;
		if (total == null) {
			if (other.total != null)
				return false;
		} else if (!total.equals(other.total))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JDBCReporteTotalInventario [codigosap=" + codigosap + ", descripcion=" + descripcion + ", total="
				+ total + ", reserva=" + reserva + ", neto=" + neto + "]";
	}
	
}
