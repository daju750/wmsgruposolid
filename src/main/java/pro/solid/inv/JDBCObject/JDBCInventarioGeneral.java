package pro.solid.inv.JDBCObject;

public class JDBCInventarioGeneral {

	private int ubicaciones;
	private Long id;
	private String codigosap;
	private Long total;
	
	public JDBCInventarioGeneral() {}

	public JDBCInventarioGeneral(int ubicaciones, Long id, String codigosap, Long total) {
		super();
		this.ubicaciones = ubicaciones;
		this.id = id;
		this.codigosap = codigosap;
		this.total = total;
	}

	public int getUbicaciones() {
		return ubicaciones;
	}

	public void setUbicaciones(int ubicaciones) {
		this.ubicaciones = ubicaciones;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigosap() {
		return codigosap;
	}

	public void setCodigosap(String codigosap) {
		this.codigosap = codigosap;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigosap == null) ? 0 : codigosap.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((total == null) ? 0 : total.hashCode());
		result = prime * result + ubicaciones;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JDBCInventarioGeneral other = (JDBCInventarioGeneral) obj;
		if (codigosap == null) {
			if (other.codigosap != null)
				return false;
		} else if (!codigosap.equals(other.codigosap))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (total == null) {
			if (other.total != null)
				return false;
		} else if (!total.equals(other.total))
			return false;
		if (ubicaciones != other.ubicaciones)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JDBCInventarioGeneral [ubicaciones=" + ubicaciones + ", id=" + id + ", codigosap=" + codigosap
				+ ", total=" + total + "]";
	}
	
}
