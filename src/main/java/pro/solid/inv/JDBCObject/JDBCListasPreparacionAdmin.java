package pro.solid.inv.JDBCObject;

import java.util.Date;

public class JDBCListasPreparacionAdmin {

	private Long id;
	private String pikeador;
	private String puerta;
	private String ruta;
	private boolean posteado;
	private Date fecha;
	
	public JDBCListasPreparacionAdmin(){}

	public JDBCListasPreparacionAdmin(Long id, String pikeador, String puerta, String ruta, boolean posteado, Date fecha) {
		super();
		this.id = id;
		this.pikeador = pikeador;
		this.puerta = puerta;
		this.ruta = ruta;
		this.posteado = posteado;
		this.fecha = fecha;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPikeador() {
		return pikeador;
	}

	public void setPikeador(String pikeador) {
		this.pikeador = pikeador;
	}

	public String getPuerta() {
		return puerta;
	}

	public void setPuerta(String puerta) {
		this.puerta = puerta;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public boolean isPosteado() {
		return posteado;
	}

	public void setPosteado(boolean posteado) {
		this.posteado = posteado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((pikeador == null) ? 0 : pikeador.hashCode());
		result = prime * result + (posteado ? 1231 : 1237);
		result = prime * result + ((puerta == null) ? 0 : puerta.hashCode());
		result = prime * result + ((ruta == null) ? 0 : ruta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JDBCListasPreparacionAdmin other = (JDBCListasPreparacionAdmin) obj;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (pikeador == null) {
			if (other.pikeador != null)
				return false;
		} else if (!pikeador.equals(other.pikeador))
			return false;
		if (posteado != other.posteado)
			return false;
		if (puerta == null) {
			if (other.puerta != null)
				return false;
		} else if (!puerta.equals(other.puerta))
			return false;
		if (ruta == null) {
			if (other.ruta != null)
				return false;
		} else if (!ruta.equals(other.ruta))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ListasPreparacionAdmin [id=" + id + ", pikeador=" + pikeador + ", puerta=" + puerta + ", ruta=" + ruta
				+ ", posteado=" + posteado + ", fecha=" + fecha + "]";
	}
		
}
