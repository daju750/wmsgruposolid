package pro.solid.inv.JDBCObject;

public class JDBCEncabezadoDetalleSalida {

	private String puerta;
	private String ruta;
	private Long id_usuario = (long)0;
	private boolean visto;
	
	public JDBCEncabezadoDetalleSalida() {}
	public JDBCEncabezadoDetalleSalida(String puerta, String ruta, Long id_usuario, boolean visto) {
		super();
		this.puerta = puerta;
		this.ruta = ruta;
		this.id_usuario = id_usuario;
		this.visto = visto;
	}
	public String getPuerta() {
		return puerta;
	}
	public void setPuerta(String puerta) {
		this.puerta = puerta;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public Long getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}
	public boolean isVisto() {
		return visto;
	}
	public void setVisto(boolean visto) {
		this.visto = visto;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id_usuario == null) ? 0 : id_usuario.hashCode());
		result = prime * result + ((puerta == null) ? 0 : puerta.hashCode());
		result = prime * result + ((ruta == null) ? 0 : ruta.hashCode());
		result = prime * result + (visto ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JDBCEncabezadoDetalleSalida other = (JDBCEncabezadoDetalleSalida) obj;
		if (id_usuario == null) {
			if (other.id_usuario != null)
				return false;
		} else if (!id_usuario.equals(other.id_usuario))
			return false;
		if (puerta == null) {
			if (other.puerta != null)
				return false;
		} else if (!puerta.equals(other.puerta))
			return false;
		if (ruta == null) {
			if (other.ruta != null)
				return false;
		} else if (!ruta.equals(other.ruta))
			return false;
		if (visto != other.visto)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "JDBCEncabezadoDetalleSalida [puerta=" + puerta + ", ruta=" + ruta + ", id_usuario=" + id_usuario
				+ ", visto=" + visto + "]";
	}
	
}
