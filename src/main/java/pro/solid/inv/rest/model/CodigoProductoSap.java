package pro.solid.inv.rest.model;

public class CodigoProductoSap {

	String codigosap;

	public CodigoProductoSap() {}	
	public CodigoProductoSap(String codigosap) {
		super();
		this.codigosap = codigosap;
	}

	public String getCodigosap() {
		return codigosap;
	}

	public void setCodigosap(String codigosap) {
		this.codigosap = codigosap;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigosap == null) ? 0 : codigosap.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodigoProductoSap other = (CodigoProductoSap) obj;
		if (codigosap == null) {
			if (other.codigosap != null)
				return false;
		} else if (!codigosap.equals(other.codigosap))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CodigoSap [codigosap=" + codigosap + "]";
	}
}
