package pro.solid.inv.rest.model;

public class CodigoUbicacion {

	private String codigo;
	
	public CodigoUbicacion() {}
	public CodigoUbicacion(String codigo) {
		this.codigo = codigo;
	}
	public String getCodigoubicacion() {
		return codigo;
	}
	public void setCodigoubicacion(String codigo) {
		this.codigo = codigo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodigoUbicacion other = (CodigoUbicacion) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CodigoUbicacion [codigo=" + codigo + "]";
	}
	
}
