package pro.solid.inv.servicio;

import java.util.List;

import pro.solid.inv.rest.model.CodigoUbicacion;;

public interface UbicacionServicio {

	public Long IdUbicacion(String codigo);
	public List<CodigoUbicacion> JDBCRestComoListaUbicaciones(String codigo);
	public boolean JQPLRestConsultaExiste(String codigo);
	
}
