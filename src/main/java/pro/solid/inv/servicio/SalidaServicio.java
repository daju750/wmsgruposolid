package pro.solid.inv.servicio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import pro.solid.inv.JDBCObject.JDBCDetalleListaSalida;
import pro.solid.inv.JDBCObject.JDBCEncabezadoDetalleSalida;
import pro.solid.inv.modelo.Preparacion;

public interface SalidaServicio {

	public Page<Preparacion> JPQLConsultaListaUsuario(Long id,PageRequest pageRequest);
	public List<JDBCDetalleListaSalida> JDBConsultaDetalleLista(Long id_preparacion, Long limit, Long offset);
	public JDBCEncabezadoDetalleSalida ConsultaEncabezadoDetalle(Long id_preparacion);
	public Long JPQLConsultaTotalItemsDetalleSalida(Long id_preparacion); 
	public Long JDBConsultaIdUsuarioPreparacion(Long id_usuario,Long id);
	public void ActulizarEstadoPreparacionDetalleSalida(Long id);
	

}
