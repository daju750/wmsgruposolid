package pro.solid.inv.servicio;

public interface RolServicio {

	public String findRolname(Long id);
	public String nombreRolIdUsuario(String usuario);
	public void JDBCGuardar(Long id_usuario,Long id_role);
	
}
