package pro.solid.inv.servicio;

import java.util.List;
import pro.solid.inv.modelo.PreparacionDetalle;
public interface PreparacionDetalleServicio {

	public void JDBCTemplateInsertDetallePreparacion(String Sql);
	public List<PreparacionDetalle> JPQLPaginacionAdmin(Long id_preparacion, int limit, long offset);
	public String JDBProcedimientoIdsexcelpreparacion(String codigodsap, int cantidad);
	public String JDBCProcedimientoIdsexcelmenospreparacion(Long id_preparacion,String codigodsap, int cantidad);
	public Long JDBConsultaCountIDetalle(Long id_preparacion);
	public List<PreparacionDetalle> JDBConsultaPreparacion(Long id);
	public List<PreparacionDetalle> JDBConsultaPreparacionCodigoCantidad(Long id);
	public PreparacionDetalle ConsultaUno(Long id);
	public void Eliminar(Long id);
	public boolean JDBCExisteRegistro(Long id_preparacion,String codigosap);
	
}
