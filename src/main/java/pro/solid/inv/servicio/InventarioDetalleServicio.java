package pro.solid.inv.servicio;

import java.util.List;

import pro.solid.inv.JDBCObject.JDBCDetalleInventarioAdmin;
import pro.solid.inv.modelo.Inventario;

public interface InventarioDetalleServicio {

	public Long ConsultaId(Long id_ventario, String lote);
	public void JDBCInsercion(Inventario inventarioDetalle);
	public void ModificarIncremento(Long id, int incremento);
	public void JdbcTemplateActilizarLoteRegistroPrepa(String Sql);
	public Inventario ConsultarUno(Long id_detale_inventario);
	public void ModificarDecrementar(Long id,int decrementar);
	public void JDBCModificarDecrementar(Long idInvetario,Long cantidad,String lote);
	public Long JDBConsultaId(String codigoSap,String lote,String codigoUbicacion);
	public List<JDBCDetalleInventarioAdmin> JDBConsultaInventarioDetalleAdmin(String codigo);
	
}
