package pro.solid.inv.servicio;

import java.util.List;

import pro.solid.inv.JDBCObject.JDBCInventarioGeneral;
import pro.solid.inv.modelo.Inventario;

public interface InventarioServicio {
	
	public void Insercion(Inventario inv);
	public Long ConsultaIdMax();
	public Long JPQLConsultaExistencia(Long id_ubicacion, Long id_producto, String lote);
	public void ModificarIncrementar(int incremento,Long id,String lote);
	public void ModificarDecrementar(int decremento,Long id);
	public Long Total(String codigoSap);
	public void JDBCInsercion(Inventario inventario);
	public List<JDBCInventarioGeneral> JDBConsultarInventarioGeneral(int limit, long offset);
	public Long JDBContarInventarioGeneral();
	public Inventario JQPLConsultaCantidad(Long id_ubicacion,Long id_producto,String lote);
	public void JPQLQuitarReservar(Long id, Long cantidad);
	public Inventario ConsultaUno(Long id);
	public void JDBCQuitarReservas(Long id,Long cantidad);
	public Long JDBConsultaTotal(String id_producto);
}
