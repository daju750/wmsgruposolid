package pro.solid.inv.servicio;

import java.util.List;

import pro.solid.inv.interesp.ExcelPreparacionSalida;
import pro.solid.inv.modelo.PreparacionDetalleSalida;

public interface PreparacionDetalleSalidaServicio {

	public PreparacionDetalleSalida ConsultarUno(Long id);
	public List<ExcelPreparacionSalida> JDBCExcelPrepaSalida(Long id);
	public List<PreparacionDetalleSalida> JDBConsultaIdIDIventarioCantidad(Long id_preparacion,String codigosap);
	public void JDBCEliminarIdin(String ids);
	public boolean JDBCExisteProducto(Long id_inventario,Long id_preparacion);
	
}
