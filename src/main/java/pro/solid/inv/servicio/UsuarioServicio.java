package pro.solid.inv.servicio;

import java.util.List;

import pro.solid.inv.JDBCObject.JDBCGruposUsuarios;
import pro.solid.inv.entidad.UsuarioMaestroDetalle;
import pro.solid.inv.modelo.Usuario;

public interface UsuarioServicio {

    public void save(Usuario usuario);
    public Usuario findByUsername(String username);
    public Long ConsultaId(String username);
    public List<UsuarioMaestroDetalle> JDBCPaginacionListarUsuariosRoleUser(Long limit,Long offse);
    public UsuarioMaestroDetalle usuarioRolerUserId(Long id);
    public List<JDBCGruposUsuarios> JDBConsultaGrupoUsuarios(String grupo);
    public Usuario usuario(Long id_usuario);
    public List<Usuario> JDBConsultaPrimeroUnionTodos(Long id,String puesto);
    public Long JDBConsultaContarUsuarioRoleUser();
    public void JDBCActulizarContrasena(Long id,String password);
    public void JDBCGuardar(Usuario usuario);
    public Long JDBCMax();
    public List<Usuario> JDBConsultaDondePuesto(String puesto);
    
}