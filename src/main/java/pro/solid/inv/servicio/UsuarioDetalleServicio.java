package pro.solid.inv.servicio;

import pro.solid.inv.modelo.UsuarioDetalle;

public interface UsuarioDetalleServicio {

	public void Guardar(UsuarioDetalle usuarioDetalle);
	public Long JDBCMax();
	
}
