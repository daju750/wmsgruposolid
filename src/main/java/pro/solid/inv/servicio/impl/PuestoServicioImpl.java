package pro.solid.inv.servicio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.modelo.Puesto;
import pro.solid.inv.repositorio.PuestoRepositorio;
import pro.solid.inv.servicio.PuestoServicio;

@Service
public class PuestoServicioImpl implements PuestoServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private PuestoRepositorio puestoRespo;
	
	@Override
	@Transactional
	public String BuscarPuesto(String nombreUsuario) {
		return this.puestoRespo.NombrePuestoIdUsuario(nombreUsuario);
	}

	@Override
	@Transactional
	public Page<Puesto> PuestoUsuario(PageRequest pageRequest) {
		return this.puestoRespo.PuestosUsers(pageRequest);
	}

	@Override
	@Transactional
	public List<Puesto> JDBConsultaPuestoUnionTodosNo(Long id) {	
		return this.jdbcTemplate.query("select id,nombre from puesto where id = (select puestos_id from usuario_puestos where usuarios_id = ?) union all select id,nombre from puesto where id != 1 and id!=2 and id!=5 and id!=(select puestos_id from usuario_puestos where usuarios_id = ?)",
				new Object[]{id,id},
				(rs, rowNum) -> new Puesto(
				rs.getLong("id"),
				rs.getString("nombre")
				));
	}

	@Override
	@Transactional
	public void Actulizar(Long id_usuario, Long id_puesto) {
		this.jdbcTemplate.update("update usuario_puestos set puestos_id="+id_puesto+" where usuarios_id="+id_usuario);
	}

	@Override
	@Transactional
	public String JDBCConsultar(Long id_usuario) {
		return this.jdbcTemplate.queryForObject("select nombre from puesto where id=(select puestos_id from usuario_puestos where usuarios_id="+id_usuario+")",String.class);
	}

	@Override
	@Transactional
	public void JDBCInsertar(Long id_usuario, Long id_puesto) {
		this.jdbcTemplate.update("insert into usuario_puestos(usuarios_id,puestos_id) values("+id_usuario+","+id_puesto+")");
	}


}
