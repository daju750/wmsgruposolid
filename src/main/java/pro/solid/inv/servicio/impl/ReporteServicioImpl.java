package pro.solid.inv.servicio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.JDBCObject.JDBCReportaTraslados;
import pro.solid.inv.JDBCObject.JDBCReporteIngreso;
import pro.solid.inv.JDBCObject.JDBCReporteSalida;
import pro.solid.inv.JDBCObject.JDBCReporteTotalInventario;
import pro.solid.inv.JDBCObject.JDBCReporteUbicaciones;
import pro.solid.inv.servicio.ReporteServicio;

@Service
public class ReporteServicioImpl implements ReporteServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;

	@Override
	@Transactional
	public List<JDBCReporteTotalInventario> JDBConsultaReporteInvTotal(long limit,long offset) {
		return this.jdbcTemplate.query("select p.codigosap,p.descripcion,sum(ide.cantidad) as total,sum(ide.reserva) as reserva,(sum(ide.cantidad)-sum(ide.reserva)) as neto from inventario i,inventario_detalle ide,producto p where i.id_producto in(select distinct(id_producto) from inventario) and p.id=i.id_producto and i.id=ide.id_inventario group by i.id_producto,p.codigosap,p.id order by codigosap limit ? offset ?",
				new Object[]{limit,offset},
				(rs, rowNum) -> new JDBCReporteTotalInventario(
				rs.getString("codigosap"),
				rs.getString("descripcion"),
				rs.getLong("total"),
				rs.getLong("reserva"),
				rs.getLong("neto")
				));
	}

	@Override
	@Transactional
	public Long JDBContarConsultaReporteInvTotal() {
		return this.jdbcTemplate.queryForObject("select count(distinct(id_producto)) from inventario",Long.class);
	}

	@Override
	@Transactional
	public List<JDBCReporteTotalInventario> JDBReporteInvTotal(){
		return this.jdbcTemplate.query("select p.codigosap,p.descripcion,sum(i.cantidad) as total,sum(i.reserva) reserva,(sum(i.cantidad)-sum(i.reserva)) as neto from inventario i,producto p where i.id_producto in(select distinct(id_producto) from inventario) and p.id=i.id_producto group by p.codigosap,p.descripcion",
				new Object[]{},
				(rs, rowNum) -> new JDBCReporteTotalInventario(
				rs.getString("codigosap"),
				rs.getString("descripcion"),
				rs.getLong("total"),
				rs.getLong("reserva"),
				rs.getLong("neto")
				));
	}

	
	//Ubicaciones
	@Override
	@Transactional
	public List<JDBCReporteUbicaciones> JDBReporteUbicaciones() {
		// TODO Auto-generated method stub
		return this.jdbcTemplate.query("select p.codigosap,u.codigo as ubicacion,i.cantidad,i.reserva,i.lote from inventario i,producto p,ubicacion u where i.id_producto=p.id and i.id_ubicacion=u.id order by p.codigosap,lote",
				new Object[]{},
				(rs, rowNum) -> new JDBCReporteUbicaciones(
				rs.getString("codigosap"),
				rs.getLong("ubicacion"),
				rs.getLong("cantidad"),
				rs.getLong("reserva"),
				rs.getString("lote")
				));
	}

	
	//Ingresos
	@Override
	@Transactional
	public List<JDBCReporteIngreso> JDBCReporteIngresos(String fechaI, String fechaF,String usuario) {
		return this.jdbcTemplate.query("select p.codigosap,u.codigo as ubicacion,ri.cantidad,c.puerta,ri.lote,ri.fecha from registro_ingreso ri,producto p,ubicacion u,compuerta c where ri.id_usuario=(select id from usuario where username=?) and ri.id_producto=p.id and ri.id_compuerta=c.id and ri.id_ubicacion=u.id and ri.fecha between '"+fechaI+"' and '"+fechaF+" 23:59:59'",
				new Object[]{usuario},
				(rs, rowNum) -> new JDBCReporteIngreso(
				rs.getString("codigosap"),
				rs.getString("ubicacion"),
				rs.getInt("cantidad"),
				rs.getString("puerta"),
				rs.getString("lote"),
				rs.getString("fecha")
				));
	}

	@Override
	public List<JDBCReportaTraslados> JDBCReporteTraslados(String fechaI, String fechaF, String usuario) {
		return this.jdbcTemplate.query("select p.codigosap,tr.lote,tr.cantidad,ubi.codigo as origen,u.codigo as destino,tr.fecha from registro_traslado tr,producto p,ubicacion u,ubicacion ubi where tr.id_usuario=(select id from usuario where username=?) and tr.id_producto=p.id and tr.id_destino=u.id and ubi.id=tr.id_origen and tr.fecha between '"+fechaI+"' and '"+fechaF+" 23:59:59' order by tr.fecha",
				new Object[]{usuario},
				(rs, rowNum) -> new JDBCReportaTraslados(
				rs.getString("codigosap"),
				rs.getString("lote"),
				rs.getLong("cantidad"),
				rs.getLong("origen"),
				rs.getLong("destino"),
				rs.getString("fecha")
				));
	}
	
	
	@Override
	@Transactional
	public List<JDBCReporteSalida> JDBCReporteSalida(String usuario, String fechaI,String fechaF) {
		return this.jdbcTemplate.query("select pr.id,p.codigosap,u.codigo as ubicacion,s.cantidad,s.descripcion,r.ruta,c.puerta,s.lote,s.fecha from registro_salida s,producto p, ubicacion u,preparacion pr,ruta r,compuerta c where s.id_usuario=(select id from usuario where username=?) and s.id_producto=p.id and pr.id_ruta=r.id and s.id_ubicacion=u.id and pr.id_compuerta=c.id and s.id_preparacion=pr.id and s.fecha between '"+fechaI+"' and '"+fechaF+" 23:59:59'",
				new Object[]{usuario},
				(rs, rowNum) -> new JDBCReporteSalida(
					rs.getLong("id"),	
					rs.getString("codigosap"),
					rs.getString("ubicacion"),
					rs.getLong("cantidad"),
					rs.getString("descripcion"),
					rs.getString("ruta"),
					rs.getString("puerta"),
					rs.getString("lote"),
					rs.getString("fecha")
				));
	}
	

}
