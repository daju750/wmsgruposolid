package pro.solid.inv.servicio.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.modelo.RegistroIngreso;
import pro.solid.inv.servicio.RegistroIngresoServicio;

@Service
public class RegistroIngresoServicioImpl implements RegistroIngresoServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;

	@Override
	@Transactional
	public void JDBCInsercion(RegistroIngreso ri) {
		this.jdbcTemplate.update("insert into registro_ingreso(id_producto,id_ubicacion,id_compuerta,id_usuario,cantidad,lote) values("+
	ri.getId_producto()+","+ri.getId_ubicacion()+","+ri.getId_compuerta()+","+ri.getId_usuario()+","+ri.getCantidad()+",'"+ri.getLote()+"')");
		//this.registroIngresoRepositorio.JPQLInsercion(idinventario, idcompuerta, idusuario, cantidad, lote);
	}
	
}
