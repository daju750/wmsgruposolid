package pro.solid.inv.servicio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import pro.solid.inv.modelo.PreparacionDetalle;
import pro.solid.inv.repositorio.PreparacionDetalleRepositorio;
import pro.solid.inv.servicio.PreparacionDetalleServicio;

@Service
public class PreparacionDetalleServicioImpl implements PreparacionDetalleServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall simpleJdbcCallFunction1;
    
    @Autowired
    private PreparacionDetalleRepositorio preparacionDetalleRepositorio;
	
	@Override
	@Transactional
	public void JDBCTemplateInsertDetallePreparacion(String Sql) {
		this.jdbcTemplate.update(Sql);
	}

	@Override
	@Transactional
	public List<PreparacionDetalle> JPQLPaginacionAdmin(Long id_preparacion, int limit, long offset){
		return this.jdbcTemplate.query("select p.id,p.id_preparacion,p.codigosap,p.cantidad,sum(i.cantidad-i.reserva) as total from preparacion_detalle as p inner join inventario as i on i.id_producto = (select id from producto where codigosap=p.codigosap) and i.habilitado=true and id_preparacion=? group by p.id,p.id_preparacion,p.codigosap,p.cantidad order by codigosap limit ? offset ?",
				new Object[]{id_preparacion,limit,offset},
				(rs, rowNum) -> new PreparacionDetalle(
				rs.getLong("id"),
				rs.getLong("id_preparacion"),
				rs.getString("codigosap"),
				rs.getLong("cantidad"),
				rs.getLong("total")
				));
	}

	@Override
	@Transactional
	public String JDBProcedimientoIdsexcelpreparacion(String codigosap, int cantidad) {
			
		this.jdbcTemplate.setResultsMapCaseInsensitive(true);
		
		simpleJdbcCallFunction1 = new SimpleJdbcCall(jdbcTemplate).withFunctionName("IdsExcelPreparacion");
		SqlParameterSource in = new MapSqlParameterSource().addValue("id_productosap",codigosap).addValue("total",cantidad);
		return simpleJdbcCallFunction1.executeFunction(String.class,in);
	}
	
	
	@Override
	public String JDBCProcedimientoIdsexcelmenospreparacion(Long id_preparacion, String codigosap, int cantidad) {
		this.jdbcTemplate.setResultsMapCaseInsensitive(true);
		
		simpleJdbcCallFunction1 = new SimpleJdbcCall(jdbcTemplate).withFunctionName("idsexcelpreparacionmenos");
		SqlParameterSource in = new MapSqlParameterSource().addValue("id_preparaciondetalle",id_preparacion).addValue("id_productosap",codigosap).addValue("total",cantidad);
		return simpleJdbcCallFunction1.executeFunction(String.class,in);
	}

	@Override
	@Transactional
	public Long JDBConsultaCountIDetalle(Long id_preparacion) {
		return this.jdbcTemplate.queryForObject("select count(id) from preparacion_detalle where id_preparacion = ?",new Object[]{id_preparacion},Long.class);
	}

	@Override
	@Transactional
	public List<PreparacionDetalle> JDBConsultaPreparacion(Long id) {
		return this.jdbcTemplate.query("select p.id,p.id_preparacion,p.codigosap,p.cantidad,sum(i.cantidad-i.reserva) as total from preparacion_detalle as p inner join inventario as i on i.id_producto = (select id from producto where codigosap=p.codigosap) and i.habilitado=true and id_preparacion=? group by p.id,p.id_preparacion,p.codigosap,p.cantidad",
				new Object[]{id},
				(rs, rowNum) -> new PreparacionDetalle(
				rs.getLong("id"),
				rs.getLong("id_preparacion"),
				rs.getString("codigosap"),
				rs.getLong("cantidad"),
				rs.getLong("total")
				));
	}

	@Override
	@Transactional
	public List<PreparacionDetalle> JDBConsultaPreparacionCodigoCantidad(Long id) {
		return this.jdbcTemplate.query("select codigosap,cantidad from preparacion_detalle where id_preparacion = ? order by codigosap",
				new Object[]{id},
				(rs, rowNum) -> new PreparacionDetalle(
				rs.getString("codigosap"),
				rs.getLong("cantidad")
				));
	}

	@Override
	@Transactional
	public PreparacionDetalle ConsultaUno(Long id) {
		return this.preparacionDetalleRepositorio.getOne(id);
	}

	@Override
	@Transactional
	public void Eliminar(Long id) {
		this.preparacionDetalleRepositorio.deleteById(id);
	}

	@Override
	@Transactional
	public boolean JDBCExisteRegistro(Long id_preparacion, String codigosap) {
		return this.jdbcTemplate.queryForObject("select exists(select 1 from preparacion_detalle where id_preparacion=? and codigosap=?)",
				new Object[]{id_preparacion,codigosap},Boolean.class);
	}	

}
