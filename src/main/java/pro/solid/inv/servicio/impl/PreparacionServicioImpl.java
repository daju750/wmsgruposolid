package pro.solid.inv.servicio.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.JDBCObject.JDBCListasPreparacionAdmin;
import pro.solid.inv.JDBCObject.JDBCPreparacionSalidaEliminar;
import pro.solid.inv.modelo.Preparacion;
import pro.solid.inv.repositorio.PreparacionRepositorio;
import pro.solid.inv.servicio.PreparacionServicio;

@Service
public class PreparacionServicioImpl implements PreparacionServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private PreparacionRepositorio preparacionRepositorio;
	
	@Override
	@Transactional
	public Long JDBConsultaIdMax() {
		Long idmax=(long)0;
		
		Long tempidmax=jdbcTemplate.queryForObject("select max(id) from preparacion",Long.class);
			if(tempidmax!=null){
				idmax=tempidmax;
			}
        return idmax;
	}

	@Override
	@Transactional
	public void Insertar(Preparacion preparacion) {
		this.preparacionRepositorio.save(preparacion);
	}
	
	@Override
	@Transactional
	public void JDBCActulizar(Preparacion preparacion) {
		this.jdbcTemplate.update("update preparacion set id_usuario=?,id_ruta=?,id_compuerta=?,fecha=? where id=?",
				preparacion.getId_usuario(),preparacion.getId_ruta(),preparacion.getId_compuerta(),new Date(),preparacion.getId());
	}

	@Override
	@Transactional
	public List<JDBCListasPreparacionAdmin> JDBConsultaListaAdmin(Long id_usuario,String ids) {
				
		String sql ="select p.id,u.username,c.puerta,r.ruta,p.posteado,p.fecha from preparacion p,compuerta c,ruta r,usuario as u where p.id_administrador="+id_usuario+" and r.id=p.id_ruta and u.id=p.id_usuario and p.eliminar=false and c.id=p.id_compuerta and p.id in("+ids+") order by p.fecha desc";
		return this.jdbcTemplate.query(sql,(rs, rowNum) ->new JDBCListasPreparacionAdmin(
				rs.getLong("id"),
				rs.getString("username"),
				rs.getString("puerta"),
				rs.getString("ruta"),
				rs.getBoolean("posteado"),
				rs.getDate("fecha")
				));
	}

	@Override
	@Transactional
	public Preparacion ConsultaPreparacion(Long id_preparacion){Preparacion preparacion;		
		if(JDBConsultaExistePrepacionAdmin(id_preparacion)){
			preparacion = this.preparacionRepositorio.getOne(id_preparacion);
		}else {
			preparacion = new Preparacion();
		}
		return preparacion;
	}

	@Override
	@Transactional
	public Page<Object> JPQLConsultaPaginacionAdmin(Long id,PageRequest pageRequest) {
		return this.preparacionRepositorio.JPQLPaginacionIdsAdmin(id,pageRequest);
	}

	@Override
	@Transactional
	public boolean JDBConsultaExistePrepacionAdmin(Long id) {
		return this.jdbcTemplate.queryForObject("select exists(select 1 from preparacion where id=?)",Boolean.class,id);
	}
	
	@Override
	@Transactional
	public void JDBCRestPostearHoja(Long id_preparacion) {
		this.jdbcTemplate.update("update preparacion set posteado=true where id=?",id_preparacion);
	}

	@Override
	@Transactional
	public void JDBCRestVistoHoja(Long id_preparacion) {
		this.jdbcTemplate.update("update preparacion set visto=true where id=?",id_preparacion);
	}

	@Override
	@Transactional
	public void JPQLBorrar(Long id_preparacion) {
		this.preparacionRepositorio.JPQLBorrar(id_preparacion);
	}

	@Override
	public List<JDBCPreparacionSalidaEliminar> JDBConsultaReservas(Long id) {
		List<JDBCPreparacionSalidaEliminar> lista=null;
		try {
			lista = this.jdbcTemplate.query("select id_inventario,cantidad from preparacion_salida where id_preparacion=?",
					new Object[]{id},
					(rs, rowNum) ->new JDBCPreparacionSalidaEliminar(
					rs.getLong("id_inventario"),
					rs.getLong("cantidad")
					));
		}catch(Exception ex) {
			
		}
		
		return lista;
	}
}
