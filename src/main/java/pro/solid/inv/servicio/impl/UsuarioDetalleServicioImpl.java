package pro.solid.inv.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.modelo.UsuarioDetalle;
import pro.solid.inv.repositorio.UsuarioDetalleRepositorio;
import pro.solid.inv.servicio.UsuarioDetalleServicio;

@Service
public class UsuarioDetalleServicioImpl implements UsuarioDetalleServicio{

	@Autowired
	private UsuarioDetalleRepositorio usuarioDetalleRepositorio;

    @Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public void Guardar(UsuarioDetalle usuarioDetalle) {
		this.usuarioDetalleRepositorio.save(usuarioDetalle);
	}

	@Override
	public Long JDBCMax() {
		Long idmax=(long)0;
		Long tempidmax=jdbcTemplate.queryForObject("select max(id) from usuario_detalle",Long.class);
			if(tempidmax!=null){idmax=tempidmax;}
		
	return idmax;
	}
	
}
