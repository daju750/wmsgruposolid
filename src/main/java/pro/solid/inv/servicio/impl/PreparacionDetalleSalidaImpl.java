package pro.solid.inv.servicio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.interesp.ExcelPreparacionSalida;
import pro.solid.inv.modelo.PreparacionDetalleSalida;
import pro.solid.inv.repositorio.PreparacionDetalleSalidaRepositorio;
import pro.solid.inv.servicio.PreparacionDetalleSalidaServicio;

@Service
public class PreparacionDetalleSalidaImpl implements PreparacionDetalleSalidaServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private PreparacionDetalleSalidaRepositorio preparacionSalidaRepositorio;
	
	@Override
	@Transactional
	public PreparacionDetalleSalida ConsultarUno(Long id) {
		//this.preparacionSalidaRepositorio.findAll().stream().filter(c->c.getId()==id).collect(Collectors.toList());;
		return this.preparacionSalidaRepositorio.getOne(id);
	}

	@Override
	@Transactional
	public List<ExcelPreparacionSalida> JDBCExcelPrepaSalida(Long id) {
		return this.jdbcTemplate.query("select p.codigosap,u.codigo as ubicacion,ps.lote,ps.cantidad from preparacion_salida ps,producto p,ubicacion u where ps.id_preparacion=? and ps.id_producto=p.id and ps.id_ubicacion=u.id order by p.codigosap,lote",
			new Object[]{id},
			(rs, rowNum) -> new ExcelPreparacionSalida(
			rs.getString("codigosap"),
			rs.getString("ubicacion"),
			rs.getString("lote"),
			rs.getInt("cantidad")
			));
	}
	
	@Override
	@Transactional
	public List<PreparacionDetalleSalida> JDBConsultaIdIDIventarioCantidad(Long id_preparacion, String codigosap) {
		return this.jdbcTemplate.query("select id,id_inventario,cantidad from preparacion_salida where id_preparacion = ? and id_producto=(select id from producto where codigosap=?)",
			new Object[]{id_preparacion,codigosap},
			(rs, rowNum) -> new PreparacionDetalleSalida(
			rs.getLong("id"),
			rs.getLong("id_inventario"),
			rs.getLong("cantidad")
			));
	}

	@Override
	@Transactional
	public void JDBCEliminarIdin(String ids) {
		this.jdbcTemplate.update("delete from preparacion_salida where id in("+ids+")");
	}

	@Override
	public boolean JDBCExisteProducto(Long id_inventario, Long id_preparacion) {
		return this.jdbcTemplate.queryForObject("select exists(select 1 from preparacion_salida where id_inventario="+id_inventario+" and id_preparacion="+id_preparacion+")",Boolean.class);
	}

	
	
}
