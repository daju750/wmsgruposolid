package pro.solid.inv.servicio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.modelo.Compuerta;
import pro.solid.inv.repositorio.CompuertaRepositorio;
import pro.solid.inv.servicio.CompuertaServicio;

@Service
public class CompuertaServicioImpl implements CompuertaServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private CompuertaRepositorio puertaRepositorio;

	@Override
	@Transactional
	public Compuerta ConsultaCompuerta(Long id) {
		return this.puertaRepositorio.getOne(id);
	}

	@Override//Acutal
	@Transactional
	public List<Object[]> ConsultaCompuertasDomo(Long id_domo) {
		return this.puertaRepositorio.JQLPConsultaCompuertasDomo(id_domo);
	}

	@Override
	@Transactional
	public List<Compuerta> JDBConsultaPrimeroUnionTodos(Long id) {
		
		return jdbcTemplate.query("(select id,puerta from compuerta where id=? and id_domo=1) union all (select id,puerta from compuerta where id!=? and id!=0 and id_domo=1 order by puerta)",
				new Object[]{id,id},
				(rs, rowNum) -> new Compuerta(
				rs.getLong("id"),
				rs.getString("puerta")
				));
		
	}

}
