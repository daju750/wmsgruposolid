package pro.solid.inv.servicio.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.modelo.RegistroSalida;
import pro.solid.inv.servicio.RegistroSalidaServicio;

@Service
public class RegistroSalidaServicioImpl implements RegistroSalidaServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	@Transactional
	public void Insertar(RegistroSalida registro) {
		String cadena= "insert into registro_salida(id_preparacion,id_producto,id_ubicacion,id_usuario,cantidad,lote) values("
				+ registro.getId_preparacion()+","
				+ registro.getId_producto()+","+
				+ registro.getId_ubicacion()+","+
				+ registro.getId_usuario()+","+
				+ registro.getCantidad()+","+
				"'"+registro.getLote()+"')";
		
		this.jdbcTemplate.update(cadena);
	}

}
