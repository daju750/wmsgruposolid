package pro.solid.inv.servicio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import pro.solid.inv.modelo.PreparacionError;
import pro.solid.inv.repositorio.PreparacionErrorRepositorio;
import pro.solid.inv.servicio.PreparacionErrorServicio;

@Service
public class PreparacionErrorServicioImpl implements PreparacionErrorServicio{

	@Autowired
	private PreparacionErrorRepositorio preparacionErrorRepositorio;

	@Override
	@Transactional
	public Page<PreparacionError> PaginacionConsultaAdminListaError(Long id_preparacion, PageRequest pageRequest) {
		return this.preparacionErrorRepositorio.JPQLPaginacionAdminLista(id_preparacion, pageRequest);
	}

	@Override
	@Transactional
	public List<PreparacionError> ConsultaAdminListaError(Long id_preparacion) {
		return this.preparacionErrorRepositorio.JPQLConsultaAdmin(id_preparacion);
	}
	
	
	
}
