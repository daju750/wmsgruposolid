package pro.solid.inv.servicio.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import pro.solid.inv.repositorio.ProductoRepositorio;
import pro.solid.inv.rest.model.CodigoProductoSap;
import pro.solid.inv.modelo.Producto;
import pro.solid.inv.servicio.ProductoServicio;

@Service
public class ProductoServicioImpl implements ProductoServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private ProductoRepositorio productoRepositorio;
	
	@Override
	@Transactional
	public Long JPQLConsultaId(String codigosap){
	        return this.productoRepositorio.JPQLProductoCodigo(codigosap);
	}

	@Override
	@Transactional
	public Page<Producto> Producto(PageRequest pageRequest) {
		return this.productoRepositorio.findAll(pageRequest);
	}

	@Override
	@Transactional
	public Boolean JDBCConsultaExiste(String codigosap) {
		return this.jdbcTemplate.queryForObject("select exists(select 1 from producto where codigosap=?)",Boolean.class,codigosap);
	}

	@Override
	@Transactional
	public List<String> RestComoListaProducots(String codigo) {
		return this.productoRepositorio.SqlComoListaCodigos(codigo);
	}

	@Override
	@Transactional
	public List<CodigoProductoSap> JDBCRestComoListaProductos(String codigosap) {
		return this.jdbcTemplate.query("select codigosap from producto where codigosap like '%"+codigosap+"%' limit 7;",
				(rs, rowNum) -> new CodigoProductoSap(
				rs.getString("codigosap")
				));
	}

	@Override
	@Transactional
	public Optional<Producto> Consultar(long id) {
		return this.productoRepositorio.findById(id);
	}
	
	
	
}
