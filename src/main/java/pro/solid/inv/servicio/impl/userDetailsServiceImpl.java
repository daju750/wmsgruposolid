package pro.solid.inv.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pro.solid.inv.modelo.Role;
import pro.solid.inv.modelo.Usuario;
import pro.solid.inv.repositorio.UsuarioRespositorio;

import java.util.HashSet;
import java.util.Set;

@Service
public class userDetailsServiceImpl implements UserDetailsService{
    @Autowired
    private UsuarioRespositorio usuarioRespositorio;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {
        Usuario usuario = usuarioRespositorio.findByUsername(username);
        if (usuario == null) throw new UsernameNotFoundException(username);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : usuario.getRoles()){
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }

        return new org.springframework.security.core.userdetails.User(usuario.getUsername(), usuario.getPassword(), grantedAuthorities);
    }
}
