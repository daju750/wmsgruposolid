package pro.solid.inv.servicio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.JDBCObject.JDBCDetalleListaSalida;
import pro.solid.inv.JDBCObject.JDBCEncabezadoDetalleSalida;
import pro.solid.inv.modelo.Preparacion;
import pro.solid.inv.repositorio.PreparacionDetalleSalidaRepositorio;
import pro.solid.inv.repositorio.PreparacionRepositorio;
import pro.solid.inv.servicio.SalidaServicio;

@Service
public class SalidaServicioImpl implements SalidaServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private PreparacionRepositorio preparacionRepositorio;
	

	@Autowired
	private PreparacionDetalleSalidaRepositorio preparacionDetalleSalidaRespositorio;

	@Override
	@Transactional
	public Page<Preparacion> JPQLConsultaListaUsuario(Long id,PageRequest pageRequest) {
		return this.preparacionRepositorio.JPQLPaginacionIdsUsuario(id, pageRequest);
	}

	@Override
	@Transactional
	public List<JDBCDetalleListaSalida> JDBConsultaDetalleLista(Long id_preparacion, Long limit, Long offset) {
		return this.jdbcTemplate.query("select s.id,p.codigosap,u.codigo as ubicacion,s.lote,s.cantidad,s.estado from preparacion_salida s,producto p,ubicacion u where id_preparacion=? and p.id=s.id_producto and u.id=s.id_ubicacion order by codigosap,ubicacion,estado limit ? offset ?",
				new Object[]{id_preparacion,limit,offset},
				(rs, rowNum) -> new JDBCDetalleListaSalida(
					rs.getLong("id"),
					rs.getString("codigosap"),
					rs.getString("ubicacion"),
					rs.getString("lote"),
					rs.getLong("cantidad"),
					rs.getBoolean("estado")
				));
	}
	
	@Override
	@Transactional
	public JDBCEncabezadoDetalleSalida ConsultaEncabezadoDetalle(Long id_preparacion) {
		
		JDBCEncabezadoDetalleSalida salida = this.jdbcTemplate.queryForObject("select c.puerta,r.ruta,p.id_usuario,p.visto from preparacion p, compuerta c, ruta r where p.id=? and p.id_ruta=r.id and p.id_compuerta=c.id",
				new Object[]{id_preparacion},
				(rs, rowNum) -> new JDBCEncabezadoDetalleSalida(
					rs.getString("puerta"),
					rs.getString("ruta"),
					rs.getLong("id_usuario"),
					rs.getBoolean("visto")
				));
		
		if(salida.getId_usuario()==0){
			salida = new JDBCEncabezadoDetalleSalida();
		}
		
		return salida;
	}

	@Override
	@Transactional
	public Long JPQLConsultaTotalItemsDetalleSalida(Long id_preparacion) {
		return this.jdbcTemplate.queryForObject("select count(id) from preparacion_salida where id_preparacion=?",new Object[]{id_preparacion},Long.class);
	}

	@Override
	@Transactional
	public Long JDBConsultaIdUsuarioPreparacion(Long id_usuario,Long id){
		Long id_prepa=(long)0;
		
		try {id_prepa=jdbcTemplate.queryForObject("select id from preparacion where id="+id+" and id_usuario ="+id_usuario,Long.class);}catch(Exception ex) {}
		
		return id_prepa;
	}

	@Override
	@Transactional
	public void ActulizarEstadoPreparacionDetalleSalida(Long id) {
		this.preparacionDetalleSalidaRespositorio.JPQLActulizarEstado(id);
	}

	
}
