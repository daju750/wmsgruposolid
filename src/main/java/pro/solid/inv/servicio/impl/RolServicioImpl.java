package pro.solid.inv.servicio.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.repositorio.RolRepositorio;
import pro.solid.inv.servicio.RolServicio;

@Service
public class RolServicioImpl implements RolServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
    private RolRepositorio rolRepositorio;
	
	@Override
	@Transactional
	public String findRolname(Long id) {
		return this.rolRepositorio.findByNameId(id);
	}

	@Override
	@Transactional
	public String nombreRolIdUsuario(String nombreUsuario) {
		return jdbcTemplate.queryForObject(
                "select name from role where id=(select roles_id from usuario_roles where usuarios_id=(select id from usuario where username=?))",
                new Object[]{nombreUsuario},
                String.class
        );
	}

	@Override
	@Transactional
	public void JDBCGuardar(Long id_usuario, Long id_role) {
		jdbcTemplate.update("insert into usuario_roles(usuarios_id,roles_id) values("+id_usuario+","+id_role+")");
	}
	
}
