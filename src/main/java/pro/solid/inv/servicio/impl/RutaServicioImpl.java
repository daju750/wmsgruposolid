package pro.solid.inv.servicio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.modelo.Ruta;
import pro.solid.inv.repositorio.RutaRepositorio;
import pro.solid.inv.servicio.RutaServicio;

@Service
public class RutaServicioImpl implements RutaServicio{

	@Autowired
	private RutaRepositorio rutaServicio;
	
	@Autowired
    private JdbcTemplate jdbcTemplate;

	@Override
	@Transactional
	public Ruta ConsultaDondeUnicoId(Long id) {
		return this.rutaServicio.getOne(id);
	}

	@Override
	@Transactional
	public List<Object[]> ConsultaTodasRutas() {
		return this.rutaServicio.JQLPConsultaRutas();
	}

	@Override
	@Transactional
	public List<Ruta> JDBConsultaPrimeroUnionTodos(Long id) {
		return jdbcTemplate.query("(select id,ruta from ruta where id=?) union all (select id,ruta from ruta where id!=? order by ruta)",
				new Object[]{id,id},
				(rs, rowNum) -> new Ruta(
				rs.getLong("id"),
				rs.getString("ruta")
				));
	}
	
}
