package pro.solid.inv.servicio.impl;

import pro.solid.inv.JDBCObject.JDBCGruposUsuarios;
import pro.solid.inv.entidad.UsuarioMaestroDetalle;
import pro.solid.inv.mapper.UsuarioMaestroDetalleRowMapper;
import pro.solid.inv.modelo.Usuario;
import pro.solid.inv.repositorio.RolRepositorio;
import pro.solid.inv.repositorio.UsuarioRespositorio;
import pro.solid.inv.servicio.UsuarioServicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

@Service
public class UsuarioServicioImpl implements UsuarioServicio {
	
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private UsuarioRespositorio usuarioRespositorio;
    @Autowired
    private RolRepositorio rolRepositorio;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
	@Transactional
    public void save(Usuario usuario) {
        usuario.setPassword(bCryptPasswordEncoder.encode(usuario.getPassword()));
        usuario.setRoles(new HashSet<>(rolRepositorio.findAll()));
        usuarioRespositorio.save(usuario);
    }

	@Override
	@Transactional
    public Usuario findByUsername(String username) {
        return usuarioRespositorio.findByUsername(username);
    }
	
	
	
	@Override
	@Transactional
	public Long ConsultaId(String username){
		return usuarioRespositorio.findByUsername(username).getId();
	}

	@Override
	@Transactional
	public List<UsuarioMaestroDetalle> JDBCPaginacionListarUsuariosRoleUser(Long limit,Long offset) {
		return jdbcTemplate.query("select a.id,a.username,b.nombre as puesto from usuario as a inner join puesto as b on b.id = (select puestos_id from usuario_puestos where usuarios_id=(select usuarios_id from usuario_roles where usuarios_id=a.id and roles_id=2)) order by a.username limit "+limit+" offset "+offset,new UsuarioMaestroDetalleRowMapper());
	}
	
	@Override
	@Transactional
	public UsuarioMaestroDetalle usuarioRolerUserId(Long id) {
		return jdbcTemplate.queryForObject("select a.id,a.username,b.nombre as puesto,c.primer_nombre,c.segundo_nombre,c.primer_apellido,c.segundo_apellido,c.direccion,c.telefono,c.email from usuario as a inner join puesto as b on b.id = (select puestos_id from usuario_puestos where usuarios_id=?) and a.id=? left join usuario_detalle as c on c.id_usuario=a.id",
			new Object[]{id,id},
			(rs, rowNum) -> new UsuarioMaestroDetalle(
			rs.getLong("id"),
			rs.getString("username"),
			rs.getString("puesto"),
			rs.getString("primer_nombre"),
			rs.getString("segundo_nombre"),
			rs.getString("primer_apellido"),
			rs.getString("segundo_apellido"),
			rs.getString("direccion"),
			rs.getString("email"),
			rs.getString("telefono")
			));
	}

	@Override
	@Transactional
	public List<JDBCGruposUsuarios> JDBConsultaGrupoUsuarios(String grupo) {
		return this.jdbcTemplate.query("select id, username from usuario where id in (select usuarios_id from usuario_puestos where puestos_id = (select id from puesto where nombre=?)) order by username",
				new Object[]{grupo},
				(rs, rowNum) -> new JDBCGruposUsuarios(
				rs.getLong("id"),
				rs.getString("username")
				)); 
	}

	@Override
	@Transactional
	public Usuario usuario(Long id_usuario) {
		return this.usuarioRespositorio.getOne(id_usuario);
	}

	@Override
	@Transactional
	public List<Usuario> JDBConsultaPrimeroUnionTodos(Long id,String puesto) {
		return this.jdbcTemplate.query("(select id,username from usuario where id=?) union all (select id,username from usuario where id in(select usuarios_id from usuario_puestos where puestos_id=(select id from puesto where nombre=?)) and id!=? order by username)",
				new Object[]{id,puesto,id},
				(rs, rowNum) -> new Usuario(
				rs.getLong("id"),
				rs.getString("username")
				)); 
	}

	@Override
	@Transactional
	public Long JDBConsultaContarUsuarioRoleUser() {
		return this.jdbcTemplate.queryForObject("select count(a.id) from usuario as a inner join puesto as b on b.id = (select puestos_id from usuario_puestos where usuarios_id=(select usuarios_id from usuario_roles where usuarios_id=a.id and roles_id=2))",Long.class);
	}

	@Override
	@Transactional
	public void JDBCActulizarContrasena(Long id,String password) {
		this.jdbcTemplate.update("update usuario set password='"+bCryptPasswordEncoder.encode(password)+"' where id="+id);
	}

	@Override
	@Transactional
	public void JDBCGuardar(Usuario usuario) {
		this.jdbcTemplate.update("insert into usuario(id,password,username) values("+usuario.getId()+",'"+bCryptPasswordEncoder.encode(usuario.getPassword())+"','"+usuario.getUsername()+"')");
	}

	@Override
	@Transactional
	public Long JDBCMax() {
		Long idmax=(long)0;
		Long tempidmax=jdbcTemplate.queryForObject("select max(id) from usuario",Long.class);
			if(tempidmax!=null){idmax=tempidmax;}
		
		return idmax;
	}

	@Override
	public List<Usuario> JDBConsultaDondePuesto(String puesto) {
		// TODO Auto-generated method stub
		return this.jdbcTemplate.query("select id,username from usuario where id in(select usuarios_id from usuario_puestos where puestos_id=(select id from puesto where nombre=?))",
				new Object[]{puesto},
				(rs, rowNum) -> new Usuario(
				rs.getLong("id"),
				rs.getString("username")
				));
	}
	
}
