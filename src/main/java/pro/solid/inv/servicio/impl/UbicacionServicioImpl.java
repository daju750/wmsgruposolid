package pro.solid.inv.servicio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.repositorio.UbicacionRepositorio;
import pro.solid.inv.rest.model.CodigoUbicacion;
import pro.solid.inv.servicio.UbicacionServicio;

@Service
public class UbicacionServicioImpl implements UbicacionServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private UbicacionRepositorio ubicacionRepositorio;
	
	@Override
	@Transactional
	public Long IdUbicacion(String codigo) {
		
		return this.ubicacionRepositorio.SqlIdUbicacionCogio(codigo);
	}

	@Override
	@Transactional
	public List<CodigoUbicacion> JDBCRestComoListaUbicaciones(String codigo) {
		return this.jdbcTemplate.query("select codigo from ubicacion where codigo like '%"+codigo+"%' limit 5;",
				(rs, rowNum) -> new CodigoUbicacion(
				rs.getString("codigo")
				));
	}

	@Override
	@Transactional
	public boolean JQPLRestConsultaExiste(String codigo) {
		// TODO Auto-generated method stub
		return this.ubicacionRepositorio.JQPLConsultaExiste(codigo);
	}
	
}
