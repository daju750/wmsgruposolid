package pro.solid.inv.servicio.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.entidad.RegistroTraslado;
import pro.solid.inv.servicio.RegistroTrasladoServicio;

@Service
public class RegistroTrasladoServicioImpl implements RegistroTrasladoServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	@Transactional
	public void JDBCInsercion(RegistroTraslado registras) {		
		jdbcTemplate.update("insert into registro_traslado(id_producto,cantidad,id_origen,id_destino,id_usuario,lote)values(?,?,?,?,?,?)"
			,registras.getId_producto(),registras.getCantidad(),registras.getId_origen(),registras.getId_destino(),registras.getId_usuario(),registras.getLote());
	}

}
