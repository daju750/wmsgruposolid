package pro.solid.inv.servicio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.solid.inv.JDBCObject.JDBCInventarioGeneral;
import pro.solid.inv.modelo.Inventario;
import pro.solid.inv.repositorio.InventarioRepositorio;
import pro.solid.inv.servicio.InventarioServicio;

@Service
public class InventarioServicioImpl implements InventarioServicio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private InventarioRepositorio inventarioRepository;
	
	@Override
	@Transactional
	public void Insercion(Inventario inv){	
		this.inventarioRepository.save(inv);
	}
	
	@Override
	@Transactional
	public Long ConsultaIdMax() {
		Long idmax=(long)0;
		Long tempidmax=jdbcTemplate.queryForObject("select max(id) from inventario",Long.class);
			if(tempidmax!=null){idmax=tempidmax;}
        return idmax;
	}	
	
	@Override
	@Transactional
	public Long JPQLConsultaExistencia(Long id_ubicacion, Long id_producto, String lote) {
		
		Long cantidad= (long)0;
		
		try{cantidad = this.inventarioRepository.JPQLExiste(id_ubicacion, id_producto, lote);}catch(NullPointerException e){}
		
		return cantidad;
	}

	@Override
	@Transactional
	public void ModificarIncrementar(int incremento, Long id, String lote){
		this.inventarioRepository.JPQLIncrementarInventario(id, incremento, lote);
	}

	@Override
	@Transactional
	public void ModificarDecrementar(int decremento, Long id) {
		this.inventarioRepository.JPQLDecrementarInventario(id, decremento);
	}

	@Override
	@Transactional
	public Long Total(String codigoSap) {
		return this.jdbcTemplate.queryForObject("select sum(cantidad) as total from inventario where id_producto=(select id from producto where codigosap=?)",new Object[]{codigoSap},Long.class);
	}

	@Override
	@Transactional
	public void JDBCInsercion(Inventario inventario) {
		this.jdbcTemplate.update("insert into inventario(id_producto,id_ubicacion,cantidad,lote,id_domo) values("+inventario.getId_producto()+","+inventario.getId_ubicacion()+","+inventario.getCantidad()+",'"+inventario.getLote()+"',"+inventario.getId_domo()+")");
	}

	@Override
	@Transactional
	public List<JDBCInventarioGeneral> JDBConsultarInventarioGeneral(int limit, long offset) {
		return this.jdbcTemplate.query("select count(p.id) as posiciones,p.id,p.codigosap,sum(cantidad) as total from inventario i,producto p where i.id_producto in(select distinct(id_producto) from inventario) and p.id=i.id_producto group by i.id_producto,p.codigosap,p.id order by codigosap limit ? offset ?",
			new Object[]{limit,offset},
			(rs, rowNum) -> new JDBCInventarioGeneral(
				rs.getInt("posiciones"),
				rs.getLong("id"),
				rs.getString("codigosap"),
				rs.getLong("total")
			));
	}

	@Override
	@Transactional
	public Long JDBContarInventarioGeneral(){
		return this.jdbcTemplate.queryForObject("select count(distinct(id_producto)) from inventario",Long.class);
	}

	@Override
	@Transactional
	public Inventario JQPLConsultaCantidad(Long id_ubicacion, Long id_producto, String lote) {
		Inventario inventario= new Inventario();
		inventario.setCantidad((long)0);
		inventario.setReserva((long)0);
		try {
			inventario = this.inventarioRepository.JPQLConsultarCantidad(id_ubicacion, id_producto, lote);
		}
		catch(NullPointerException e) {
			System.out.println("NullPointerException thrown!");
		}
		
		return inventario;
	}

	@Override
	@Transactional
	public void JPQLQuitarReservar(Long id, Long cantidad) {
		this.inventarioRepository.JPQLQuitarReservas(id, cantidad);
	}

	@Override
	@Transactional
	public Inventario ConsultaUno(Long id) {
		return this.inventarioRepository.getOne(id);
	}

	@Override
	@Transactional
	public void JDBCQuitarReservas(Long id, Long cantidad) {
		this.jdbcTemplate.update("update inventario set reserva = reserva-"+cantidad+",habilitado=true where id = "+id);
	}

	@Override
	@Transactional
	public Long JDBConsultaTotal(String id_producto) {
		return this.jdbcTemplate.queryForObject("select (sum(cantidad)-sum(reserva)) as total from inventario where id_producto=(select id from producto where codigosap=?)",new Object[]{id_producto},Long.class);
	}

}
