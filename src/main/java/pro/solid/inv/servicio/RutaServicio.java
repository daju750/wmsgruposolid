package pro.solid.inv.servicio;

import java.util.List;
import pro.solid.inv.modelo.Ruta;

public interface RutaServicio {

	public Ruta ConsultaDondeUnicoId(Long id);
	public List<Object[]> ConsultaTodasRutas();
	public List<Ruta> JDBConsultaPrimeroUnionTodos(Long id);
	
}
