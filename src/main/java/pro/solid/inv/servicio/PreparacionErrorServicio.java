package pro.solid.inv.servicio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import pro.solid.inv.modelo.PreparacionError;

public interface PreparacionErrorServicio {

	public Page<PreparacionError> PaginacionConsultaAdminListaError(Long id_preparacion,PageRequest pageRequest);
	public List<PreparacionError> ConsultaAdminListaError(Long id_preparacion);
	
}
