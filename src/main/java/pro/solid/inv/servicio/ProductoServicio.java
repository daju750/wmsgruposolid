package pro.solid.inv.servicio;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import pro.solid.inv.modelo.Producto;
import pro.solid.inv.rest.model.CodigoProductoSap;
public interface ProductoServicio {
	
	public Long JPQLConsultaId(String codigosap); 
	public Page<Producto> Producto(PageRequest pageRequest);
	public Boolean JDBCConsultaExiste(String codigosap);
	public List<String> RestComoListaProducots(String codigo);
	public List<CodigoProductoSap> JDBCRestComoListaProductos(String codigosap);
	public Optional<Producto> Consultar(long id);
}

