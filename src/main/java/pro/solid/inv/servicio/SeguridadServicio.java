package pro.solid.inv.servicio;

public interface SeguridadServicio {
    
	String findLoggedInUsername();
    void autoLogin(String username, String password);
    
}
