package pro.solid.inv.servicio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import pro.solid.inv.modelo.Puesto;

public interface PuestoServicio {

	public String BuscarPuesto(String nombreUsuario);
	public Page<Puesto> PuestoUsuario(PageRequest pageRequest);
	public List<Puesto> JDBConsultaPuestoUnionTodosNo(Long id);
	public void Actulizar(Long id_usuario, Long id_puesto);
	public String JDBCConsultar(Long id_usuario);
	public void JDBCInsertar(Long id_usuario, Long id_puesto);
	
}
