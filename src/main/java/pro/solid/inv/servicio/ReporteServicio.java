package pro.solid.inv.servicio;

import java.util.List;

import pro.solid.inv.JDBCObject.JDBCReportaTraslados;
import pro.solid.inv.JDBCObject.JDBCReporteIngreso;
import pro.solid.inv.JDBCObject.JDBCReporteSalida;
import pro.solid.inv.JDBCObject.JDBCReporteTotalInventario;
import pro.solid.inv.JDBCObject.JDBCReporteUbicaciones;

public interface ReporteServicio {

	//InventarioTotal
	public List<JDBCReporteTotalInventario> JDBConsultaReporteInvTotal(long limit,long offset);
	public Long JDBContarConsultaReporteInvTotal();
	public List<JDBCReporteTotalInventario> JDBReporteInvTotal();
	
	//Ubicaciones
	public List<JDBCReporteUbicaciones> JDBReporteUbicaciones();
	
	//Ingresos
	public List<JDBCReporteIngreso> JDBCReporteIngresos(String fechaI,String fechaF,String usuario);
	
	//Traslados
	public List<JDBCReportaTraslados> JDBCReporteTraslados(String fechaI,String fechaF,String usuario);
	
	//Salidas
	public List<JDBCReporteSalida> JDBCReporteSalida(String usuario,String fechaI,String fechaF);
	
}
