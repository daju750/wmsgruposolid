package pro.solid.inv.servicio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import pro.solid.inv.JDBCObject.JDBCListasPreparacionAdmin;
import pro.solid.inv.JDBCObject.JDBCPreparacionSalidaEliminar;
import pro.solid.inv.modelo.Preparacion;


public interface PreparacionServicio {

	public Long JDBConsultaIdMax();
	public void Insertar(Preparacion preparacion);
	public List<JDBCListasPreparacionAdmin> JDBConsultaListaAdmin(Long id_usuario,String ids);
	public Preparacion ConsultaPreparacion(Long id_preparacion);
	public Page<Object> JPQLConsultaPaginacionAdmin(Long id,PageRequest pageRequest);
	public boolean JDBConsultaExistePrepacionAdmin(Long id);
	public void JDBCActulizar(Preparacion preparacion);
	public void JDBCRestPostearHoja(Long id_preparacion);
	public void JDBCRestVistoHoja(Long id_preparacion);
	public void JPQLBorrar(Long id_preparacion);
	public List<JDBCPreparacionSalidaEliminar> JDBConsultaReservas(Long id);
	
}
