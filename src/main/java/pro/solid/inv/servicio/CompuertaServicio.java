package pro.solid.inv.servicio;

import java.util.List;

import pro.solid.inv.modelo.Compuerta;

public interface CompuertaServicio {

	public Compuerta ConsultaCompuerta(Long id);
	public List<Object[]> ConsultaCompuertasDomo(Long id_domo);//Actual
	public List<Compuerta> JDBConsultaPrimeroUnionTodos(Long id);
	
}
