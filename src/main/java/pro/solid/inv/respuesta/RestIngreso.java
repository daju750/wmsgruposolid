package pro.solid.inv.respuesta;

public class RestIngreso {

	private boolean codigosap;
	private boolean ubicacion;
	
	public RestIngreso() {}
	
	public RestIngreso(boolean codigosap, boolean ubicacion) {
		super();
		this.codigosap = codigosap;
		this.ubicacion = ubicacion;
	}
	
	public boolean isCodigosap() {
		return codigosap;
	}
	public void setCodigosap(boolean codigosap) {
		this.codigosap = codigosap;
	}
	public boolean isUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(boolean ubicacion) {
		this.ubicacion = ubicacion;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (codigosap ? 1231 : 1237);
		result = prime * result + (ubicacion ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RestIngreso other = (RestIngreso) obj;
		if (codigosap != other.codigosap)
			return false;
		if (ubicacion != other.ubicacion)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RestIngreso [codigosap=" + codigosap + ", ubicacion=" + ubicacion + "]";
	}
	
}
