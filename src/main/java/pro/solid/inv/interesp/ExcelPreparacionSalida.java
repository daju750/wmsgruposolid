package pro.solid.inv.interesp;

public class ExcelPreparacionSalida {

	private String codigosap;
	private String ubicacion;
	private String lote;
	private int cantidad;
	
	public ExcelPreparacionSalida() {}
	public ExcelPreparacionSalida(String codigosap, String ubicacion, String lote, int cantidad) {
		super();
		this.codigosap = codigosap;
		this.ubicacion = ubicacion;
		this.lote = lote;
		this.cantidad = cantidad;
	}
	
	public String getCodigosap() {
		return codigosap;
	}
	public void setCodigosap(String codigosap) {
		this.codigosap = codigosap;
	}
	public String getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	public String getLote() {
		return lote;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cantidad;
		result = prime * result + ((codigosap == null) ? 0 : codigosap.hashCode());
		result = prime * result + ((lote == null) ? 0 : lote.hashCode());
		result = prime * result + ((ubicacion == null) ? 0 : ubicacion.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExcelPreparacionSalida other = (ExcelPreparacionSalida) obj;
		if (cantidad != other.cantidad)
			return false;
		if (codigosap == null) {
			if (other.codigosap != null)
				return false;
		} else if (!codigosap.equals(other.codigosap))
			return false;
		if (lote == null) {
			if (other.lote != null)
				return false;
		} else if (!lote.equals(other.lote))
			return false;
		if (ubicacion == null) {
			if (other.ubicacion != null)
				return false;
		} else if (!ubicacion.equals(other.ubicacion))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ExcelPreparacionSalida [codigosap=" + codigosap + ", ubicacion=" + ubicacion + ", lote=" + lote
				+ ", cantidad=" + cantidad + "]";
	}
	
}
