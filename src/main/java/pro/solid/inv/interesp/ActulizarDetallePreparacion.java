package pro.solid.inv.interesp;

public class ActulizarDetallePreparacion {

	private Long id;
	private String cantidad;
	private String codigosap;
	
	public ActulizarDetallePreparacion(){}
	
	public ActulizarDetallePreparacion(Long id, String cantidad, String codigosap) {
		super();
		this.id = id;
		this.cantidad = cantidad;
		this.codigosap = codigosap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cantidad == null) ? 0 : cantidad.hashCode());
		result = prime * result + ((codigosap == null) ? 0 : codigosap.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActulizarDetallePreparacion other = (ActulizarDetallePreparacion) obj;
		if (cantidad == null) {
			if (other.cantidad != null)
				return false;
		} else if (!cantidad.equals(other.cantidad))
			return false;
		if (codigosap == null) {
			if (other.codigosap != null)
				return false;
		} else if (!codigosap.equals(other.codigosap))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getCodigosap() {
		return codigosap;
	}

	public void setCodigosap(String codigosap) {
		this.codigosap = codigosap;
	}

	@Override
	public String toString() {
		return "ActulizarDetallePreparacion [id=" + id + ", cantidad=" + cantidad + ", codigosap=" + codigosap + "]";
	}
	

	
}
