package pro.solid.inv.componente;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class FechaAutomatica {

	public String FechaYHora(){
		
        Date fechaActual = new Date();
        System.out.println(fechaActual);
		
		DateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");
        DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        //System.out.println("Son las: "+formatoHora.format(fechaActual)+" de fecha: "+formatoFecha.format(fechaActual));
        return formatoFecha.format(fechaActual)+" "+formatoHora.format(fechaActual);
	}
	
}
