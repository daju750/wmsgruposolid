package pro.solid.inv.componente;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.springframework.stereotype.Component;

@SuppressWarnings("deprecation")
@Component
public class ExcelApachePoi {

    public String getCellValue(XSSFCell xssfCell){
		
		if(xssfCell == null) return "";
		
		switch (xssfCell.getCellType()) {
		case STRING: return xssfCell.getStringCellValue();
		case BOOLEAN: return Boolean.toString(xssfCell.getBooleanCellValue());
		case NUMERIC: 
			if(HSSFDateUtil.isCellDateFormatted(xssfCell))
				return HSSFDateUtil.getJavaDate(xssfCell.getNumericCellValue()).toString();
			else
				return String.valueOf(xssfCell.getNumericCellValue());
		case FORMULA : return "";
		case BLANK : return "";
		default:return "";
		}
		
	}
	
}
