package pro.solid.inv.componente;

import org.springframework.stereotype.Component;

@Component
public class Paginacion {

	private int LimiteSuperior;
	private int LimiteInferior;
	private boolean BotonLimtInf=false;
	private boolean BotonLimtSup=false;
	private Long PaginaActual;
	private Long PaginasTotales;
	private Long TotalItems;

	public Paginacion() {}
	
	public Paginacion(int PaginaActual,int PaginasTotales) {
		
		int limtsup=0;
		int limtinf=0;
		
		limtsup=PaginaActual+4;
		limtinf=PaginaActual-4;
		
		if(limtsup<9 || limtinf<1){limtinf=1;limtsup=9;this.BotonLimtSup=true;}
		if(limtsup<=(PaginasTotales-1)){this.BotonLimtSup=true;}
		if(limtinf>=2){this.BotonLimtInf=true;}
		if(limtsup>=PaginasTotales){limtinf=PaginasTotales-8;limtsup=PaginasTotales;this.BotonLimtInf=true;}
		if(PaginaActual>= 1 && PaginasTotales<=8){limtinf=1;limtsup=PaginasTotales;this.BotonLimtInf=false;this.BotonLimtSup=false;}
		
		this.LimiteInferior = limtinf;
		this.LimiteSuperior = limtsup;		
	}
	
	public void GenerarLimites(int PaginaActual,int PaginasTotales){
		int limtsup=0;
		int limtinf=0;
		
		limtsup=PaginaActual+4;
		limtinf=PaginaActual-4;
		
		if(limtsup<9 || limtinf<1){limtinf=1;limtsup=9;this.BotonLimtSup=true;}
		if(limtsup<=(PaginasTotales-1)){this.BotonLimtSup=true;}
		if(limtinf>=2){this.BotonLimtInf=true;}
		if(limtsup>=PaginasTotales){limtinf=PaginasTotales-8;limtsup=PaginasTotales;this.BotonLimtInf=true;}
		if(PaginaActual>= 1 && PaginasTotales<=8){limtinf=1;limtsup=PaginasTotales;this.BotonLimtInf=false;this.BotonLimtSup=false;}
		
		this.LimiteInferior = limtinf;
		this.LimiteSuperior = limtsup;
	}

	public Long PaginasTotales(long size,long items){
		
		float decimal= (float)items/size;
		float result= items/size;
		Long pagtotal= (long)0;
		if(decimal>result){
			pagtotal = (long)result+1;
 		}else{
 			pagtotal = (long)result;
 		}

		this.PaginasTotales = pagtotal;
		return pagtotal;
	}
	
	public int getLimiteSuperior() {
		return LimiteSuperior;
	}

	public void setLimiteSuperior(int limiteSuperior) {
		LimiteSuperior = limiteSuperior;
	}

	public int getLimiteInferior() {
		return LimiteInferior;
	}

	public void setLimiteInferior(int limiteInferior) {
		LimiteInferior = limiteInferior;
	}

	public boolean isBotonLimtInf() {
		return BotonLimtInf;
	}

	public void setBotonLimtInf(boolean botonLimtInf) {
		BotonLimtInf = botonLimtInf;
	}

	public boolean isBotonLimtSup() {
		return BotonLimtSup;
	}

	public void setBotonLimtSup(boolean botonLimtSup) {
		BotonLimtSup = botonLimtSup;
	}

	public Long getPaginaActual() {
		return PaginaActual;
	}

	public void setPaginaActual(Long paginaActual) {
		PaginaActual = paginaActual;
	}

	public Long getPaginasTotales() {
		return PaginasTotales;
	}

	public void setPaginasTotales(Long paginasTotales) {
		PaginasTotales = paginasTotales;
	}
	
	public Long getTotalItems() {
		return TotalItems;
	}

	public void setTotalItems(Long totalItems) {
		TotalItems = totalItems;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (BotonLimtInf ? 1231 : 1237);
		result = prime * result + (BotonLimtSup ? 1231 : 1237);
		result = prime * result + LimiteInferior;
		result = prime * result + LimiteSuperior;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paginacion other = (Paginacion) obj;
		if (BotonLimtInf != other.BotonLimtInf)
			return false;
		if (BotonLimtSup != other.BotonLimtSup)
			return false;
		if (LimiteInferior != other.LimiteInferior)
			return false;
		if (LimiteSuperior != other.LimiteSuperior)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Paginacion [LimiteSuperior=" + LimiteSuperior + ", LimiteInferior=" + LimiteInferior + ", BotonLimtInf="
				+ BotonLimtInf + ", BotonLimtSup=" + BotonLimtSup + "]";
	}
	
}
