package pro.solid.inv.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pro.solid.inv.modelo.UsuarioDetalle;

public interface UsuarioDetalleRepositorio extends JpaRepository<UsuarioDetalle,Long>{

	@Query(value = "select id,puerta from compuerta u where u.id_domo=?1 order by puerta",nativeQuery = true)
	Long JQLPConsultaCompuertasDomo(Long id);
	
}
