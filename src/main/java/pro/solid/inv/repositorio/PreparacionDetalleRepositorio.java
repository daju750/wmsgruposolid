package pro.solid.inv.repositorio;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pro.solid.inv.modelo.PreparacionDetalle;

public interface PreparacionDetalleRepositorio extends JpaRepository<PreparacionDetalle,Long>{

	@Query(value = "select id,id_preparacion,descripcion from preparacion_error where id_preparacion = ?1 order by ?#{#pageable},codigosap",
		       countQuery = "select count(*) from preparacion_error where id_preparacion = ?1",
		       nativeQuery = true)
	Page<Object> JPQLPaginacionAdmin(Long id, PageRequest pageRequest);
	
}
