package pro.solid.inv.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pro.solid.inv.modelo.Ruta;

public interface RutaRepositorio extends JpaRepository<Ruta,Long>{

	@Query(value = "select id,ruta from ruta order by ruta",nativeQuery = true)
	List<Object[]> JQLPConsultaRutas();
	
}
