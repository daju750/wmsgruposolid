package pro.solid.inv.repositorio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pro.solid.inv.modelo.PreparacionError;

public interface PreparacionErrorRepositorio extends JpaRepository<PreparacionError,Long>{

	@Query(value = "select id,id_preparacion,descripcion from preparacion_error where id_preparacion = ?1 order by ?#{#pageable}",
		       countQuery = "select count(*) from preparacion_error where id_preparacion = ?1",
		       nativeQuery = true)
	Page<PreparacionError> JPQLPaginacionAdminLista(Long id, PageRequest pageRequest);
	
	@Query(value = "select id,id_preparacion,descripcion from preparacion_error where id_preparacion = ?",nativeQuery = true)
	List<PreparacionError> JPQLConsultaAdmin(Long id);
	
	
	
}
