package pro.solid.inv.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pro.solid.inv.modelo.PreparacionDetalleSalida;

public interface PreparacionDetalleSalidaRepositorio extends JpaRepository<PreparacionDetalleSalida,Long>{
			
	@Modifying //update preparacion_salida set estado=true where id=83
	@Query(value = "update preparacion_salida set estado=true where id=:id",nativeQuery = true)
	void JPQLActulizarEstado(@Param("id") Long id);
	
//	@Modifying
//	@Query(value = "delete from preparacion_salida p where p.id_preparacion=:id_preparacion and p.id_producto=:idproducto",nativeQuery = true)
//	void deleteBooks(@Param("id_preparacion") Long id_preparacion,@Param("idproducto") Long idproducto);

	

	
}
