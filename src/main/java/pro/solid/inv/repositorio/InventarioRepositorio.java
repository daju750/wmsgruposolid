package pro.solid.inv.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pro.solid.inv.modelo.Inventario;

public interface InventarioRepositorio extends JpaRepository<Inventario,Long>{

	@Query(value = "select id from inventario where id_ubicacion=:id_ubicacion  and id_producto=:id_producto and (lote=:lote or lote='') limit 1",nativeQuery = true)
	Long JPQLExiste(@Param("id_ubicacion") Long id_ubicacion,@Param("id_producto") Long id_producto,@Param("lote") String lote);
	
	@Modifying
	@Query(value = "update inventario set cantidad = cantidad + :incremento, lote =:lote,habilitado=true where id = :id",nativeQuery = true)
	void JPQLIncrementarInventario(@Param("id") Long id,@Param("incremento") int incremento,@Param("lote") String lote);
	
	@Modifying
	@Query(value = "update inventario set cantidad = cantidad - :decremento where id = :id",nativeQuery = true)
	void JPQLDecrementarInventario(@Param("id") Long id,@Param("decremento") int incremento);
	
	@Query(value = "select id,cantidad,reserva,id_producto,habilitado,id_ubicacion,lote,id_domo from inventario where id_ubicacion=:id_ubicacion  and id_producto=:id_producto and lote=:lote and habilitado=true",nativeQuery = true)
	Inventario JPQLConsultarCantidad(@Param("id_ubicacion") Long id_ubicacion,@Param("id_producto") Long id_producto,@Param("lote") String lote);
	
	@Modifying
	@Query(value = "update inventario set reserva = reserva-:cantidad,habilitado=true where id = :id",nativeQuery = true)
	void JPQLQuitarReservas(@Param("id") Long id,@Param("cantidad") Long cantidad);
	
}
