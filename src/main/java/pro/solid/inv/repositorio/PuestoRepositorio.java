package pro.solid.inv.repositorio;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pro.solid.inv.modelo.Puesto;

@Repository
public interface PuestoRepositorio extends JpaRepository<Puesto,Long>{

	@Query(value = "select nombre from puesto where id=(select puestos_id from usuario_puestos where usuarios_id=(select id from usuario where username=?1))",nativeQuery = true)
    String NombrePuestoIdUsuario(String nombreUsuario);
	
	@Query(value = "select id,nombre from puesto where id!=1 and id!=2 and id!=5",nativeQuery = true)
    Page<Puesto> PuestosUsers(PageRequest pageRequest);

}
