package pro.solid.inv.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pro.solid.inv.modelo.Ubicacion;

public interface UbicacionRepositorio extends JpaRepository<Ubicacion,Long>{

	@Query(value = "select id from ubicacion u where u.codigo=?",nativeQuery = true)
	Long SqlIdUbicacionCogio(String codigo);

	@Query(value = "select exists(select 1 from ubicacion u where u.codigo=?)",nativeQuery = true)
	Boolean JQPLConsultaExiste(String codigo);
	
}
