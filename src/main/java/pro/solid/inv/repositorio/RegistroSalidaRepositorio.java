package pro.solid.inv.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pro.solid.inv.modelo.RegistroSalida;

@Repository
public interface RegistroSalidaRepositorio extends JpaRepository<RegistroSalida,Long>{

}
