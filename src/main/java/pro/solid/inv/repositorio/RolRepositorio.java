package pro.solid.inv.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pro.solid.inv.modelo.Role;

@Repository
public interface RolRepositorio extends JpaRepository<Role, Long>{
	
	@Query(value = "select name from role where id =(select roles_id from usuario_roles u where u.usuarios_id=?1)",nativeQuery = true)
    String findByNameId(Long usuarios_id);
	
}
