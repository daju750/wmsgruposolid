package pro.solid.inv.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pro.solid.inv.modelo.Compuerta;

@Repository
public interface CompuertaRepositorio extends JpaRepository<Compuerta,Long>{

	@Query(value = "select id,puerta from compuerta u where u.id_domo=?1 and u.id!=0 order by puerta",nativeQuery = true)
	List<Object[]> JQLPConsultaCompuertasDomo(Long id);

}
