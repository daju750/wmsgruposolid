package pro.solid.inv.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pro.solid.inv.modelo.RegistroIngreso;

@Repository
public interface RegistroIngresoRepositorio extends JpaRepository<RegistroIngreso,Long>{
	
	@Modifying
	@Query(value = "insert into registro_ingreso(id_inventario,id_compuerta,id_usuario,cantidad,lote)values(:idinventario,:idcompuerta,:idusuario,:cantidad,:lote)",nativeQuery=true)
	void JPQLInsercion(@Param("idinventario") Long idinventario, @Param("idcompuerta") Long idcompuerta,@Param("idusuario") Long idusuario, @Param("cantidad") Long cantidad, @Param("lote") String lote);
	
}
