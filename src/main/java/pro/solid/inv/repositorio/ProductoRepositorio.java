package pro.solid.inv.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pro.solid.inv.modelo.Producto;

public interface ProductoRepositorio extends JpaRepository<Producto,Long>{

	@Query(value = "select id from producto u where u.codigosap=?",nativeQuery = true)
	Long JPQLProductoCodigo(String codigo);
	
	@Query(value = "select codigosap from producto where codigosap LIKE %"+"?"+"% limit 10;",nativeQuery = true)
	List<String> SqlComoListaCodigos(String codigo);
	
}
