package pro.solid.inv.repositorio;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pro.solid.inv.modelo.Preparacion;

public interface PreparacionRepositorio extends JpaRepository<Preparacion,Long>{
	
	@Query(value = "select id,id_usuario,id_ruta,id_administrador,id_compuerta,fecha,posteado,visto,observacion from preparacion where id_usuario = ?1 and eliminar=false and posteado=true order by fecha desc,?#{#pageable}",
		       countQuery = "select count(*) from preparacion where id_usuario = ?1",
		       nativeQuery = true)
	Page<Preparacion> JPQLPaginacionIdsUsuario(Long id, PageRequest pageRequest);   
	
	@Query(value = "select id from preparacion where id_administrador = ?1 and eliminar=false order by ?#{#pageable}",
		       countQuery = "select count(*) from preparacion where id_usuario = ?1",
		       nativeQuery = true)
	Page<Object> JPQLPaginacionIdsAdmin(Long id, PageRequest pageRequest);   
	
	@Modifying
	@Query(value = "update preparacion set eliminar = true where id = :id",nativeQuery = true)
	void JPQLBorrar(@Param("id") Long id);

	
}
