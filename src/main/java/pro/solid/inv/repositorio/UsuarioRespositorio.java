package pro.solid.inv.repositorio;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pro.solid.inv.modelo.Usuario;

@Repository
public interface UsuarioRespositorio extends JpaRepository<Usuario, Long> {
	
    public Usuario findByUsername(String username);
    
}
