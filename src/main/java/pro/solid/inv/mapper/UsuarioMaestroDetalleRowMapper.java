package pro.solid.inv.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pro.solid.inv.entidad.UsuarioMaestroDetalle;

public class UsuarioMaestroDetalleRowMapper implements RowMapper<UsuarioMaestroDetalle>{

	@Override
	public UsuarioMaestroDetalle mapRow(ResultSet rs, int rowNum) throws SQLException {
		UsuarioMaestroDetalle usuariomaestrodetalle = new UsuarioMaestroDetalle(rs.getLong(1),rs.getString(2),rs.getString(3));
		return usuariomaestrodetalle;
	}
	
	
	
}