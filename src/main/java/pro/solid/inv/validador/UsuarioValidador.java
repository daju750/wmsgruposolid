package pro.solid.inv.validador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pro.solid.inv.modelo.Usuario;
import pro.solid.inv.servicio.UsuarioServicio;

@Component
public class UsuarioValidador implements Validator {
   
	@Autowired
    private UsuarioServicio usuarioServicio;

    @Override
    public boolean supports(Class<?> aClass) {
        return Usuario.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Usuario usuario = (Usuario) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
        if (usuario.getUsername().length() < 6 || usuario.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.userForm.username");
        }
        if (usuarioServicio.findByUsername(usuario.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (usuario.getPassword().length() < 8 || usuario.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password");
        }

        if (!usuario.getPasswordConfirm().equals(usuario.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
        }
    }
}
