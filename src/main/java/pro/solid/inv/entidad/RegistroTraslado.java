package pro.solid.inv.entidad;

public class RegistroTraslado {

	private Long id_producto;
	private int cantidad;
	private Long id_origen;
	private Long id_destino;
	private Long id_usuario;
	private String lote;
	
	public RegistroTraslado() {}
	
	public RegistroTraslado(Long id_producto, int cantidad, Long id_origen, Long id_destino, Long id_usuario,
			String lote) {
		super();
		this.id_producto = id_producto;
		this.cantidad = cantidad;
		this.id_origen = id_origen;
		this.id_destino = id_destino;
		this.id_usuario = id_usuario;
		this.lote = lote;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cantidad;
		result = prime * result + ((id_destino == null) ? 0 : id_destino.hashCode());
		result = prime * result + ((id_origen == null) ? 0 : id_origen.hashCode());
		result = prime * result + ((id_producto == null) ? 0 : id_producto.hashCode());
		result = prime * result + ((id_usuario == null) ? 0 : id_usuario.hashCode());
		result = prime * result + ((lote == null) ? 0 : lote.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistroTraslado other = (RegistroTraslado) obj;
		if (cantidad != other.cantidad)
			return false;
		if (id_destino == null) {
			if (other.id_destino != null)
				return false;
		} else if (!id_destino.equals(other.id_destino))
			return false;
		if (id_origen == null) {
			if (other.id_origen != null)
				return false;
		} else if (!id_origen.equals(other.id_origen))
			return false;
		if (id_producto == null) {
			if (other.id_producto != null)
				return false;
		} else if (!id_producto.equals(other.id_producto))
			return false;
		if (id_usuario == null) {
			if (other.id_usuario != null)
				return false;
		} else if (!id_usuario.equals(other.id_usuario))
			return false;
		if (lote == null) {
			if (other.lote != null)
				return false;
		} else if (!lote.equals(other.lote))
			return false;
		return true;
	}
	public Long getId_producto() {
		return id_producto;
	}
	public void setId_producto(Long id_producto) {
		this.id_producto = id_producto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Long getId_origen() {
		return id_origen;
	}
	public void setId_origen(Long id_origen) {
		this.id_origen = id_origen;
	}
	public Long getId_destino() {
		return id_destino;
	}
	public void setId_destino(Long id_destino) {
		this.id_destino = id_destino;
	}
	public Long getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getLote() {
		return lote;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
	
	@Override
	public String toString() {
		return "RegistroTraslado [id_producto=" + id_producto + ", cantidad=" + cantidad + ", id_origen=" + id_origen
				+ ", id_destino=" + id_destino + ", id_usuario=" + id_usuario + ", lote=" + lote + "]";
	}
	
}

