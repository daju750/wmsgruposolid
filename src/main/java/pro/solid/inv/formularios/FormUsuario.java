package pro.solid.inv.formularios;

public class FormUsuario {

	private String usuario;
	private String contrasena;
	private String puesto;
	private String primer_nombre;
	private String segundo_nombre;
	private String primer_apellido;
	private String segundo_apellido;
	private String direccion;
	private String telefono;
	private String email;
	
	public FormUsuario(){}
	
	public FormUsuario(String usuario, String contrasena, String puesto, String primer_nombre, String segundo_nombre,
			String primer_apellido, String segundo_apellido, String direccion, String telefono, String email) {
		super();
		this.usuario = usuario;
		this.contrasena = contrasena;
		this.puesto = puesto;
		this.primer_nombre = primer_nombre;
		this.segundo_nombre = segundo_nombre;
		this.primer_apellido = primer_apellido;
		this.segundo_apellido = segundo_apellido;
		this.direccion = direccion;
		this.telefono = telefono;
		this.email = email;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public String getPrimer_nombre() {
		return primer_nombre;
	}

	public void setPrimer_nombre(String primer_nombre) {
		this.primer_nombre = primer_nombre;
	}

	public String getSegundo_nombre() {
		return segundo_nombre;
	}

	public void setSegundo_nombre(String segundo_nombre) {
		this.segundo_nombre = segundo_nombre;
	}

	public String getPrimer_apellido() {
		return primer_apellido;
	}

	public void setPrimer_apellido(String primer_apellido) {
		this.primer_apellido = primer_apellido;
	}

	public String getSegundo_apellido() {
		return segundo_apellido;
	}

	public void setSegundo_apellido(String segundo_apellido) {
		this.segundo_apellido = segundo_apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contrasena == null) ? 0 : contrasena.hashCode());
		result = prime * result + ((direccion == null) ? 0 : direccion.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((primer_apellido == null) ? 0 : primer_apellido.hashCode());
		result = prime * result + ((primer_nombre == null) ? 0 : primer_nombre.hashCode());
		result = prime * result + ((puesto == null) ? 0 : puesto.hashCode());
		result = prime * result + ((segundo_apellido == null) ? 0 : segundo_apellido.hashCode());
		result = prime * result + ((segundo_nombre == null) ? 0 : segundo_nombre.hashCode());
		result = prime * result + ((telefono == null) ? 0 : telefono.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormUsuario other = (FormUsuario) obj;
		if (contrasena == null) {
			if (other.contrasena != null)
				return false;
		} else if (!contrasena.equals(other.contrasena))
			return false;
		if (direccion == null) {
			if (other.direccion != null)
				return false;
		} else if (!direccion.equals(other.direccion))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (primer_apellido == null) {
			if (other.primer_apellido != null)
				return false;
		} else if (!primer_apellido.equals(other.primer_apellido))
			return false;
		if (primer_nombre == null) {
			if (other.primer_nombre != null)
				return false;
		} else if (!primer_nombre.equals(other.primer_nombre))
			return false;
		if (puesto == null) {
			if (other.puesto != null)
				return false;
		} else if (!puesto.equals(other.puesto))
			return false;
		if (segundo_apellido == null) {
			if (other.segundo_apellido != null)
				return false;
		} else if (!segundo_apellido.equals(other.segundo_apellido))
			return false;
		if (segundo_nombre == null) {
			if (other.segundo_nombre != null)
				return false;
		} else if (!segundo_nombre.equals(other.segundo_nombre))
			return false;
		if (telefono == null) {
			if (other.telefono != null)
				return false;
		} else if (!telefono.equals(other.telefono))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FormUsuario [usuario=" + usuario + ", contrasena=" + contrasena + ", puesto=" + puesto
				+ ", primer_nombre=" + primer_nombre + ", segundo_nombre=" + segundo_nombre + ", primer_apellido="
				+ primer_apellido + ", segundo_apellido=" + segundo_apellido + ", direccion=" + direccion
				+ ", telefono=" + telefono + ", email=" + email + "]";
	}

}
