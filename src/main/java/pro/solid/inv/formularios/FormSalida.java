package pro.solid.inv.formularios;

public class FormSalida {

	private String codigo;
	private int cantidad;
	private String ubicacion;
	private String compuerta;
	
	public FormSalida(){}
	
	public FormSalida(String codigo, int cantidad, String ubicacion, String compuerta) {
		super();
		this.codigo = codigo;
		this.cantidad = cantidad;
		this.ubicacion = ubicacion;
		this.compuerta = compuerta;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getCompuerta() {
		return compuerta;
	}

	public void setCompuerta(String compuerta) {
		this.compuerta = compuerta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cantidad;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((compuerta == null) ? 0 : compuerta.hashCode());
		result = prime * result + ((ubicacion == null) ? 0 : ubicacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormSalida other = (FormSalida) obj;
		if (cantidad != other.cantidad)
			return false;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (compuerta == null) {
			if (other.compuerta != null)
				return false;
		} else if (!compuerta.equals(other.compuerta))
			return false;
		if (ubicacion == null) {
			if (other.ubicacion != null)
				return false;
		} else if (!ubicacion.equals(other.ubicacion))
			return false;
		return true;
	}
	
}
