package pro.solid.inv.formularios;

public class FormPreparacion {

	private Long ruta;
	private Long usuario;
	private Long compuerta;
	private String observacion;
	
	public FormPreparacion() {}

	public FormPreparacion(Long ruta, Long usuario, Long compuerta, String observacion) {
		super();
		this.ruta = ruta;
		this.usuario = usuario;
		this.compuerta = compuerta;
		this.observacion = observacion;
	}

	public Long getRuta() {
		return ruta;
	}

	public void setRuta(Long ruta) {
		this.ruta = ruta;
	}

	public Long getUsuario() {
		return usuario;
	}

	public void setUsuario(Long usuario) {
		this.usuario = usuario;
	}

	public Long getCompuerta() {
		return compuerta;
	}

	public void setCompuerta(Long compuerta) {
		this.compuerta = compuerta;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((compuerta == null) ? 0 : compuerta.hashCode());
		result = prime * result + ((observacion == null) ? 0 : observacion.hashCode());
		result = prime * result + ((ruta == null) ? 0 : ruta.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormPreparacion other = (FormPreparacion) obj;
		if (compuerta == null) {
			if (other.compuerta != null)
				return false;
		} else if (!compuerta.equals(other.compuerta))
			return false;
		if (observacion == null) {
			if (other.observacion != null)
				return false;
		} else if (!observacion.equals(other.observacion))
			return false;
		if (ruta == null) {
			if (other.ruta != null)
				return false;
		} else if (!ruta.equals(other.ruta))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FormPreparacion [ruta=" + ruta + ", usuario=" + usuario + ", compuerta=" + compuerta + ", observacion="
				+ observacion + "]";
	}

}
