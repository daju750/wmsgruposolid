package pro.solid.inv.formularios;

public class FormReporteIngreso {

	private String FechaInicio;
	private String FechaFin;
	private String usuario;
	
	public FormReporteIngreso() {}
	
	public FormReporteIngreso(String fechaInicio, String fechaFin, String usuario) {
		super();
		FechaInicio = fechaInicio;
		FechaFin = fechaFin;
		this.usuario = usuario;
	}

	public String getFechaInicio() {
		return FechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		FechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return FechaFin;
	}

	public void setFechaFin(String fechaFin) {
		FechaFin = fechaFin;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((FechaFin == null) ? 0 : FechaFin.hashCode());
		result = prime * result + ((FechaInicio == null) ? 0 : FechaInicio.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormReporteIngreso other = (FormReporteIngreso) obj;
		if (FechaFin == null) {
			if (other.FechaFin != null)
				return false;
		} else if (!FechaFin.equals(other.FechaFin))
			return false;
		if (FechaInicio == null) {
			if (other.FechaInicio != null)
				return false;
		} else if (!FechaInicio.equals(other.FechaInicio))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FormReporteIngreso [FechaInicio=" + FechaInicio + ", FechaFin=" + FechaFin + ", usuario=" + usuario
				+ "]";
	}	
}
