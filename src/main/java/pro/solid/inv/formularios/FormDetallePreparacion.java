package pro.solid.inv.formularios;

public class FormDetallePreparacion {

	private String cantidad;

	public FormDetallePreparacion() {}
	
	public FormDetallePreparacion(String cantidad) {
		super();
		this.cantidad = cantidad;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cantidad == null) ? 0 : cantidad.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormDetallePreparacion other = (FormDetallePreparacion) obj;
		if (cantidad == null) {
			if (other.cantidad != null)
				return false;
		} else if (!cantidad.equals(other.cantidad))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FormDetallePreparacion [cantidad=" + cantidad + "]";
	}
	
}
