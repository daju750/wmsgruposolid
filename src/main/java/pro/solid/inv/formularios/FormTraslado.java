package pro.solid.inv.formularios;

public class FormTraslado {

	private String origen;
	private String codigosap;
	private String cantidad;
	private String destino;
	private String lote;
	
	public FormTraslado() {}

	public FormTraslado(String origen, String codigosap, String cantidad, String destino, String lote) {
		super();
		this.origen = origen;
		this.codigosap = codigosap;
		this.cantidad = cantidad;
		this.destino = destino;
		this.lote = lote;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getCodigosap() {
		return codigosap;
	}

	public void setCodigosap(String codigosap) {
		this.codigosap = codigosap;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cantidad == null) ? 0 : cantidad.hashCode());
		result = prime * result + ((codigosap == null) ? 0 : codigosap.hashCode());
		result = prime * result + ((destino == null) ? 0 : destino.hashCode());
		result = prime * result + ((lote == null) ? 0 : lote.hashCode());
		result = prime * result + ((origen == null) ? 0 : origen.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormTraslado other = (FormTraslado) obj;
		if (cantidad == null) {
			if (other.cantidad != null)
				return false;
		} else if (!cantidad.equals(other.cantidad))
			return false;
		if (codigosap == null) {
			if (other.codigosap != null)
				return false;
		} else if (!codigosap.equals(other.codigosap))
			return false;
		if (destino == null) {
			if (other.destino != null)
				return false;
		} else if (!destino.equals(other.destino))
			return false;
		if (lote == null) {
			if (other.lote != null)
				return false;
		} else if (!lote.equals(other.lote))
			return false;
		if (origen == null) {
			if (other.origen != null)
				return false;
		} else if (!origen.equals(other.origen))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FormTraslado [origen=" + origen + ", codigosap=" + codigosap + ", cantidad=" + cantidad + ", destino="
				+ destino + ", lote=" + lote + "]";
	}
	
}
