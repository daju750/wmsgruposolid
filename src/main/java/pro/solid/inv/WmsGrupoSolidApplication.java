package pro.solid.inv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class WmsGrupoSolidApplication {
	public static void main(String[] args) {
		SpringApplication.run(WmsGrupoSolidApplication.class, args);
	}
}
