package pro.solid.inv.modelo;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "registro_ingreso")
public class RegistroIngreso{
	@Id
	private Long id;
	private Long id_producto;
	private Long id_ubicacion;
	private Long id_compuerta;
	private Long id_usuario;
	private Long cantidad;
	private String lote;
	private Date fecha;

	public RegistroIngreso(){}

	public RegistroIngreso(Long id, Long id_producto, Long id_ubicacion, Long id_compuerta, Long id_usuario,
			Long cantidad, String lote, Date fecha) {
		super();
		this.id = id;
		this.id_producto = id_producto;
		this.id_ubicacion = id_ubicacion;
		this.id_compuerta = id_compuerta;
		this.id_usuario = id_usuario;
		this.cantidad = cantidad;
		this.lote = lote;
		this.fecha = fecha;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId_producto() {
		return id_producto;
	}

	public void setId_producto(Long id_producto) {
		this.id_producto = id_producto;
	}

	public Long getId_ubicacion() {
		return id_ubicacion;
	}

	public void setId_ubicacion(Long id_ubicacion) {
		this.id_ubicacion = id_ubicacion;
	}

	public Long getId_compuerta() {
		return id_compuerta;
	}

	public void setId_compuerta(Long id_compuerta) {
		this.id_compuerta = id_compuerta;
	}

	public Long getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cantidad == null) ? 0 : cantidad.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((id_compuerta == null) ? 0 : id_compuerta.hashCode());
		result = prime * result + ((id_producto == null) ? 0 : id_producto.hashCode());
		result = prime * result + ((id_ubicacion == null) ? 0 : id_ubicacion.hashCode());
		result = prime * result + ((id_usuario == null) ? 0 : id_usuario.hashCode());
		result = prime * result + ((lote == null) ? 0 : lote.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistroIngreso other = (RegistroIngreso) obj;
		if (cantidad == null) {
			if (other.cantidad != null)
				return false;
		} else if (!cantidad.equals(other.cantidad))
			return false;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (id_compuerta == null) {
			if (other.id_compuerta != null)
				return false;
		} else if (!id_compuerta.equals(other.id_compuerta))
			return false;
		if (id_producto == null) {
			if (other.id_producto != null)
				return false;
		} else if (!id_producto.equals(other.id_producto))
			return false;
		if (id_ubicacion == null) {
			if (other.id_ubicacion != null)
				return false;
		} else if (!id_ubicacion.equals(other.id_ubicacion))
			return false;
		if (id_usuario == null) {
			if (other.id_usuario != null)
				return false;
		} else if (!id_usuario.equals(other.id_usuario))
			return false;
		if (lote == null) {
			if (other.lote != null)
				return false;
		} else if (!lote.equals(other.lote))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RegistroIngreso [id=" + id + ", id_producto=" + id_producto + ", id_ubicacion=" + id_ubicacion
				+ ", id_compuerta=" + id_compuerta + ", id_usuario=" + id_usuario + ", cantidad=" + cantidad + ", lote="
				+ lote + ", fecha=" + fecha + "]";
	}
	
}
