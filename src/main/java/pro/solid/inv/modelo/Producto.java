package pro.solid.inv.modelo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Producto")
public class Producto {
	
	@Id
	private Long id;
	private String codigo;
	private String codigosap;
	private String descripcion;
	private String precio;
	private Long envase;
	private String factor;
	
	public Producto() {}
	
	public Producto(String codigosap) {this.codigosap = codigosap;}
	
	public Producto(Long id, String codigo, String codigosap, String descripcion, String precio, Long envase,
			String factor) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.codigosap = codigosap;
		this.descripcion = descripcion;
		this.precio = precio;
		this.envase = envase;
		this.factor = factor;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}



	public String getCodigosap() {
		return codigosap;
	}



	public void setCodigosap(String codigosap) {
		this.codigosap = codigosap;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public String getPrecio() {
		return precio;
	}



	public void setPrecio(String precio) {
		this.precio = precio;
	}



	public Long getEnvase() {
		return envase;
	}



	public void setEnvase(Long envase) {
		this.envase = envase;
	}

	public String getFactor() {
		return factor;
	}

	public void setFactor(String factor) {
		this.factor = factor;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((codigosap == null) ? 0 : codigosap.hashCode());
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((envase == null) ? 0 : envase.hashCode());
		result = prime * result + ((factor == null) ? 0 : factor.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((precio == null) ? 0 : precio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producto other = (Producto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (codigosap == null) {
			if (other.codigosap != null)
				return false;
		} else if (!codigosap.equals(other.codigosap))
			return false;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (envase == null) {
			if (other.envase != null)
				return false;
		} else if (!envase.equals(other.envase))
			return false;
		if (factor == null) {
			if (other.factor != null)
				return false;
		} else if (!factor.equals(other.factor))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (precio == null) {
			if (other.precio != null)
				return false;
		} else if (!precio.equals(other.precio))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Producto [id=" + id + ", codigo=" + codigo + ", codigosap=" + codigosap + ", descripcion=" + descripcion
				+ ", precio=" + precio + ", envase=" + envase + ", factor=" + factor + "]";
	}
	
}
