package pro.solid.inv.modelo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="preparacion_detalle")
public class PreparacionDetalle {

	@Id
	private Long id;
	private Long id_preparacion;
	private String codigosap;
	private Long cantidad;
	@Transient
	private Long total;

	public PreparacionDetalle() {}

	public PreparacionDetalle(Long id, Long id_preparacion, String codigosap, Long cantidad) {
		super();
		this.id = id;
		this.id_preparacion = id_preparacion;
		this.codigosap = codigosap;
		this.cantidad = cantidad;
	}
	
	public PreparacionDetalle(String codigosap, Long cantidad) {
		super();
		this.codigosap = codigosap;
		this.cantidad = cantidad;
	}

	public PreparacionDetalle(Long id, Long id_preparacion, String codigosap, Long cantidad, Long total) {
		super();
		this.id = id;
		this.id_preparacion = id_preparacion;
		this.codigosap = codigosap;
		this.cantidad = cantidad;
		this.total = total;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId_preparacion() {
		return id_preparacion;
	}

	public void setId_preparacion(Long id_preparacion) {
		this.id_preparacion = id_preparacion;
	}

	public String getCodigosap() {
		return codigosap;
	}

	public void setCodigosap(String codigosap) {
		this.codigosap = codigosap;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cantidad == null) ? 0 : cantidad.hashCode());
		result = prime * result + ((codigosap == null) ? 0 : codigosap.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((id_preparacion == null) ? 0 : id_preparacion.hashCode());
		result = prime * result + ((total == null) ? 0 : total.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PreparacionDetalle other = (PreparacionDetalle) obj;
		if (cantidad == null) {
			if (other.cantidad != null)
				return false;
		} else if (!cantidad.equals(other.cantidad))
			return false;
		if (codigosap == null) {
			if (other.codigosap != null)
				return false;
		} else if (!codigosap.equals(other.codigosap))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (id_preparacion == null) {
			if (other.id_preparacion != null)
				return false;
		} else if (!id_preparacion.equals(other.id_preparacion))
			return false;
		if (total == null) {
			if (other.total != null)
				return false;
		} else if (!total.equals(other.total))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PreparacionDetalle [id=" + id + ", id_preparacion=" + id_preparacion + ", codigosap=" + codigosap
				+ ", cantidad=" + cantidad + ", total=" + total + "]";
	}

}
