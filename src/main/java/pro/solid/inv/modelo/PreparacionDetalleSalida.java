package pro.solid.inv.modelo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="preparacion_salida")
public class PreparacionDetalleSalida {
	
	@Id
	private Long id;
	private Long id_preparacion;
	private Long id_inventario;
	private Long id_producto;
	private Long id_ubicacion;
	private boolean estado;
	private String lote;
	private Long cantidad;
	
	public PreparacionDetalleSalida() {}
	
	public PreparacionDetalleSalida(Long id, Long id_preparacion, Long id_inventario, Long id_producto,
			Long id_ubicacion, boolean estado, String lote, Long cantidad) {
		super();
		this.id = id;
		this.id_preparacion = id_preparacion;
		this.id_inventario = id_inventario;
		this.id_producto = id_producto;
		this.id_ubicacion = id_ubicacion;
		this.estado = estado;
		this.lote = lote;
		this.cantidad = cantidad;
	}
	
	public PreparacionDetalleSalida(Long id,Long id_inventario,Long cantidad){
		this.id = id;
		this.id_inventario = id_inventario;
		this.cantidad = cantidad;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId_preparacion() {
		return id_preparacion;
	}
	public void setId_preparacion(Long id_preparacion) {
		this.id_preparacion = id_preparacion;
	}
	public Long getId_inventario() {
		return id_inventario;
	}
	public void setId_inventario(Long id_inventario) {
		this.id_inventario = id_inventario;
	}
	public Long getId_producto() {
		return id_producto;
	}
	public void setId_producto(Long id_producto) {
		this.id_producto = id_producto;
	}
	public Long getId_ubicacion() {
		return id_ubicacion;
	}
	public void setId_ubicacion(Long id_ubicacion) {
		this.id_ubicacion = id_ubicacion;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	public String getLote() {
		return lote;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
	public Long getCantidad() {
		return cantidad;
	}
	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cantidad == null) ? 0 : cantidad.hashCode());
		result = prime * result + (estado ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((id_inventario == null) ? 0 : id_inventario.hashCode());
		result = prime * result + ((id_preparacion == null) ? 0 : id_preparacion.hashCode());
		result = prime * result + ((id_producto == null) ? 0 : id_producto.hashCode());
		result = prime * result + ((id_ubicacion == null) ? 0 : id_ubicacion.hashCode());
		result = prime * result + ((lote == null) ? 0 : lote.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PreparacionDetalleSalida other = (PreparacionDetalleSalida) obj;
		if (cantidad == null) {
			if (other.cantidad != null)
				return false;
		} else if (!cantidad.equals(other.cantidad))
			return false;
		if (estado != other.estado)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (id_inventario == null) {
			if (other.id_inventario != null)
				return false;
		} else if (!id_inventario.equals(other.id_inventario))
			return false;
		if (id_preparacion == null) {
			if (other.id_preparacion != null)
				return false;
		} else if (!id_preparacion.equals(other.id_preparacion))
			return false;
		if (id_producto == null) {
			if (other.id_producto != null)
				return false;
		} else if (!id_producto.equals(other.id_producto))
			return false;
		if (id_ubicacion == null) {
			if (other.id_ubicacion != null)
				return false;
		} else if (!id_ubicacion.equals(other.id_ubicacion))
			return false;
		if (lote == null) {
			if (other.lote != null)
				return false;
		} else if (!lote.equals(other.lote))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "PreparacionDetalleSalida [id=" + id + ", id_preparacion=" + id_preparacion + ", id_inventario="
				+ id_inventario + ", id_producto=" + id_producto + ", id_ubicacion=" + id_ubicacion + ", estado="
				+ estado + ", lote=" + lote + ", cantidad=" + cantidad + "]";
	}
	
}
