package pro.solid.inv.modelo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="inventario")
public class Inventario {

	@Id
	private Long id;
	private Long id_producto;
	private Long id_ubicacion;
	private Long cantidad;
	private Long reserva;
	private boolean habilitado;
	private String lote;
	private int id_domo;
	
	public Inventario() {}
	
	public Inventario(Long id, Long id_producto, Long id_ubicacion, Long cantidad, Long reserva, boolean habilitado,
			String lote, int id_domo) {
		super();
		this.id = id;
		this.id_producto = id_producto;
		this.id_ubicacion = id_ubicacion;
		this.cantidad = cantidad;
		this.reserva = reserva;
		this.habilitado = habilitado;
		this.lote = lote;
		this.id_domo = id_domo;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public Long getId_producto() {
		return id_producto;
	}



	public void setId_producto(Long id_producto) {
		this.id_producto = id_producto;
	}



	public Long getId_ubicacion() {
		return id_ubicacion;
	}



	public void setId_ubicacion(Long id_ubicacion) {
		this.id_ubicacion = id_ubicacion;
	}



	public Long getCantidad() {
		return cantidad;
	}



	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}



	public Long getReserva() {
		return reserva;
	}



	public void setReserva(Long reserva) {
		this.reserva = reserva;
	}



	public boolean isHabilitado() {
		return habilitado;
	}



	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}



	public String getLote() {
		return lote;
	}



	public void setLote(String lote) {
		this.lote = lote;
	}



	public int getId_domo() {
		return id_domo;
	}



	public void setId_domo(int id_domo) {
		this.id_domo = id_domo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cantidad == null) ? 0 : cantidad.hashCode());
		result = prime * result + (habilitado ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + id_domo;
		result = prime * result + ((id_producto == null) ? 0 : id_producto.hashCode());
		result = prime * result + ((id_ubicacion == null) ? 0 : id_ubicacion.hashCode());
		result = prime * result + ((lote == null) ? 0 : lote.hashCode());
		result = prime * result + ((reserva == null) ? 0 : reserva.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inventario other = (Inventario) obj;
		if (cantidad == null) {
			if (other.cantidad != null)
				return false;
		} else if (!cantidad.equals(other.cantidad))
			return false;
		if (habilitado != other.habilitado)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (id_domo != other.id_domo)
			return false;
		if (id_producto == null) {
			if (other.id_producto != null)
				return false;
		} else if (!id_producto.equals(other.id_producto))
			return false;
		if (id_ubicacion == null) {
			if (other.id_ubicacion != null)
				return false;
		} else if (!id_ubicacion.equals(other.id_ubicacion))
			return false;
		if (lote == null) {
			if (other.lote != null)
				return false;
		} else if (!lote.equals(other.lote))
			return false;
		if (reserva == null) {
			if (other.reserva != null)
				return false;
		} else if (!reserva.equals(other.reserva))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Inventario [id=" + id + ", id_producto=" + id_producto + ", id_ubicacion=" + id_ubicacion
				+ ", cantidad=" + cantidad + ", reserva=" + reserva + ", habilitado=" + habilitado + ", lote=" + lote
				+ ", id_domo=" + id_domo + "]";
	}
	
}
