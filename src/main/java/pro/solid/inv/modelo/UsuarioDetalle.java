package pro.solid.inv.modelo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="usuario_detalle")
public class UsuarioDetalle {

	@Id
	private Long id;
	private Long id_usuario;
	private String primer_nombre;
	private String segundo_nombre;
	private String primer_apellido;
	private String segundo_apellido;
	private String direccion;
	private String email;
	private int telefono;
	
	public UsuarioDetalle(){};
	
	public UsuarioDetalle(Long id, Long id_usuario, String primer_nombre, String segundo_nombre, String primer_apellido,
			String segundo_apellido, String direccion, String email, int telefono) {
		super();
		this.id = id;
		this.id_usuario = id_usuario;
		this.primer_nombre = primer_nombre;
		this.segundo_nombre = segundo_nombre;
		this.primer_apellido = primer_apellido;
		this.segundo_apellido = segundo_apellido;
		this.direccion = direccion;
		this.email = email;
		this.telefono = telefono;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getPrimer_nombre() {
		return primer_nombre;
	}
	public void setPrimer_nombre(String primer_nombre) {
		this.primer_nombre = primer_nombre;
	}
	public String getSegundo_nombre() {
		return segundo_nombre;
	}
	public void setSegundo_nombre(String segundo_nombre) {
		this.segundo_nombre = segundo_nombre;
	}
	public String getPrimer_apellido() {
		return primer_apellido;
	}
	public void setPrimer_apellido(String primer_apellido) {
		this.primer_apellido = primer_apellido;
	}
	public String getSegundo_apellido() {
		return segundo_apellido;
	}
	public void setSegundo_apellido(String segundo_apellido) {
		this.segundo_apellido = segundo_apellido;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getTelefono() {
		return telefono;
	}
	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((direccion == null) ? 0 : direccion.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((id_usuario == null) ? 0 : id_usuario.hashCode());
		result = prime * result + ((primer_apellido == null) ? 0 : primer_apellido.hashCode());
		result = prime * result + ((primer_nombre == null) ? 0 : primer_nombre.hashCode());
		result = prime * result + ((segundo_apellido == null) ? 0 : segundo_apellido.hashCode());
		result = prime * result + ((segundo_nombre == null) ? 0 : segundo_nombre.hashCode());
		result = prime * result + telefono;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioDetalle other = (UsuarioDetalle) obj;
		if (direccion == null) {
			if (other.direccion != null)
				return false;
		} else if (!direccion.equals(other.direccion))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (id_usuario == null) {
			if (other.id_usuario != null)
				return false;
		} else if (!id_usuario.equals(other.id_usuario))
			return false;
		if (primer_apellido == null) {
			if (other.primer_apellido != null)
				return false;
		} else if (!primer_apellido.equals(other.primer_apellido))
			return false;
		if (primer_nombre == null) {
			if (other.primer_nombre != null)
				return false;
		} else if (!primer_nombre.equals(other.primer_nombre))
			return false;
		if (segundo_apellido == null) {
			if (other.segundo_apellido != null)
				return false;
		} else if (!segundo_apellido.equals(other.segundo_apellido))
			return false;
		if (segundo_nombre == null) {
			if (other.segundo_nombre != null)
				return false;
		} else if (!segundo_nombre.equals(other.segundo_nombre))
			return false;
		if (telefono != other.telefono)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "UsuarioDetalle [id=" + id + ", id_usuario=" + id_usuario + ", primer_nombre=" + primer_nombre
				+ ", segundo_nombre=" + segundo_nombre + ", primer_apellido=" + primer_apellido + ", segundo_apellido="
				+ segundo_apellido + ", direccion=" + direccion + ", email=" + email + ", telefono=" + telefono + "]";
	}
	
}
