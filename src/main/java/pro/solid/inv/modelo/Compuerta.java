package pro.solid.inv.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="compuerta")
public class Compuerta implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id")
	private Long id;
	private Long id_domo;
	private Long item;
	private String puerta;
	
	public Compuerta() {}
	
	public Compuerta(Long id,String puerta) {
		this.id = id;
		this.puerta = puerta;
	}

	public Compuerta(Long id, Long id_domo, Long item, String puerta) {
		super();
		this.id = id;
		this.id_domo = id_domo;
		this.item = item;
		this.puerta = puerta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId_domo() {
		return id_domo;
	}

	public void setId_domo(Long id_domo) {
		this.id_domo = id_domo;
	}

	public Long getItem() {
		return item;
	}

	public void setItem(Long item) {
		this.item = item;
	}

	public String getPuerta() {
		return puerta;
	}

	public void setPuerta(String puerta) {
		this.puerta = puerta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((id_domo == null) ? 0 : id_domo.hashCode());
		result = prime * result + ((item == null) ? 0 : item.hashCode());
		result = prime * result + ((puerta == null) ? 0 : puerta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Compuerta other = (Compuerta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (id_domo == null) {
			if (other.id_domo != null)
				return false;
		} else if (!id_domo.equals(other.id_domo))
			return false;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item))
			return false;
		if (puerta == null) {
			if (other.puerta != null)
				return false;
		} else if (!puerta.equals(other.puerta))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Compuerta [id=" + id + ", id_domo=" + id_domo + ", item=" + item + ", puerta=" + puerta + "]";
	}
}
