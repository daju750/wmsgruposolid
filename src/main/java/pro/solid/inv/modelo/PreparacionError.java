package pro.solid.inv.modelo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="preparacion_error")
public class PreparacionError {

	@Id
	private Long id;
	private Long id_preparacion;
	private String descripcion;
	
	public PreparacionError() {}
	public PreparacionError(Long id, Long id_preparacion, String descripcion) {
		super();
		this.id = id;
		this.id_preparacion = id_preparacion;
		this.descripcion = descripcion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId_preparacion() {
		return id_preparacion;
	}
	public void setId_preparacion(Long id_preparacion) {
		this.id_preparacion = id_preparacion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((id_preparacion == null) ? 0 : id_preparacion.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PreparacionError other = (PreparacionError) obj;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (id_preparacion == null) {
			if (other.id_preparacion != null)
				return false;
		} else if (!id_preparacion.equals(other.id_preparacion))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "preparacion_error [id=" + id + ", id_preparacion=" + id_preparacion + ", descripcion=" + descripcion
				+ "]";
	}
	
}

