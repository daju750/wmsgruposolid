package pro.solid.inv.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="preparacion")
public class Preparacion implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id")
	private Long id;
	private Long id_usuario;
	private Long id_ruta;
	private Long id_administrador=(long)0;
	private Long id_compuerta;
	private Date fecha=new Date();
	private boolean posteado;
	private boolean visto;
	private String observacion;
	@Transient
	private boolean generado;

    public Preparacion() {}

    public Preparacion(Long id, Long id_usuario, Long id_ruta, Long id_administrador, Long id_compuerta, Date fecha,
			boolean posteado, boolean visto, String observacion, boolean generado) {
		super();
		this.id = id;
		this.id_usuario = id_usuario;
		this.id_ruta = id_ruta;
		this.id_administrador = id_administrador;
		this.id_compuerta = id_compuerta;
		this.fecha = fecha;
		this.posteado = posteado;
		this.visto = visto;
		this.observacion = observacion;
		this.generado = generado;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}

	public Long getId_ruta() {
		return id_ruta;
	}

	public void setId_ruta(Long id_ruta) {
		this.id_ruta = id_ruta;
	}

	public Long getId_administrador() {
		return id_administrador;
	}

	public void setId_administrador(Long id_administrador) {
		this.id_administrador = id_administrador;
	}

	public Long getId_compuerta() {
		return id_compuerta;
	}

	public void setId_compuerta(Long id_compuerta) {
		this.id_compuerta = id_compuerta;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public boolean isPosteado() {
		return posteado;
	}

	public void setPosteado(boolean posteado) {
		this.posteado = posteado;
	}

	public boolean isVisto() {
		return visto;
	}

	public void setVisto(boolean visto) {
		this.visto = visto;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public boolean isGenerado() {
		return generado;
	}

	public void setGenerado(boolean generado) {
		this.generado = generado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + (generado ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((id_administrador == null) ? 0 : id_administrador.hashCode());
		result = prime * result + ((id_compuerta == null) ? 0 : id_compuerta.hashCode());
		result = prime * result + ((id_ruta == null) ? 0 : id_ruta.hashCode());
		result = prime * result + ((id_usuario == null) ? 0 : id_usuario.hashCode());
		result = prime * result + ((observacion == null) ? 0 : observacion.hashCode());
		result = prime * result + (posteado ? 1231 : 1237);
		result = prime * result + (visto ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Preparacion other = (Preparacion) obj;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (generado != other.generado)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (id_administrador == null) {
			if (other.id_administrador != null)
				return false;
		} else if (!id_administrador.equals(other.id_administrador))
			return false;
		if (id_compuerta == null) {
			if (other.id_compuerta != null)
				return false;
		} else if (!id_compuerta.equals(other.id_compuerta))
			return false;
		if (id_ruta == null) {
			if (other.id_ruta != null)
				return false;
		} else if (!id_ruta.equals(other.id_ruta))
			return false;
		if (id_usuario == null) {
			if (other.id_usuario != null)
				return false;
		} else if (!id_usuario.equals(other.id_usuario))
			return false;
		if (observacion == null) {
			if (other.observacion != null)
				return false;
		} else if (!observacion.equals(other.observacion))
			return false;
		if (posteado != other.posteado)
			return false;
		if (visto != other.visto)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Preparacion [id=" + id + ", id_usuario=" + id_usuario + ", id_ruta=" + id_ruta + ", id_administrador="
				+ id_administrador + ", id_compuerta=" + id_compuerta + ", fecha=" + fecha + ", posteado=" + posteado
				+ ", visto=" + visto + ", observacion=" + observacion + ", generado=" + generado + "]";
	}

}
