package pro.solid.inv.modelo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ubicacion")
public class Ubicacion {
	
	@Id
	private Long id;
	private Long id_domo;
	private int modulo;
	private int nivel;
	private String codigo;
	private Long item;
	
	public Ubicacion() {}
	
	public Ubicacion(Long id, Long id_domo, int modulo, int nivel, String codigo, Long item) {
		super();
		this.id = id;
		this.id_domo = id_domo;
		this.modulo = modulo;
		this.nivel = nivel;
		this.codigo = codigo;
		this.item = item;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId_domo() {
		return id_domo;
	}
	public void setId_domo(Long id_domo) {
		this.id_domo = id_domo;
	}
	public int getModulo() {
		return modulo;
	}
	public void setModulo(int modulo) {
		this.modulo = modulo;
	}
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Long getItem() {
		return item;
	}
	public void setItem(Long item) {
		this.item = item;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((id_domo == null) ? 0 : id_domo.hashCode());
		result = prime * result + ((item == null) ? 0 : item.hashCode());
		result = prime * result + modulo;
		result = prime * result + nivel;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ubicacion other = (Ubicacion) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (id_domo == null) {
			if (other.id_domo != null)
				return false;
		} else if (!id_domo.equals(other.id_domo))
			return false;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item))
			return false;
		if (modulo != other.modulo)
			return false;
		if (nivel != other.nivel)
			return false;
		return true;
	}
	
}
