package pro.solid.inv.modelo;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "roles")
    private Set<Usuario> usuarios;

    public Role(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
    
    public Role() {}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Usuario> getUsers() {
        return usuarios;
    }

    public void setUsers(Set<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
}
