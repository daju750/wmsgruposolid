package pro.solid.inv.RestControlador;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pro.solid.inv.modelo.Producto;
import pro.solid.inv.rest.model.CodigoProductoSap;
import pro.solid.inv.servicio.ProductoServicio;

@RestController
@RequestMapping("/producto-rest")
public class ProductoRestControlador {

	@Autowired
	private ProductoServicio productoServicio;
	
	@ResponseBody
	@RequestMapping("/existe/{id}")
	public Map<String, Boolean> existeProId(@PathVariable("id") String codigosap) {
		System.out.println(this.productoServicio.JDBCConsultaExiste(codigosap));
		if(this.productoServicio.JDBCConsultaExiste(codigosap)==true){
			return Collections.singletonMap("success", true);
		}else{
			return Collections.singletonMap("failed", false);
		}
	}
	
	@ResponseBody
	@GetMapping(value = "/listarCodigosSap")
	public List<CodigoProductoSap> buscar(HttpServletRequest request){
		return this.productoServicio.JDBCRestComoListaProductos(request.getParameter("codigosap"));
	}
	
	@GetMapping(value = "/{id}")
	public Optional<Producto> buscar(@PathVariable("id") Long id){
		   Optional<Producto> producto =  this.productoServicio.Consultar(id);
		return producto;
	}
	
}
