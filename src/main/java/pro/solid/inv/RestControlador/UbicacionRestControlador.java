package pro.solid.inv.RestControlador;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pro.solid.inv.rest.model.CodigoUbicacion;
import pro.solid.inv.servicio.UbicacionServicio;


@RestController
@RequestMapping("/ubicacion-rest")
public class UbicacionRestControlador {

	@Autowired
	private UbicacionServicio ubicacionServicio;
	
	@ResponseBody
	@GetMapping(value = "/listarCodigosUbicacion")
	public List<CodigoUbicacion> buscar(HttpServletRequest request){
		return this.ubicacionServicio.JDBCRestComoListaUbicaciones(request.getParameter("codigo"));
	}
	
}
