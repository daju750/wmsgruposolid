package pro.solid.inv.RestControlador;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pro.solid.inv.servicio.InventarioServicio;

@RestController
@RequestMapping("/inventario-rest")
public class InventarioRestControlador {

	@Autowired
	private InventarioServicio inventarioServicio;
	
	@GetMapping("/total")
	@ResponseBody
	public Long GetValidar(HttpServletRequest request){
		//String port = environment.getProperty("local.server.port");
		Long total= (long)0;
		try {
		
			total=this.inventarioServicio.JDBConsultaTotal(request.getParameter("codigosap"));
		} catch(Exception ex){}
	
	return total;
	}
	
}
