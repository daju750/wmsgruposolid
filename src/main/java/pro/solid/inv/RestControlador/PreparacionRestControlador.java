package pro.solid.inv.RestControlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pro.solid.inv.servicio.PreparacionServicio;

@RestController
@RequestMapping("/preparacion-rest")
public class PreparacionRestControlador {

	@Autowired
	private PreparacionServicio preparacionSevicio;
	
	@GetMapping("/postear/{id}")
	public ResponseEntity<String> PostearHoja(@PathVariable("id") long id){
		this.preparacionSevicio.JDBCRestPostearHoja(id);
		return new ResponseEntity<>("OK",HttpStatus.OK);
	}
	
	@GetMapping("/visto/{id}")
	public ResponseEntity<String> VistoHojaPikeador(@PathVariable("id") long id){
		this.preparacionSevicio.JDBCRestVistoHoja(id);
		return new ResponseEntity<>("OK",HttpStatus.OK);
	}
	
}
