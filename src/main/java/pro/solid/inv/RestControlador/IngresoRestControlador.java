package pro.solid.inv.RestControlador;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pro.solid.inv.respuesta.RestIngreso;
import pro.solid.inv.servicio.ProductoServicio;
import pro.solid.inv.servicio.UbicacionServicio;

@RestController
@RequestMapping("/ingreso-rest")
public class IngresoRestControlador {

	@Autowired
	private ProductoServicio productoServicio;
	@Autowired
	private UbicacionServicio ubicacionServicio;
	
	//@Autowired
	//private Environment environment;
	
	@GetMapping
	@ResponseBody
	public RestIngreso GetValidar(HttpServletRequest request){
		RestIngreso ingreso = new RestIngreso(false,false);
		//String port = environment.getProperty("local.server.port");
		try {
		ingreso.setCodigosap(this.productoServicio.JDBCConsultaExiste(request.getParameter("codigosap")));
		ingreso.setUbicacion(this.ubicacionServicio.JQPLRestConsultaExiste(request.getParameter("ubicacion")));
		} catch(Exception ex){}
		
		return ingreso;
	}

	
	@PostMapping
	@ResponseBody
	public RestIngreso PostValidar(HttpServletRequest request){
		RestIngreso ingreso = new RestIngreso();
		//String port = environment.getProperty("local.server.port");
		
		ingreso.setCodigosap(this.productoServicio.JDBCConsultaExiste(request.getParameter("codigosap")));
		ingreso.setUbicacion(this.ubicacionServicio.JQPLRestConsultaExiste(request.getParameter("ubicacion")));

		return ingreso;
	}
	
}
