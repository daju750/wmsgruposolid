package pro.solid.inv.RestControlador;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pro.solid.inv.modelo.PreparacionDetalle;
import pro.solid.inv.servicio.PreparacionDetalleSalidaServicio;
import pro.solid.inv.servicio.PreparacionDetalleServicio;

@RestController
@RequestMapping("/preparaciondetalle-rest")
public class PreparacionDetalleRestControlador {

	@Autowired
	private PreparacionDetalleServicio preparacionDetalleSerivicio;
	
	@Autowired
	private PreparacionDetalleServicio detallePreparacionServicio;

	@Autowired
	private PreparacionDetalleSalidaServicio preparacionDetalleSalidaServicio;
	
	@ResponseBody
	@GetMapping("/actulizar")
	public void GetValidar(HttpServletRequest request){
		
		PreparacionDetalle preparaciondetalle = this.preparacionDetalleSerivicio.ConsultaUno(Long.parseLong(request.getParameter("id")));
		String sqlInsercionErrores = "insert into preparacion_error(id_preparacion,descripcion) values";
		
		if(Long.parseLong(request.getParameter("cantidad"))>preparaciondetalle.getCantidad()){//System.out.println("Es mayor");
			int cantidad = (int) (Long.parseLong(request.getParameter("cantidad"))-preparaciondetalle.getCantidad());
			String cadena = this.detallePreparacionServicio.JDBProcedimientoIdsexcelpreparacion(preparaciondetalle.getCodigosap(),cantidad);
			String sqlInsercionPreparacionSalida = "insert into preparacion_salida(id_preparacion,id_inventario,id_ubicacion,id_producto,lote,cantidad) values";
			
			String[] split = cadena.split(":");
			int temp_total=0;		
			
			for(int i=0;i<split.length;i++){
				String[] columna = split[i].split(",");
				//System.out.print(columna[4]+"-"+cantidad+":"+(cantidad-Integer.parseInt(columna[4])));
				
				if(Integer.parseInt(columna[4])<cantidad || Integer.parseInt(columna[4])==cantidad){
					temp_total=Integer.parseInt(columna[4]);
					this.detallePreparacionServicio.JDBCTemplateInsertDetallePreparacion("update inventario set reserva=reserva+"+columna[4]+",habilitado=false where id="+columna[0]);
				//System.out.println(" Menor o igual");	
				}else{
					temp_total=cantidad;
					this.detallePreparacionServicio.JDBCTemplateInsertDetallePreparacion("update inventario set reserva=reserva+"+cantidad+" where id="+columna[0]);
				//System.out.println(" Mayor");
				}
				
	
				if(this.preparacionDetalleSalidaServicio.JDBCExisteProducto(Long.parseLong(columna[0]),preparaciondetalle.getId_preparacion())){
					this.detallePreparacionServicio.JDBCTemplateInsertDetallePreparacion("update preparacion_salida set cantidad=cantidad+"+temp_total+" where id_inventario="+columna[0]+" and id_preparacion="+preparaciondetalle.getId_preparacion());
				}else{
					this.detallePreparacionServicio.JDBCTemplateInsertDetallePreparacion(sqlInsercionPreparacionSalida+"("+preparaciondetalle.getId_preparacion()+","+columna[0]+","+columna[1]+","+columna[2]+","+columna[3]+","+temp_total+")");
				}
				
				cantidad-=Integer.parseInt(columna[4]);
				if(split.length==i && cantidad>0){
					sqlInsercionErrores+="("+preparaciondetalle.getId_preparacion()+",'No se encontro suficiente cantidad con el codigoSap:"+preparaciondetalle.getCodigosap()+" con "+cantidad+" unidades faltantes'),";
				}
			}
			
		}else if(Long.parseLong(request.getParameter("cantidad"))<preparaciondetalle.getCantidad()){
			//System.out.println("Es menor");
			int cantidad = (int) (preparaciondetalle.getCantidad()- Long.parseLong((request.getParameter("cantidad"))));
			String SQLEliminarids="delete from preparacion_salida where id in(";
			String cadena = this.detallePreparacionServicio.JDBCProcedimientoIdsexcelmenospreparacion(preparaciondetalle.getId_preparacion(),preparaciondetalle.getCodigosap(), cantidad);
			
			System.out.println(SQLEliminarids.length());
			
			String[] split = cadena.split(":");
			int temp_total=0;		
			
			for(int i=0;i<split.length;i++){
				String[] columna = split[i].split(",");
				
				if(Integer.parseInt(columna[2])<cantidad || Integer.parseInt(columna[2])==cantidad){
					temp_total=Integer.parseInt(columna[2]);
					SQLEliminarids+=columna[0]+",";
				}else{
					temp_total=cantidad;
					this.detallePreparacionServicio.JDBCTemplateInsertDetallePreparacion("update preparacion_salida set cantidad=cantidad-"+cantidad+" where id="+columna[0]);
				}
				this.detallePreparacionServicio.JDBCTemplateInsertDetallePreparacion("update inventario set reserva=reserva-"+temp_total+",habilitado=true where id="+columna[1]);
				
				cantidad-=Integer.parseInt(columna[2]);
			}
			if(SQLEliminarids.length()>43){this.detallePreparacionServicio.JDBCTemplateInsertDetallePreparacion(SQLEliminarids.substring(0,SQLEliminarids.length()-1)+")");}	
		}
		if(Integer.parseInt(request.getParameter("cantidad"))!=0){this.detallePreparacionServicio.JDBCTemplateInsertDetallePreparacion("update preparacion_detalle set cantidad="+request.getParameter("cantidad")+" where id="+preparaciondetalle.getId());	}
		
	}
	
	
	@ResponseBody
	@PostMapping("/validar/producto")
	public void GetValidarProducto(HttpServletResponse response){
		//String port = environment.getProperty("local.server.port");
		
		boolean existe=false;
		try {
		
			System.out.println("hola");
			
		} catch(Exception ex){}

	}
	
	
}
