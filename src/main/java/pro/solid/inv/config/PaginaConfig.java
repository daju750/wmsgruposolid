package pro.solid.inv.config;

public class PaginaConfig {

	//Users
	public static String Inicio="Inicio/inicio";
	public static String Login_registrar = "Login/registrar";
	public static String Login= "Login/login";
	public static String Ingreso = "Ingreso/ingreso";
	public static String Traslado = "Traslado/traslado";
	public static String Salida = "Salida/salida";
	public static String ListadoSalida = "Salida/lista_salida";
	public static String DetalleProductoSalida = "Salida/detalle_producto_salida";
	
	//Admin
	public static String AdminInicio = "Administrador/inicio";
	public static String AdminInicioPreparacion = "Administrador/preparacion_inicio";
	public static String AdminPreparacionNueva ="Administrador/preparacion/nueva";
	public static String AdminPreparacion = "Administrador/preparacion/inicio";
	public static String AdminRegisroDetallePreparacion = "Administrador/preparacion/registro_detalle";
	public static String AdminReporteInicio="Administrador/reporte/inicio";
	
	public static String AdminUsuario ="Administrador/usuario/inicio";
	public static String AdminNuevoUsuario = "Administrador/usuario/nuevo";
	public static String AdminEditarUsuario = "Administrador/usuario/editar";
	public static String AdminListarUsuarios = "Administrador/usuario/lista";
	public static String AdminHistorialUsuario = "Administrador/usuario_historial";
	public static String AdminGrupos = "Administrador/grupos";
	
	
}
